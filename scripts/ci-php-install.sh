#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update \
&& apt-get install python3-launchpadlib -y \
&& apt-get install software-properties-common -y \
&& add-apt-repository -r ppa:ondrej/php -y \
&& apt-get update -y \
&& apt-get install git -yqq \
&& apt-get autoremove -y \
&& apt-get install zlib1g-dev libicu-dev libpng-dev libzip-dev libmcrypt-dev -y \
&& docker-php-ext-install mysqli pdo pdo_mysql gd intl zip exif \
&& apt-get install curl git wget zip unzip mariadb-server mariadb-client -y \
&& curl -sS https://get.symfony.com/cli/installer -o /usr/local/bin/symfony | bash \
&& curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | bash \
&& apt install symfony-cli \
#&& echo 'extension=pdo_mysql.so' >> /usr/local/etc/php/php.ini \
#&& echo 'extension=pdo.so' >> /usr/local/etc/php/php.ini \
#&& mv /root/.symfony/bin/symfony /usr/local/bin/symfony \
#&& php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
#&& php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
#&& php -r "unlink('composer-setup.php');" \
#&& php composer install \
#&& curl -sS https://getcomposer.org/installer | php