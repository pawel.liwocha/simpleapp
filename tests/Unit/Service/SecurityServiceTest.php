<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service;

use App\Repository\FailureLoginAttemptRepository;
use App\Repository\TokenRepository;
use App\Service\SecurityService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class SecurityServiceTest extends KernelTestCase
{
    private SecurityService $securityService;

    protected function setUp(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $tokenRepository = $this->createMock(TokenRepository::class);
        $failureLoginRepository = $this->createMock(FailureLoginAttemptRepository::class);

        $securityService = new SecurityService(
            $em,
            $tokenRepository,
            $failureLoginRepository,
            '10',
            3,
            900
        );

        $this->securityService = $securityService;
    }

    public function testGeneratePassword(): void
    {
        $event = $this->securityService->generatePassword();

        self::assertIsString($event);
    }
}
