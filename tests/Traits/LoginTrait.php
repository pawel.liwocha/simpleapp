<?php

declare(strict_types=1);

namespace Traits;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\User\UserInterface;

trait LoginTrait
{
    private User $user;

    private string $token;

    public function loginSuperUser(): void
    {
        /** @var UserRepository $userRepository */
        $userRepository = static::getContainer()->get(UserRepository::class);
        /** @var UserInterface $testUserLogin */
        $testUserLogin = $userRepository->findOneBy(['userName' => 'superadmin']);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['userName' => 'superadmin']);
        $this->user = $testUser;

        $this->client->loginUser($testUserLogin, 'api');
    }

    public function loginTestUser(): void
    {
        /** @var UserRepository $userRepository */
        $userRepository = static::getContainer()->get(UserRepository::class);
        /** @var UserInterface $testUserLogin */
        $testUserLogin = $userRepository->findOneBy(['userName' => 'krupap']);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['userName' => 'krupap']);
        $this->user = $testUser;

        $this->client->loginUser($testUserLogin, 'api');
    }

    public function loginApiSuperUser(): void
    {
        $this->client->request('POST', 'api/login', [], [], [], (string) json_encode(['username' => 'superadmin', 'password' => '[wd0]5VTdw!{4tE5-ZADj']));

        $content = (array) json_decode((string) $this->client->getResponse()->getContent(), true);
        $this->token = (string) $content['token'];
    }

    public function loginApiTestUser(): void
    {
        $this->client->request('POST', 'api/login', [], [], [], (string) json_encode(['username' => 'krupap', 'password' => 'le891$9Htr1K9O3Y%%7-7']));

        $content = (array) json_decode((string) $this->client->getResponse()->getContent(), true);
        $this->token = (string) $content['token'];
    }
}
