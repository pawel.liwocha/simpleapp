<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Tests\Application\Public;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class IndexControllerTest extends WebTestCase
{
    public KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = $this->createClient();
    }

    public function testMainIndex(): void
    {
        $this->client->request('GET', '/');

        $this->assertResponseIsSuccessful();
    }
}
