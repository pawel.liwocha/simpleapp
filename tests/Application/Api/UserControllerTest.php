<?php

declare(strict_types=1);

namespace App\Tests\Controller\Api;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Traits\LoginTrait;

/**
 * @internal
 *
 * @coversNothing
 */
final class UserControllerTest extends WebTestCase
{
    use LoginTrait;

    private KernelBrowser $client;

    private const URL = 'user';

    protected function setUp(): void
    {
        $this->client = $this->createClient();
        $this->client->setServerParameter('content-type', 'application/json');
        $this->client->setServerParameter('HTTP_CONTENT_TYPE', 'application/json');
        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');

        /** @var UserRepository $userRepository */
        $userRepository = self::getContainer()->get(UserRepository::class);

        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['userName' => 'krupap']);
        $this->user = $testUser;
    }

    public function testAddUser(): void
    {
        $this->loginApiSuperUser();
        $data = [
            'token' => $this->token,
            'firstName' => 'NewFirstName',
            'lastName' => 'NewLastName',
            'email' => 'testAdd@liwocha.pl',
            'role' => 'ROLE_USER',
        ];

        $this->client->request('POST', self::URL.'/add', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testEditUser(): void
    {
        $this->loginApiSuperUser();
        $data = [
            'token' => $this->token,
            'firstName' => 'NewFirstNameEdit',
            'lastName' => 'NewLastNameEdit',
            'email' => 'testAdd@liwocha.pl',
        ];

        $this->client->request('PUT', self::URL.'/'.$this->user->getUuid().'/edit', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testShowUser(): void
    {
        $this->loginApiSuperUser();
        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/'.$this->user->getUuid(), [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testGetRolesForUser(): void
    {
        $this->loginApiSuperUser();
        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/get-roles', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testShowAllUsers(): void
    {
        $this->loginApiSuperUser();
        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/list', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testChangePassword(): void
    {
        $this->loginApiSuperUser();
        $data = [
            'token' => $this->token,
            'plainPassword' => 'newPassword123@!!',
            'confirmPassword' => 'newPassword123@!!',
        ];

        $this->client->request('PUT', self::URL.'/'.$this->user->getUuid().'/change-password', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testChangeWrongPassword(): void
    {
        $this->loginApiSuperUser();

        $data = [
            'token' => $this->token,
            'plainPassword' => 'short',
            'confirmPassword' => 'short',
        ];

        $this->client->request('PUT', self::URL.'/'.$this->user->getUuid().'/change-password', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseStatusCodeSame(400);
        self::assertFalse($content['Success']);
    }

    public function testDeactivate(): void
    {
        $this->loginApiSuperUser();

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('PUT', self::URL.'/'.$this->user->getUuid().'/deactivate', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testActivate(): void
    {
        $this->loginApiSuperUser();

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('PUT', self::URL.'/'.$this->user->getUuid().'/deactivate', [], [], [], (string) json_encode($data));
        $this->client->restart();

        $this->loginApiSuperUser();
        $this->client->request('PUT', self::URL.'/'.$this->user->getUuid().'/activate', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testChangeRole(): void
    {
        $this->loginApiSuperUser();
        $data = [
            'token' => $this->token,
            'role' => 'ROLE_USER',
        ];

        $this->client->request('POST', self::URL.'/'.$this->user->getUuid().'/change-role', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testDelete(): void
    {
        $this->loginApiSuperUser();

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('DELETE', self::URL.'/'.$this->user->getUuid().'/delete', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }
}
