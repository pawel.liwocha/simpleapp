<?php

declare(strict_types=1);

namespace App\Tests\Application\Api;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class AuthControllerTest extends WebTestCase
{
    private const URL = 'api/';

    public KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = $this->createClient();
        $this->client->setServerParameter('content-type', 'application/json');
        $this->client->setServerParameter('HTTP_CONTENT_TYPE', 'application/json');
        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');
    }

    public function testLoginSuccess(): void
    {
        $this->client->request('POST', self::URL.'login', [], [], [], (string) json_encode(['username' => 'superadmin', 'password' => '[wd0]5VTdw!{4tE5-ZADj']));
        $this->client->restart();

        $this->assertResponseIsSuccessful();
    }

    public function testLoginFailed(): void
    {
        $this->client->request('POST', self::URL.'login', [], [], [], (string) json_encode(['username' => 'superadmin', 'password' => 'ZleHaslo12!@']));
        $this->client->restart();

        $this->assertResponseStatusCodeSame(401);
    }

    public function testLogoutSuccess(): void
    {
        $this->client->request('POST', self::URL.'login', [], [], [], (string) json_encode(['username' => 'superadmin', 'password' => '[wd0]5VTdw!{4tE5-ZADj']));

        $response = $this->client->getResponse();

        $this->assertResponseIsSuccessful();
        $content = (array) json_decode((string) $response->getContent(), true);
        $this->client->restart();

        $this->client->request('POST', self::URL.'logout', [], [], [], (string) json_encode(['token' => $content['token']]));
        $this->client->getResponse();
        $this->assertResponseRedirects();
        $this->client->restart();
    }
}
