<?php

declare(strict_types=1);

namespace App\Tests\Controller\Api;

use App\Entity\Lead;
use App\Entity\User;
use App\Repository\LeadRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Traits\LoginTrait;

/**
 * @internal
 *
 * @coversNothing
 */
final class LeadControllerTest extends WebTestCase
{
    use LoginTrait;

    private KernelBrowser $client;

    private LeadRepository $leadRepository;

    private const URL = 'lead';

    protected function setUp(): void
    {
        $this->client = $this->createClient();
        $this->client->setServerParameter('content-type', 'application/json');
        $this->client->setServerParameter('HTTP_CONTENT_TYPE', 'application/json');
        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');

        /** @var UserRepository $userRepository */
        $userRepository = self::getContainer()->get(UserRepository::class);
        /** @var LeadRepository $leadRepository */
        $leadRepository = self::getContainer()->get(LeadRepository::class);
        $this->leadRepository = $leadRepository;

        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['userName' => 'krupap']);
        $this->user = $testUser;
    }

    public function testAddLead(): void
    {
        $this->loginApiTestUser();
        $data = [
            'token' => $this->token,
            'sourcedDate' => '2024-01-29 13:00:00',
            'source' => 'Whatsapp',
            'firstName' => 'Test',
            'lastName' => 'AddDSi',
            'email' => 'testAddLead@liwocha.pl',
            'mobileNumber' => '500500500',
            'inquiredTreatment' => 'scar',
            'comment' => 'test comment',
        ];

        $this->client->request('POST', self::URL.'/add', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testImportLeads(): void
    {
        $this->loginApiTestUser();
        $data = [
            'token' => $this->token,
        ];

        $uploadedFile = new UploadedFile(
            __DIR__.'/../../../var/data/Import_SampleDataset_January2024.csv',
            'Import_SampleDataset_January2024.csv'
        );

        $this->client->request('POST', self::URL.'/import', [], ['csv_file' => $uploadedFile], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testEditLead(): void
    {
        $this->loginApiTestUser();
        /** @var Lead $lead */
        $lead = $this->leadRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::DESC]);

        $data = [
            'token' => $this->token,
            'sourcedDate' => '2024-01-29 13:00:00',
            'source' => 'sms',
            'firstName' => 'Test Edit',
            'lastName' => 'AddDSi Edit',
            'email' => 'testAddLead@liwocha.pl',
            'mobileNumber' => '500600500',
            'inquiredTreatment' => 'scar',
            'comment' => 'test comment',
        ];

        $this->client->request('PUT', self::URL.'/'.$lead->getUuid().'/edit', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testShowLead(): void
    {
        $this->loginApiTestUser();
        /** @var Lead $lead */
        $lead = $this->leadRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::DESC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/'.$lead->getUuid(), [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testShowAllLeads(): void
    {
        $this->loginApiTestUser();

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/list', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testShowAllLeadsToSendWhatsapp(): void
    {
        $this->loginApiTestUser();

        $data = [
            'token' => $this->token,
            'campaignType' => 'whatsapp',
        ];

        $this->client->request('GET', self::URL.'/list-to-send', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testShowAllLeadsToSendSms(): void
    {
        $this->loginApiTestUser();

        $data = [
            'token' => $this->token,
            'campaignType' => 'sms',
        ];

        $this->client->request('GET', self::URL.'/list-to-send', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testShowAllLeadsToColdCall(): void
    {
        $this->loginApiTestUser();

        $data = [
            'token' => $this->token,
            'campaignType' => 'coldcall',
        ];

        $this->client->request('GET', self::URL.'/list-to-send', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testShowAllLeadsToEmail(): void
    {
        $this->loginApiTestUser();

        $data = [
            'token' => $this->token,
            'campaignType' => 'email',
        ];

        $this->client->request('GET', self::URL.'/list-to-send', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testDelete(): void
    {
        $this->loginApiTestUser();
        /** @var Lead $lead */
        $lead = $this->leadRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::DESC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('DELETE', self::URL.'/'.$lead->getUuid().'/delete', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }
}
