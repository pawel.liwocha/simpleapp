<?php

declare(strict_types=1);

namespace App\Tests\Controller\Api;

use App\Entity\ParentCategoryService;
use App\Entity\User;
use App\Repository\ParentCategoryServiceRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Traits\LoginTrait;

/**
 * @internal
 *
 * @coversNothing
 */
final class ParentCategoryServiceControllerTest extends WebTestCase
{
    use LoginTrait;

    private KernelBrowser $client;

    private ParentCategoryServiceRepository $parentCategoryServiceRepository;

    private const URL = 'parent-category-service';

    protected function setUp(): void
    {
        $this->client = $this->createClient();
        $this->client->setServerParameter('content-type', 'application/json');
        $this->client->setServerParameter('HTTP_CONTENT_TYPE', 'application/json');
        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');

        /** @var UserRepository $userRepository */
        $userRepository = self::getContainer()->get(UserRepository::class);
        /** @var ParentCategoryServiceRepository $parentCategoryServiceRepository */
        $parentCategoryServiceRepository = self::getContainer()->get(ParentCategoryServiceRepository::class);
        $this->parentCategoryServiceRepository = $parentCategoryServiceRepository;

        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['userName' => 'krupap']);
        $this->user = $testUser;
    }

    public function testAddParentCategoryService(): void
    {
        $this->loginApiTestUser();
        $data = [
            'token' => $this->token,
            'name' => 'Test ParentCategoryService',
            'active' => 'true',
        ];

        $this->client->request('POST', self::URL.'/add', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testEditParentCategoryService(): void
    {
        $this->loginApiTestUser();
        /** @var ParentCategoryService $parentCategoryService */
        $parentCategoryService = $this->parentCategoryServiceRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::DESC]);

        $data = [
            'token' => $this->token,
            'name' => 'Test ParentCategoryService Edit',
            'active' => 'true',
        ];

        $this->client->request('PUT', self::URL.'/'.$parentCategoryService->getUuid().'/edit', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testShowParentCategoryService(): void
    {
        $this->loginApiTestUser();
        /** @var ParentCategoryService $parentCategoryService */
        $parentCategoryService = $this->parentCategoryServiceRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::DESC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/'.$parentCategoryService->getUuid(), [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testShowAllParentCategoryServices(): void
    {
        $this->loginApiTestUser();

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/list-all', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testShowAllActiveParentCategoryServices(): void
    {
        $this->loginApiTestUser();

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/list-active', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testActive(): void
    {
        $this->loginApiTestUser();
        /** @var ParentCategoryService $parentCategoryService */
        $parentCategoryService = $this->parentCategoryServiceRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::DESC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('PUT', self::URL.'/'.$parentCategoryService->getUuid().'/active', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testDeactive(): void
    {
        $this->loginApiTestUser();
        /** @var ParentCategoryService $parentCategoryService */
        $parentCategoryService = $this->parentCategoryServiceRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::DESC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('PUT', self::URL.'/'.$parentCategoryService->getUuid().'/deactivate', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testDelete(): void
    {
        $this->loginApiTestUser();
        /** @var ParentCategoryService $parentCategoryService */
        $parentCategoryService = $this->parentCategoryServiceRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::DESC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('DELETE', self::URL.'/'.$parentCategoryService->getUuid().'/delete', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }
}
