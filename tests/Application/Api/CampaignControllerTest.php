<?php

declare(strict_types=1);

namespace App\Tests\Controller\Api;

use App\Entity\Campaign;
use App\Entity\User;
use App\Repository\CampaignRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Traits\LoginTrait;

/**
 * @internal
 *
 * @coversNothing
 */
final class CampaignControllerTest extends WebTestCase
{
    use LoginTrait;

    private KernelBrowser $client;

    private CampaignRepository $campaignRepository;

    private const URL = 'campaign';

    protected function setUp(): void
    {
        $this->client = $this->createClient();
        $this->client->setServerParameter('content-type', 'application/json');
        $this->client->setServerParameter('HTTP_CONTENT_TYPE', 'application/json');
        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');

        /** @var UserRepository $userRepository */
        $userRepository = self::getContainer()->get(UserRepository::class);
        /** @var CampaignRepository $campaignRepository */
        $campaignRepository = self::getContainer()->get(CampaignRepository::class);
        $this->campaignRepository = $campaignRepository;

        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['userName' => 'krupap']);
        $this->user = $testUser;
    }

    public function testAddCampaign(): void
    {
        $this->loginApiTestUser();
        $data = [
            'token' => $this->token,
            'campaignName' => 'New Campaign',
            'startDate' => '2024-02-14',
            'endDate' => '2024-05-15',
            'type' => 'whatsapp',
            'contentFrom' => 'Treść from',
            'contentText' => 'treść treść',
            'description' => 'opis',
            'promo' => true,
        ];

        $this->client->request('POST', self::URL.'/add', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testEditCampaign(): void
    {
        $this->loginApiTestUser();
        /** @var Campaign $campaign */
        $campaign = $this->campaignRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::ASC]);

        $data = [
            'token' => $this->token,
            'campaignName' => 'New Campaign Edit',
            'startDate' => '2024-02-14',
            'endDate' => '2025-02-15',
            'type' => 'whatsapp',
            'contentFrom' => 'Treść from Edit',
            'contentText' => 'treść treść Edit',
            'description' => 'opis Edit',
            'promo' => false,
        ];

        $this->client->request('PUT', self::URL.'/'.$campaign->getUuid().'/edit', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testShowCampaign(): void
    {
        $this->loginApiTestUser();
        /** @var Campaign $campaign */
        $campaign = $this->campaignRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::ASC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/'.$campaign->getUuid(), [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testShowAllCampaigns(): void
    {
        $this->loginApiTestUser();

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/list', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testShowAllActiveCampaigns(): void
    {
        $this->loginApiTestUser();

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/list-active', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testShowAllInactiveCampaigns(): void
    {
        $this->loginApiTestUser();

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/list-inactive', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testShowAllCreatedCampaigns(): void
    {
        $this->loginApiTestUser();

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/list-created', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testShowAllToSendCampaigns(): void
    {
        $this->loginApiTestUser();

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/list-to-send', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testDeactivateCampaign(): void
    {
        $this->loginApiTestUser();
        /** @var Campaign $campaign */
        $campaign = $this->campaignRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::ASC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('PUT', self::URL.'/'.$campaign->getUuid().'/deactivate', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testActivateCampaign(): void
    {
        $this->loginApiTestUser();
        /** @var Campaign $campaign */
        $campaign = $this->campaignRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::ASC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('PUT', self::URL.'/'.$campaign->getUuid().'/deactivate', [], [], [], (string) json_encode($data));
        $this->client->restart();

        $this->loginApiSuperUser();
        $this->client->request('PUT', self::URL.'/'.$campaign->getUuid().'/activate', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testChangeTypeCampaign(): void
    {
        $this->loginApiTestUser();
        /** @var Campaign $campaign */
        $campaign = $this->campaignRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::ASC]);

        $data = [
            'token' => $this->token,
            'type' => 'email',
        ];

        $this->client->request('POST', self::URL.'/'.$campaign->getUuid().'/change-type', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testChangePromoCampaign(): void
    {
        $this->loginApiTestUser();
        /** @var Campaign $campaign */
        $campaign = $this->campaignRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::ASC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('PUT', self::URL.'/'.$campaign->getUuid().'/change-promo', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testSendCampaign(): void
    {
        $this->loginApiTestUser();
        /** @var Campaign $campaign */
        $campaign = $this->campaignRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::ASC]);

        $data = [
            'token' => $this->token,
            'campaignUuid' => $campaign->getUuid(),
            'leads' => 'all',
            'date' => '2024-02-21 15:00:00',
            'description' => 'opis',
        ];

        $this->client->request('POST', self::URL.'/send', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testDeleteCampaign(): void
    {
        $this->loginApiTestUser();
        /** @var Campaign $campaign */
        $campaign = $this->campaignRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::ASC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('DELETE', self::URL.'/'.$campaign->getUuid().'/delete', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }
}
