<?php

declare(strict_types=1);

namespace App\Tests\Application\Api;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class ResetPasswordControllerTest extends WebTestCase
{
    private const URL = 'api/reset-password/';

    public KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = $this->createClient();
        $this->client->setServerParameter('content-type', 'application/json');
        $this->client->setServerParameter('HTTP_CONTENT_TYPE', 'application/json');
        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');
    }

    public function testResetPasswordRequest(): void
    {
        $this->client->request('POST', self::URL, [], [], [], (string) json_encode(['username' => 'krupap']));

        $response = $this->client->getResponse();

        $this->assertResponseIsSuccessful();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertTrue($content['Success']);
    }
}
