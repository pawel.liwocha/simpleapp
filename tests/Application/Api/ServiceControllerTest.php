<?php

declare(strict_types=1);

namespace App\Tests\Controller\Api;

use App\Entity\CategoryService;
use App\Entity\Service;
use App\Entity\User;
use App\Repository\CategoryServiceRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Traits\LoginTrait;

/**
 * @internal
 *
 * @coversNothing
 */
final class ServiceControllerTest extends WebTestCase
{
    use LoginTrait;

    private KernelBrowser $client;

    private ServiceRepository $ServiceRepository;

    private CategoryServiceRepository $categoryServiceRepository;

    private const URL = 'service';

    protected function setUp(): void
    {
        $this->client = $this->createClient();
        $this->client->setServerParameter('content-type', 'application/json');
        $this->client->setServerParameter('HTTP_CONTENT_TYPE', 'application/json');
        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');

        /** @var UserRepository $userRepository */
        $userRepository = self::getContainer()->get(UserRepository::class);
        /** @var ServiceRepository $ServiceRepository */
        $ServiceRepository = self::getContainer()->get(ServiceRepository::class);
        $this->ServiceRepository = $ServiceRepository;
        /** @var CategoryServiceRepository $categoryServiceRepository */
        $categoryServiceRepository = self::getContainer()->get(CategoryServiceRepository::class);
        $this->categoryServiceRepository = $categoryServiceRepository;

        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['userName' => 'krupap']);
        $this->user = $testUser;
    }

    public function testAddService(): void
    {
        $this->loginApiTestUser();
        /** @var CategoryService $categoryService */
        $categoryService = $this->categoryServiceRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::DESC]);

        $data = [
            'token' => $this->token,
            'name' => 'Test Service',
            'categoryServiceUuid' => $categoryService->getUuid(),
            'active' => 'true',
        ];

        $this->client->request('POST', self::URL.'/add', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testEditService(): void
    {
        $this->loginApiTestUser();
        /** @var Service $Service */
        $Service = $this->ServiceRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::DESC]);

        $data = [
            'token' => $this->token,
            'name' => 'Test Service Edit',
            'active' => 'true',
        ];

        $this->client->request('PUT', self::URL.'/'.$Service->getUuid().'/edit', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testShowService(): void
    {
        $this->loginApiTestUser();
        /** @var Service $Service */
        $Service = $this->ServiceRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::DESC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/'.$Service->getUuid(), [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testShowAllServices(): void
    {
        $this->loginApiTestUser();

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/list-all', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testShowAllActiveServices(): void
    {
        $this->loginApiTestUser();

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('GET', self::URL.'/list-active', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
    }

    public function testActive(): void
    {
        $this->loginApiTestUser();
        /** @var Service $Service */
        $Service = $this->ServiceRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::DESC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('PUT', self::URL.'/'.$Service->getUuid().'/active', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testDeactive(): void
    {
        $this->loginApiTestUser();
        /** @var Service $Service */
        $Service = $this->ServiceRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::DESC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('PUT', self::URL.'/'.$Service->getUuid().'/deactivate', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }

    public function testDelete(): void
    {
        $this->loginApiTestUser();
        /** @var Service $Service */
        $Service = $this->ServiceRepository->findOneBy(['createdBy' => $this->user], ['id' => Criteria::DESC]);

        $data = [
            'token' => $this->token,
        ];

        $this->client->request('DELETE', self::URL.'/'.$Service->getUuid().'/delete', [], [], [], (string) json_encode($data));

        $response = $this->client->getResponse();
        $content = (array) json_decode((string) $response->getContent(), true);

        self::assertResponseIsSuccessful();
        self::assertTrue($content['Success']);
    }
}
