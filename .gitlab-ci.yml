# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
default:
  image: php:8.2
  services:
    - name: mariadb
      alias: mysql
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    #- public/build/
    - vendor/

variables:
  MYSQL_DATABASE_DRIVER: pdo_mysql
  MYSQL_ROOT_PASSWORD: simpleapp
  MYSQL_DATABASE: simpleapp_test
  MYSQL_USER: simpleapp
  MYSQL_PASSWORD: simpleapp
  MYSQL_HOST: mysql
  # symfony 4
  DATABASE_URL: 'mysql://simpleapp:simpleapp@mysql:3306/simpleapp_test'
  DATABASE_DRIVER: pdo_mysql
  # symfony 3.2+
  TEST_DATABASE_PORT: 3306
  TEST_DATABASE_HOST: mysql

stages:
  - build
  - analysis
  - tests

build:
  stage: build
  script:
    - bash scripts/ci-php-install.sh > /dev/null
    - wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig
    - php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    - php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    - php composer-setup.php --install-dir=/usr/local/bin --filename=composer
    - php -r "unlink('composer-setup.php'); unlink('installer.sig');"
    - composer install
    - rm -rf .env
    - cp .env.test .env
    - composer validate

phpStan:
  stage: analysis
  script:
    - echo 'memory_limit = 512M' >> /usr/local/etc/php/php.ini
    - php vendor/bin/phpstan analyse --verbose --memory-limit=-1 -c phpstan.neon
  artifacts:
    when: always

cs-fixer:
  stage: analysis
  script:
    - echo 'memory_limit = 512M' >> /usr/local/etc/php/php.ini
    - php vendor/bin/php-cs-fixer fix --verbose --diff --config .php-cs-fixer.dist.php --dry-run
  artifacts:
    when: always

rector:
  stage: analysis
  script:
    - echo 'memory_limit = 512M' >> /usr/local/etc/php/php.ini
    - php vendor/bin/rector process -c rector.php --dry-run
  artifacts:
    when: always

tests:
  stage: tests
  before_script:
    - bash scripts/ci-php-install.sh > /dev/null
    - wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig
    - php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    - php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    - php composer-setup.php --install-dir=/usr/local/bin --filename=composer
    - php -r "unlink('composer-setup.php'); unlink('installer.sig');"
    - rm -rf .env
    - cp .env.test .env
    - composer install
    - php bin/console cache:clear --env=test --no-interaction
    - php bin/console doctrine:database:create --env=test --if-not-exists --no-interaction
    - php bin/console doctrine:migrations:migrate --env=test --no-interaction
    - php bin/console doctrine:fixtures:load --env=test --no-interaction
    - symfony server:start -d --no-tls
  script:
    - vendor/bin/phpunit tests --stop-on-failure
  artifacts:
    when: always
