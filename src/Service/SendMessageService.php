<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Service;

use App\Entity\CampaignContent;
use App\Entity\CampaignSentToLead;
use App\Exception\ApiResponseException;
use App\Infrastructure\WhatsApp\Response\TextMessageResponse;
use App\Infrastructure\WhatsApp\WhatsApp;
use App\Repository\CampaignContentRepository;
use Psr\Log\LoggerInterface;

class SendMessageService
{
    private readonly WhatsApp $whatsApp;

    public function __construct(
        string $whatsAppLive,
        string $whatsAppVersion,
        int $whatsAppPhoneNumberId,
        private readonly CampaignContentRepository $campaignContentRepository,
        private readonly LoggerInterface $loggerSendCampaignWhatsapp
    ) {
        $this->whatsApp = new WhatsApp(
            $whatsAppLive,
            $whatsAppVersion,
            $whatsAppPhoneNumberId
        );
    }

    public function sendViaWhatsApp(CampaignSentToLead $campaignSentToLead): TextMessageResponse
    {
        $content = $this->campaignContentRepository->findBy(['campaign' => $campaignSentToLead->getCampaign(), 'deleted' => false]);

        if ($content instanceof CampaignContent) {
            try {
                $send = $this->whatsApp->textMessage([
                    'to' => $campaignSentToLead->getLead()->getMobileNumber(),
                    'body' => $content->getText(),
                ]);
            } catch (ApiResponseException|\Exception $exception) {
                $this->loggerSendCampaignWhatsapp->error('Send campaign via Whatsapp error: '.$exception->getMessage().PHP_EOL);
                throw new ApiResponseException($exception->getMessage());
            }

            file_put_contents('sendViaWhatsApp.txt', var_export($send, true));

            return $send;
        }

        throw new \Exception('Cant find Content to Campaign');
    }
}
