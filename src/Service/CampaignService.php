<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Service;

use App\Entity\Campaign;
use App\Entity\CampaignSentToLead;
use App\Entity\Lead;
use App\Entity\User;
use App\Enum\CampaignSentToLeadStatus;
use App\Repository\LeadRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class CampaignService
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface $loggerSendCampaignWhatsapp,
        private readonly LoggerInterface $loggerSendCampaignSms,
        private readonly LoggerInterface $loggerSendCampaignEmail,
        private readonly LeadRepository $leadRepository
    ) {
    }

    public function prepareSendViaWhatsapp(Campaign $campaign, User $user, object $content): bool
    {
        try {
            $this->savePrepareData($campaign, $user, $content);
        } catch (\Exception $exception) {
            $this->loggerSendCampaignWhatsapp->error('PrepareSendViaWhatsapp error: '.$exception->getMessage().PHP_EOL);

            return false;
        }

        return true;
    }

    public function prepareSendViaSms(Campaign $campaign, User $user, object $content): bool
    {
        try {
            $this->savePrepareData($campaign, $user, $content);
        } catch (\Exception $exception) {
            $this->loggerSendCampaignSms->error('PrepareSendViaSms error: '.$exception->getMessage().PHP_EOL);

            return false;
        }

        return true;
    }

    public function prepareSendViaEmail(Campaign $campaign, User $user, object $content): bool
    {
        try {
            $this->savePrepareData($campaign, $user, $content);
        } catch (\Exception $exception) {
            $this->loggerSendCampaignEmail->error('PrepareSendViaEmail error: '.$exception->getMessage().PHP_EOL);

            return false;
        }

        return true;
    }

    private function savePrepareData(Campaign $campaign, User $user, object $content): bool
    {
        $leads = $this->getLeadsArray($content->leads, $campaign->getCampaignType()->value);
        $dateSend = new \DateTimeImmutable((string) $content->date);
        $return = false;

        if ($leads !== []) {
            /** @var Lead $lead */
            foreach ($leads as $lead) {
                $campaignSentToUser = new CampaignSentToLead();
                $campaignSentToUser->setCampaign($campaign);
                $campaignSentToUser->setLead($lead);
                $campaignSentToUser->setStatus(CampaignSentToLeadStatus::PLANNED);
                $campaignSentToUser->setSendDate($dateSend);
                $campaignSentToUser->setSentDate(null);
                $campaignSentToUser->setDescription(isset($content->description) ? htmlspecialchars(trim((string) $content->description)) : null);
                $campaignSentToUser->setMessageWhatsAppId(null);
                $campaignSentToUser->setMessageSmsId(null);
                $campaignSentToUser->setMessageEmailId(null);
                $campaignSentToUser->setCreatedBy($user);
                $campaignSentToUser->setDeleted(false);

                $lead->setNextContactDate($dateSend);

                $this->em->persist($campaignSentToUser);
                $this->em->persist($lead);

                $return = true;
            }

            $this->em->flush();
        }

        return $return;
    }

    private function getLeadsArray(array|string $leads, string $type): array
    {
        $leadsReturn = [];

        if (\is_array($leads)) {
            $leadsReturn = $this->leadRepository->findLeadsViaUuid($leads);
        } elseif ($type === 'whatsapp' || $type === 'sms' || $type === 'coldcall') {
            $leadsReturn = $this->leadRepository->findLeadsWithPhoneNumber();
        } elseif ($type === 'email') {
            $leadsReturn = $this->leadRepository->findLeadsWithEmail();
        }

        return $leadsReturn;
    }
}
