<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Service;

use App\Entity\FailureLoginAttempt;
use App\Entity\Token;
use App\Entity\User;
use App\Repository\FailureLoginAttemptRepository;
use App\Repository\TokenRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class SecurityService
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly TokenRepository $tokensRepository,
        private readonly FailureLoginAttemptRepository $failureLoginAttemptRepository,
        private readonly string $ttlToken,
        private readonly int $maxLoginAttempts,
        private readonly int $loginAttemptsTimeout
    ) {
    }

    public function checkValidDataToken(Request $request): bool
    {
        $content = json_decode($request->getContent());
        if (!isset($content->token)) {
            return false;
        }

        $token = $this->tokensRepository->findOneBy(['token' => (string) $content->token], ['id' => 'DESC']);
        if (!$token instanceof Token) {
            return false;
        }

        $now = new \DateTimeImmutable('now');
        $startDate = $token->getCreatedAt();
        $endDate = $token->getExpired();

        if ($startDate <= $now && $now <= $endDate) {
            $dateExpired = $now->modify('+'.$this->ttlToken.' minutes');
            $token->setExpired($dateExpired);
            $this->em->persist($token);
            $this->em->flush();

            return true;
        }

        return false;
    }

    public function generateToken(User $user): Token
    {
        $date = new \DateTimeImmutable('now');
        $token = uniqid('', true).$date->getTimestamp().md5($user->getEmail());
        $token = hash('sha512', $token);

        $dateExpired = $date->modify('+'.$this->ttlToken.' minutes');

        $tokenEntity = new Token();
        $tokenEntity->setToken($token);
        $tokenEntity->setUser($user);
        $tokenEntity->setExpired($dateExpired);

        $this->em->persist($tokenEntity);
        $this->em->flush();

        return $tokenEntity;
    }

    public function canLogin(?Request $request = null): bool
    {
        if ($request instanceof Request) {
            $content = json_decode($request->getContent());
            $username = (string) $content->username;
            $startWatchDate = new \DateTimeImmutable();

            if ($this->failureLoginAttemptRepository->getCountAttempts($username, $startWatchDate->modify('-180 second')) >= $this->maxLoginAttempts) {
                $lastAttemptDate = $this->failureLoginAttemptRepository->getLastAttempt($username);
                if ($lastAttemptDate instanceof FailureLoginAttempt) {
                    $dateAllowLogin = $lastAttemptDate->getCreatedAt()->modify('+'.$this->loginAttemptsTimeout.' second');

                    if ($dateAllowLogin->diff(new \DateTimeImmutable())->invert === 1) {
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }

    public function generatePassword(int $length = 16): string
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; ++$i) {
            $index = random_int(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }
}
