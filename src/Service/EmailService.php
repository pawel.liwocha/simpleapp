<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Service;

use App\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class EmailService
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly LoggerInterface $logger
    ) {
    }

    public function sendPasswordReset(User $user, array $data): bool
    {
        $message = (new TemplatedEmail())
            ->subject('Reset password in SimpleApp')
            ->from(new Address('no-reply@liwocha.pl', 'SimpleApp'))
            ->to(new Address($user->getEmail()))
            ->htmlTemplate('emails/reset_password_email.html.twig')
            ->context($data)
        ;

        try {
            $this->mailer->send($message);
        } catch (\Exception|TransportExceptionInterface $exception) {
            $this->logger->error('Error while sending reset password message');
            $this->logger->error($exception);

            return false;
        }

        return true;
    }

    public function sendNewUser(User $user, array $data): bool
    {
        $message = (new TemplatedEmail())
            ->subject('Welcome in SimpleApp')
            ->from(new Address('no-reply@liwocha.pl', 'SimpleApp'))
            ->to(new Address($user->getEmail()))
            ->htmlTemplate('emails/new_user.html.twig')
            ->context($data)
        ;

        try {
            $this->mailer->send($message);
        } catch (\Exception|TransportExceptionInterface $exception) {
            $this->logger->error('Error while sending reset password message');
            $this->logger->error($exception);

            return false;
        }

        return true;
    }
}
