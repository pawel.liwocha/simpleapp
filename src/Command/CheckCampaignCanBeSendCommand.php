<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Command;

use App\Enum\CampaignSentToLeadStatus;
use App\Message\SendCampaignMessage;
use App\Repository\CampaignSentToLeadRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsCommand(name: 'simpleapp:check-campaign-can-be-send', description: 'Check Campaign can be send to leads.')]
class CheckCampaignCanBeSendCommand extends Command
{
    public function __construct(
        private readonly CampaignSentToLeadRepository $campaignSentToLeadRepository,
        private readonly MessageBusInterface $messageBus,
        private readonly LoggerInterface $loggerCommandSendCampaign,
        private readonly string $logsDir
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    /**
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->setLastProcessTime(time());
        $error = false;

        $output->writeln('Check Campaign can be send Command started - '.$this->getCurrentTime()->format('Y-m-d H:i:s'));

        $campaignsSentToLead = $this->campaignSentToLeadRepository->findByDateAndStatus($this->getCurrentTime(), CampaignSentToLeadStatus::PLANNED);

        if ($campaignsSentToLead !== []) {
            try {
                foreach ($campaignsSentToLead as $campaignSentToLead) {
                    $this->messageBus->dispatch(
                        new SendCampaignMessage(
                            $campaignSentToLead,
                            $this->getCurrentTime()
                        )
                    );
                }
            } catch (\Exception $exception) {
                $this->setLastErrorTime(time());
                $error = true;

                $this->loggerCommandSendCampaign->error('Command CheckCampaignCanBeSendCommand has error: '.$exception->getMessage());
            }
        }

        $output->writeln('Check Campaign can be send Command ended - '.$this->getCurrentTime()->format('Y-m-d H:i:s'));

        if ($error) {
            $output->writeln('WITH ERROR !!');

            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }

    private function getCurrentTime(): \DateTimeImmutable
    {
        return new \DateTimeImmutable('now');
    }

    private function setLastProcessTime(int $timestamp): void
    {
        $filePath = $this->logsDir.'/check-send-campaign-command/';
        if (!is_dir($filePath)) {
            mkdir($filePath, 0o775, true);
        }

        file_put_contents($filePath.'lastProcessTime.txt', $timestamp);
    }

    private function setLastErrorTime(int $timestamp): void
    {
        $filePath = $this->logsDir.'/check-send-campaign-command/';
        if (!is_dir($filePath)) {
            mkdir($filePath, 0o775, true);
        }

        file_put_contents($filePath.'lastErrorTime.txt', $timestamp);
    }
}
