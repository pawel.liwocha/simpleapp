<?php

declare(strict_types=1);

namespace App\Enum;

use App\Trait\Enum;

enum CampaignType: string
{
    use Enum;

    case SMS = 'sms';
    case EMAIL = 'email';
    case WHATSAPP = 'whatsapp';
    case COLDCALL = 'coldcall';
}
