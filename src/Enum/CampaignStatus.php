<?php

declare(strict_types=1);

namespace App\Enum;

use App\Trait\Enum;

enum CampaignStatus: string
{
    use Enum;

    case CREATED = 'created';
    case ACTIVE = 'active';
    case INACTIVE = 'inactive';
}
