<?php

declare(strict_types=1);

namespace App\Enum;

use App\Trait\Enum;

enum UserStatus: string
{
    use Enum;

    case ACTIVE = 'active';
    case INACTIVE = 'inactive';
}
