<?php

declare(strict_types=1);

namespace App\Enum;

use App\Trait\Enum;

enum CampaignSentToLeadStatus: string
{
    use Enum;

    case SEND = 'send';
    case WAIT = 'wait';
    case PLANNED = 'planned';
    case ERROR = 'error';
}
