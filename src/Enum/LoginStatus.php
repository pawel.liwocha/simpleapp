<?php

declare(strict_types=1);

namespace App\Enum;

use App\Trait\Enum;

enum LoginStatus: string
{
    use Enum;

    case SUCCESS = 'success';
    case FAIL = 'fail';
}
