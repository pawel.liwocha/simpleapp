<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\FailureLoginAttempt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FailureLoginAttempt|null find($id, $lockMode = null, $lockVersion = null)
 * @method FailureLoginAttempt|null findOneBy(array $criteria, array $orderBy = null)
 * @method FailureLoginAttempt[]    findAll()
 * @method FailureLoginAttempt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FailureLoginAttemptRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FailureLoginAttempt::class);
    }

    public function remove(FailureLoginAttempt $attempt): void
    {
        $this->getEntityManager()->remove($attempt);
        $this->getEntityManager()->flush();
    }

    public function getCountAttempts(string $username, \DateTimeImmutable $startDate): int
    {
        $queryBuilder = $this->createQueryBuilder('attempt')
            ->select('COUNT(attempt.id)')
            ->where('attempt.username = :username')
            ->andWhere('attempt.createdAt >= :createdAt')
            ->setParameters(
                new ArrayCollection(
                    [
                        new Parameter('username', $username),
                        new Parameter('createdAt', $startDate),
                    ]
                )
            )
        ;

        return (int) $queryBuilder
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function getLastAttempt(string $username): ?FailureLoginAttempt
    {
        return $this->createQueryBuilder('attempt')
            ->where('attempt.username = :username')
            ->setParameter('username', $username)
            ->orderBy('attempt.createdAt', 'DESC')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()
        ;
    }

    public function clearAttempts(?string $username = null): void
    {
        if ($username) {
            $sql = 'DELETE FROM failure_login_attempt WHERE username = :username';
            $parameters = ['username' => $username];

            $command = $this->getEntityManager()->getConnection()->prepare($sql);
            $command->executeQuery($parameters);
        }
    }

    // /**
    //  * @return FailureLoginAttempt[] Returns an array of FailureLoginAttempt objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FailureLoginAttempt
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
