<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ParentCategoryService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ParentCategoryService>
 *
 * @method ParentCategoryService|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParentCategoryService|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParentCategoryService[]    findAll()
 * @method ParentCategoryService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParentCategoryServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParentCategoryService::class);
    }

    //    /**
    //     * @return ParentCategoryService[] Returns an array of ParentCategoryService objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ParentCategoryService
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
