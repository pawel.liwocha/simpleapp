<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Lead;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Lead>
 *
 * @method Lead|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lead|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lead[]    findAll()
 * @method Lead[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeadRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lead::class);
    }

    /**
     * @return Lead[] Returns an array of Lead objects
     */
    public function findDuplicateLead(?string $email = null, ?string $mobileNumber = null): array
    {
        $return = $this->createQueryBuilder('l');

        if ($email && $email !== '') {
            $return->orWhere('l.email = :email')
                ->setParameter('email', $email)
            ;
        }

        if ($mobileNumber && $mobileNumber !== '') {
            $return->orWhere('l.mobileNumber = :mobile_number')
                ->setParameter('mobile_number', $mobileNumber)
            ;
        }

        return $return
            ->andWhere('l.deleted = false')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Lead[] Returns an array of Lead objects
     */
    public function findLeadsWithPhoneNumber(): array
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.mobileNumber IS NOT NULL')
            ->andWhere('l.deleted = false')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Lead[] Returns an array of Lead objects
     */
    public function findLeadsViaUuid(array $leads): array
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.uuid IN (:uuids)')
            ->andWhere('l.deleted = false')
            ->setParameter('uuids', $leads)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Lead[] Returns an array of Lead objects
     */
    public function findLeadsWithEmail(): array
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.email IS NOT NULL')
            ->andWhere('l.deleted = false')
            ->getQuery()
            ->getResult()
        ;
    }

    //    /**
    //     * @return Lead[] Returns an array of Lead objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('l')
    //            ->andWhere('l.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('l.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Lead
    //    {
    //        return $this->createQueryBuilder('l')
    //            ->andWhere('l.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
