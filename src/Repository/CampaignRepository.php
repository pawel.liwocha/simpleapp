<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Campaign;
use App\Enum\CampaignStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Campaign>
 *
 * @method Campaign|null find($id, $lockMode = null, $lockVersion = null)
 * @method Campaign|null findOneBy(array $criteria, array $orderBy = null)
 * @method Campaign[]    findAll()
 * @method Campaign[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CampaignRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Campaign::class);
    }

    /**
     * @return Campaign[]
     */
    public function findCampaignsInDateRange(Campaign $campaign, \DateTimeImmutable $startDate, \DateTimeImmutable $endDate): array
    {
        return $this->createQueryBuilder('e')
            ->orWhere('e.startDate BETWEEN :startDate AND :endDate')
            ->orWhere('e.endDate BETWEEN :startDate AND :endDate')
            ->orWhere('e.startDate <= :startDate AND e.endDate >= :endDate')
            ->andWhere('e.campaign = :campaign')
            ->andWhere('e.deleted = false')
            ->setParameter('campaign', $campaign)
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findCampaignInactiveOrCreated(string $uuid): ?Campaign
    {
        return $this->createQueryBuilder('e')
            ->orWhere('e.status = :inactive')
            ->orWhere('e.status = :created')
            ->andWhere('e.uuid = :uuid')
            ->andWhere('e.deleted = false')
            ->setParameter('uuid', $uuid)
            ->setParameter('inactive', CampaignStatus::INACTIVE)
            ->setParameter('created', CampaignStatus::CREATED)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findCampaignActiveOrCreated(string $uuid): ?Campaign
    {
        return $this->createQueryBuilder('e')
            ->orWhere('e.status = :active')
            ->orWhere('e.status = :created')
            ->andWhere('e.uuid = :uuid')
            ->andWhere('e.deleted = false')
            ->setParameter('uuid', $uuid)
            ->setParameter('active', CampaignStatus::ACTIVE)
            ->setParameter('created', CampaignStatus::CREATED)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return Campaign[] Returns an array of Campaign objects
     */
    public function findAllCampaignsWithStatusActiveOrCreated(): array
    {
        return $this->createQueryBuilder('e')
            ->orWhere('e.status = :active')
            ->orWhere('e.status = :created')
            ->andWhere('e.deleted = false')
            ->setParameter('active', CampaignStatus::ACTIVE)
            ->setParameter('created', CampaignStatus::CREATED)
            ->getQuery()
            ->getResult()
        ;
    }

    //    /**
    //     * @return Campaign[] Returns an array of Campaign objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Campaign
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
