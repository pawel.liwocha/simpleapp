<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Campaign;
use App\Entity\CampaignSentToLead;
use App\Entity\Lead;
use App\Enum\CampaignSentToLeadStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CampaignSentToLead>
 *
 * @method CampaignSentToLead|null find($id, $lockMode = null, $lockVersion = null)
 * @method CampaignSentToLead|null findOneBy(array $criteria, array $orderBy = null)
 * @method CampaignSentToLead[]    findAll()
 * @method CampaignSentToLead[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CampaignSentToLeadRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CampaignSentToLead::class);
    }

    /**
     * @return CampaignSentToLead[]
     */
    public function findByDateAndStatus(\DateTimeImmutable $startDate, CampaignSentToLeadStatus $status): array
    {
        $dateNow = $startDate;
        $dateFiveMinutesEarlier = $startDate->modify('-1 day');

        return $this->createQueryBuilder('e')
            ->innerJoin(Campaign::class, 'c', Join::WITH, 'c.id = e.campaign')
            ->innerJoin(Lead::class, 'l', Join::WITH, 'l.id = e.lead')
            ->andWhere('e.sendDate <= :now AND e.sendDate >= :earlier')
            ->andWhere('e.status = :status')
            ->andWhere('e.deleted = false')
            ->setParameter('now', $dateNow)
            ->setParameter('earlier', $dateFiveMinutesEarlier)
            ->setParameter('status', $status)
            ->getQuery()
            ->getResult()
        ;
    }

    //    /**
    //     * @return CampaignSentToLead[] Returns an array of CampaignSentToLead objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?CampaignSentToLead
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
