<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Infrastructure\WhatsApp\Response;

class TextMessageResponse extends ApiResponse
{
    protected ?string $messaging_product = null;

    private ?array $contacts = [];

    private ?array $messages = [];

    public function getMessagingProduct(): ?string
    {
        return $this->messaging_product;
    }

    public function setMessagingProduct(?string $messagingProduct): self
    {
        $this->messaging_product = $messagingProduct;

        return $this;
    }

    public function getContacts(): ?array
    {
        return $this->contacts;
    }

    public function setContacts(?array $contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    public function getMessages(): ?array
    {
        return $this->messages;
    }

    public function setMessages(?array $messages): self
    {
        $this->messages = $messages;

        return $this;
    }
}
