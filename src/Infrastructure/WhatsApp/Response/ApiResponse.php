<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Infrastructure\WhatsApp\Response;

use App\Exception\ApiResponseException;
use Psr\Http\Message\ResponseInterface;

abstract class ApiResponse
{
    protected string $error;

    protected string $errorMessage = '';

    /**
     * @throws ApiResponseException
     */
    public function __construct(ResponseInterface $response)
    {
        $contents = $response->getBody();

        parse_str($contents, $parameters);

        foreach ($parameters as $key => $value) {
            if (property_exists($this, (string) $key)) {
                $this->$key = $value;
            }
        }

        if ($this->hasError()) {
            throw new ApiResponseException($this->getError());
        }
    }

    protected function hasError(): bool
    {
        return isset($this->error) && ($this->error !== '' && $this->error !== '0');
    }

    protected function getError(): string
    {
        return $this->error.' - '.$this->errorMessage;
    }
}
