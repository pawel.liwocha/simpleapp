<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Infrastructure\WhatsApp\Model;

class TextMessageModel
{
    private array $data = [];

    public function __construct(string $to, string $body)
    {
        $this->prepareData();

        $this->data['to'] = $to;
        $this->data['text']['body'] = $body;
    }

    private function prepareData(): void
    {
        $this->data['messaging_product'] = 'whatsapp';
        $this->data['recipient_type'] = 'individual';
        $this->data['type'] = 'text';
        $this->data['text']['preview_url'] = false;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
