<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Infrastructure\WhatsApp\Api;

use App\Exception\ApiResponseException;
use App\Infrastructure\WhatsApp\Model\TextMessageModel;
use App\Infrastructure\WhatsApp\Response\TestConnectionResponse;
use App\Infrastructure\WhatsApp\Response\TextMessageResponse;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class Api
{
    final public const URL_LIVE = 'https://graph.facebook.com';

    final public const URL_SANDBOX = 'https://graph.facebook.com';

    final public const ENDPOINT_MESSAGE = 'messages';

    private readonly ClientInterface $client;

    private readonly bool $live;

    public function __construct(
        string $whatsAppLive,
        string $whatsAppVersion,
        int $whatsAppPhoneNumberId
    ) {
        $this->live = filter_var($whatsAppLive, FILTER_VALIDATE_BOOLEAN);

        $this->client = new GuzzleClient([
            'base_uri' => $this->getApiUrl().'/'.$whatsAppVersion.'/'.$whatsAppPhoneNumberId.'/',
            RequestOptions::CONNECT_TIMEOUT => 10,
            RequestOptions::TIMEOUT => 30,
        ]);
    }

    /**
     * @throws ApiResponseException
     */
    public function testConnection(): TestConnectionResponse
    {
        $response = $this->request(self::ENDPOINT_MESSAGE, [
            'messaging_product' => 'whatsapp',
            'recipient_type' => 'individual',
            'type' => 'text',
            'to' => '+48500500500',
            'text' => [
                'preview_url' => false,
                'body' => 'Test Connection and send WhatsApp message - SimpleApp',
            ],
        ]);

        return new TestConnectionResponse($response);
    }

    /**
     * @throws ApiResponseException
     */
    public function sendTextMessage(TextMessageModel $textMessageRequest): TextMessageResponse
    {
        return new TextMessageResponse(
            $this->request(self::ENDPOINT_MESSAGE, $textMessageRequest->getData())
        );
    }

    private function client(): ClientInterface
    {
        return $this->client;
    }

    private function request(string $endpoint, array $parameters): ResponseInterface
    {
        return $this->client()->post($endpoint, $parameters);
    }

    private function getApiUrl(): string
    {
        return $this->live ? self::URL_LIVE : self::URL_SANDBOX;
    }
}
