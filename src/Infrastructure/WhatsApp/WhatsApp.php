<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Infrastructure\WhatsApp;

use App\Exception\ApiResponseException;
use App\Infrastructure\WhatsApp\Api\Api;
use App\Infrastructure\WhatsApp\Model\TextMessageModel;
use App\Infrastructure\WhatsApp\Response\TestConnectionResponse;
use App\Infrastructure\WhatsApp\Response\TextMessageResponse;

class WhatsApp
{
    private readonly Api $api;

    public function __construct(
        string $whatsAppLive,
        string $whatsAppVersion,
        int $whatsAppPhoneNumberId
    ) {
        $this->api = new Api($whatsAppLive, $whatsAppVersion, $whatsAppPhoneNumberId);
    }

    /**
     * @throws ApiResponseException
     */
    public function testConnection(): TestConnectionResponse
    {
        return $this->api->testConnection();
    }

    /**
     * @throws ApiResponseException
     */
    public function textMessage(array|TextMessageModel $message): TextMessageResponse
    {
        if (\is_array($message)) {
            $message = new TextMessageModel($message['to'], $message['body']);
        }

        return $this->api->sendTextMessage($message);
    }
}
