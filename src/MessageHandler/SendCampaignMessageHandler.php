<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\MessageHandler;

use App\Enum\CampaignSentToLeadStatus;
use App\Enum\CampaignType;
use App\Message\SendCampaignMessage;
use App\Service\SendMessageService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class SendCampaignMessageHandler
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface $loggerSendCampaignWhatsapp,
        private readonly LoggerInterface $loggerSendCampaignSms,
        private readonly LoggerInterface $loggerSendCampaignEmail,
        private readonly SendMessageService $sendMessageService
    ) {
    }

    public function __invoke(SendCampaignMessage $sendCampaignMessage): void
    {
        $campaignSentToLead = $sendCampaignMessage->getCampaignSentToLead();
        $date = $sendCampaignMessage->getDate() ?? new \DateTimeImmutable('now');
        $date = $date->setTime(0, 0, 0);

        $campaign = $campaignSentToLead->getCampaign();
        $lead = $campaignSentToLead->getLead();

        try {
            if ($campaign->getCampaignType() === CampaignType::WHATSAPP) {
                try {
                    $this->sendMessageService->sendViaWhatsApp($campaignSentToLead);
                } catch (\Exception $exception) {
                    $this->loggerSendCampaignWhatsapp->error('Send campaign via Whatsapp error: '.$exception->getMessage().PHP_EOL);
                }

                $this->loggerSendCampaignWhatsapp->info('Send campaign via Whatsapp successfully: '.$campaign->getCampaignName().' ('.$campaign->getUuid().')'.PHP_EOL);
            }

            if ($campaign->getCampaignType() === CampaignType::SMS) {
                try {
                    // $send = $this->smsIntegration->send($campaign, $user, $content);
                    $send = true;
                } catch (\Exception $exception) {
                    $this->loggerSendCampaignSms->error('Send campaign via SMS error: '.$exception->getMessage().PHP_EOL);
                }

                $this->loggerSendCampaignSms->info('Send campaign via SMS successfully: '.$campaign->getCampaignName().' ('.$campaign->getUuid().')'.PHP_EOL);
            }

            if ($campaign->getCampaignType() === CampaignType::EMAIL) {
                try {
                    // $send = $this->emailIntegration->send($campaign, $user, $content);
                    $send = true;
                } catch (\Exception $exception) {
                    $this->loggerSendCampaignEmail->error('Send campaign via Email error: '.$exception->getMessage().PHP_EOL);
                }

                $this->loggerSendCampaignEmail->info('Send campaign via Email successfully: '.$campaign->getCampaignName().' ('.$campaign->getUuid().')'.PHP_EOL);
            }

            $campaignSentToLead->setSentDate(new \DateTimeImmutable('now'));
            $campaignSentToLead->setStatus(CampaignSentToLeadStatus::SEND);
            $campaignSentToLead->setUpdatedAt(new \DateTimeImmutable('now'));
            $this->em->persist($campaignSentToLead);

            $lead->setLastContactDate(new \DateTimeImmutable('now'));
            $lead->setUpdatedAt(new \DateTimeImmutable('now'));
            $this->em->persist($lead);

            try {
                $this->em->flush();
            } catch (\Exception $exception) {
                $this->loggerSendCampaignEmail->error('Save campaign and sentToLead flush Error: '.$exception->getMessage().PHP_EOL);
            }
        } catch (\Exception $exception) {
            throw new \Exception('Send campaign MAIN ERROR: '.$exception->getMessage().PHP_EOL.' Dispatch handle error campaign: '.$campaign->getUuid().', Date: '.$date->format('Y-m-d').'!', $exception->getCode(), $exception);
        }
    }
}
