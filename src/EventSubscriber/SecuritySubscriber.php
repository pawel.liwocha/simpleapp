<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\EventSubscriber;

use App\Entity\FailureLoginAttempt;
use App\Entity\LoginLog;
use App\Entity\User;
use App\Enum\LoginStatus;
use App\Exception\LoginAttemptException;
use App\Repository\FailureLoginAttemptRepository;
use App\Repository\TokenRepository;
use App\Repository\UserRepository;
use App\Service\SecurityService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Http\Event\CheckPassportEvent;
use Symfony\Component\Security\Http\Event\LoginFailureEvent;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;
use Symfony\Component\Security\Http\Event\LogoutEvent;

class SecuritySubscriber extends AbstractController implements EventSubscriberInterface
{
    private const SUBSCRIBER_PRIORITY = 10000;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly TokenRepository $tokenRepository,
        private readonly RequestStack $requestStack,
        private readonly FailureLoginAttemptRepository $failureLoginAttemptRepository,
        private readonly SecurityService $securityService,
        private readonly UserRepository $userRepository,
        private readonly int $loginAttemptsTimeout
    ) {
    }

    public function onLogout(LogoutEvent $event): void
    {
        $token = $event->getToken();
        $user = $token?->getUser();

        if ($user instanceof User) {
            $tokens = $this->tokenRepository->findBy(['user' => $user]);
            foreach ($tokens as $token) {
                $this->tokenRepository->remove($token);
            }
        }
    }

    public function onLoginFailure(LoginFailureEvent $event): void
    {
        $request = $this->requestStack->getCurrentRequest();
        if ($request instanceof Request) {
            $content = json_decode($request->getContent());
            $username = (string) $content->username;
            $data = [
                'exception' => $event->getException()->getMessage(),
                'clientIp' => $request->getClientIp() ?? null,
                'sessionId' => $request->hasSession() ? $request->getSession()->getId() : null,
            ];
            $user = $this->userRepository->findOneBy(['userName' => $username]);

            if ($user instanceof User) {
                $loginLog = new LoginLog();
                $loginLog->setUser($user);
                $loginLog->setToken($request->get('token') ? (string) $request->get('token') : 'fail');
                $loginLog->setUserAgent($request->headers->get('User-Agent'));
                $loginLog->setIp($_SERVER['HTTP_CF_CONNECTING_IP'] ?? ($_SERVER['REMOTE_ADDR'] ?? 'undefined'));
                $loginLog->setStatus(LoginStatus::FAIL);

                $this->em->persist($loginLog);
            }

            if (!$event->getException() instanceof LoginAttemptException) {
                $failureAttempt = new FailureLoginAttempt();
                $failureAttempt->setIp($request->getClientIp() ?? null);
                $failureAttempt->setUsername($username);
                $failureAttempt->setData($data);

                $this->em->persist($failureAttempt);
                $this->em->flush();
            }
        }
    }

    public function onLoginSuccess(LoginSuccessEvent $event): void
    {
        $request = $this->requestStack->getCurrentRequest();
        $content = json_decode((string) $request?->getContent());
        $username = (string) $content->username;
        $this->failureLoginAttemptRepository->clearAttempts($username);
    }

    public function onCheckPassport(CheckPassportEvent $event): void
    {
        $request = $this->requestStack->getCurrentRequest();

        if (!$this->securityService->canLogin($request)) {
            $minutes = $this->loginAttemptsTimeout / 60;
            throw new LoginAttemptException('Too many authentication failures. Account is blocked for '.$minutes.' minutes.');
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            LogoutEvent::class => ['onLogout', 0],
            LoginFailureEvent::class => ['onLoginFailure', self::SUBSCRIBER_PRIORITY],
            LoginSuccessEvent::class => ['onLoginSuccess', self::SUBSCRIBER_PRIORITY],
            CheckPassportEvent::class => ['onCheckPassport', self::SUBSCRIBER_PRIORITY],
        ];
    }
}
