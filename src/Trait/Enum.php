<?php

declare(strict_types=1);

namespace App\Trait;

trait Enum
{
    public static function from(string $value): self
    {
        $class = static::class;

        if (!\defined(sprintf('%s::%s', $class, $value))) {
            throw new \InvalidArgumentException(sprintf('Invalid %s value: %s', $class, $value));
        }

        return \constant(sprintf('%s::%s', $class, $value));
    }

    public static function names(): array
    {
        return array_column(self::cases(), 'name');
    }

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }

    public static function array(): array
    {
        return array_combine(self::values(), self::names());
    }
}
