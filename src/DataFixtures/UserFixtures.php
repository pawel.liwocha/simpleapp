<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use App\Enum\UserStatus;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture implements OrderedFixtureInterface
{
    final public const userSuperAdmin = 'userSuperAdmin';

    final public const userTest = 'userTest';

    public function __construct(
        private readonly UserPasswordHasherInterface $passwordHasher
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setPassword($this->passwordHasher->hashPassword($user, '[wd0]5VTdw!{4tE5-ZADj'));
        $user->setEmail('superadmin@liwocha.pl');
        $user->setUserName('superadmin');
        $user->setFirstName('Super');
        $user->setLastName('Admin');
        $user->setStatus(UserStatus::ACTIVE);
        $user->setRoles(['ROLE_SUPER_ADMIN', 'ROLE_USER']);
        $user->setDeleted(false);
        $user->setDeactivateAt(null);

        $user2 = new User();
        $user2->setPassword($this->passwordHasher->hashPassword($user2, 'le891$9Htr1K9O3Y%%7-7'));
        $user2->setEmail('test@liwocha.pl');
        $user2->setUserName('krupap');
        $user2->setFirstName('Piotr');
        $user2->setLastName('Krupa');
        $user2->setStatus(UserStatus::ACTIVE);
        $user2->setRoles(['ROLE_USER']);
        $user2->setDeleted(false);
        $user2->setDeactivateAt(null);

        $manager->persist($user);
        $manager->persist($user2);

        $manager->flush();
        $this->addReference(self::userSuperAdmin, $user);
        $this->addReference(self::userTest, $user2);
    }

    public function getOrder(): int
    {
        return 2;
    }
}
