<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Lead;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LeadFixtures extends Fixture implements OrderedFixtureInterface
{
    final public const lead_1 = 'lead_1';

    final public const lead_2 = 'lead_2';

    final public const lead_3 = 'lead_3';

    final public const lead_4 = 'lead_4';

    public function load(ObjectManager $manager): void
    {
        /** @var User $user */
        $user = $this->getReference('userTest');

        $lead = new Lead();
        $lead->setSourcedDate(new \DateTimeImmutable('now'));
        $lead->setSource('Whatsapp');
        $lead->setFirstName(null);
        $lead->setLastName('Test');
        $lead->setEmail('blablattt@tttesttets.pl');
        $lead->setMobileNumber('650650650');
        $lead->setInquiredTreatment('B12 injection ');
        $lead->setComment(null);
        $lead->setCreatedBy($user);
        $lead->setLastContactDate(null);
        $lead->setNextContactDate((new \DateTimeImmutable('now'))->modify('+1 month'));
        $lead->setDeleted(false);

        $lead2 = new Lead();
        $lead2->setSourcedDate((new \DateTimeImmutable('now'))->modify('-1 month'));
        $lead2->setSource('Direct Call');
        $lead2->setFirstName('Andrzej');
        $lead2->setLastName(null);
        $lead2->setEmail(null);
        $lead2->setMobileNumber('500500500');
        $lead2->setInquiredTreatment(null);
        $lead2->setComment('Test test test');
        $lead2->setLastContactDate((new \DateTimeImmutable('now'))->modify('-14 days'));
        $lead2->setNextContactDate((new \DateTimeImmutable('now'))->modify('+5 month'));
        $lead2->setCreatedBy($user);
        $lead2->setDeleted(false);

        $lead3 = new Lead();
        $lead3->setSourcedDate((new \DateTimeImmutable('now'))->modify('-14 days'));
        $lead3->setSource('Whatsapp');
        $lead3->setFirstName('Karolina');
        $lead3->setLastName('Test');
        $lead3->setEmail('kkarolina.ttestst@wwwwwwwawp.pl');
        $lead3->setMobileNumber('600600600');
        $lead3->setInquiredTreatment('scar removal');
        $lead3->setComment('Komentarz testowy');
        $lead3->setLastContactDate(null);
        $lead3->setNextContactDate((new \DateTimeImmutable('now'))->modify('+2 month'));
        $lead3->setCreatedBy($user);
        $lead3->setDeleted(false);

        $lead4 = new Lead();
        $lead4->setSourcedDate((new \DateTimeImmutable('now'))->modify('-8 days'));
        $lead4->setSource('Mail');
        $lead4->setFirstName('Greg');
        $lead4->setLastName('Test');
        $lead4->setEmail('greg.testowy@ttttests.eu');
        $lead4->setMobileNumber(null);
        $lead4->setInquiredTreatment('Tattoo removal');
        $lead4->setComment(null);
        $lead4->setLastContactDate((new \DateTimeImmutable('now'))->modify('-3 days'));
        $lead4->setNextContactDate((new \DateTimeImmutable('now'))->modify('+7 month'));
        $lead4->setCreatedBy($user);
        $lead4->setDeleted(false);

        $manager->persist($lead);
        $manager->persist($lead2);
        $manager->persist($lead3);
        $manager->persist($lead4);

        $manager->flush();
        $this->addReference(self::lead_1, $lead);
        $this->addReference(self::lead_2, $lead2);
        $this->addReference(self::lead_3, $lead3);
        $this->addReference(self::lead_4, $lead4);
    }

    public function getOrder(): int
    {
        return 5;
    }
}
