<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\CategoryService;
use App\Entity\ParentCategoryService;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CategoryServiceFixtures extends Fixture implements OrderedFixtureInterface
{
    final public const category_service_1 = 'category_service_1';

    final public const category_service_2 = 'category_service_2';

    final public const category_service_3 = 'category_service_3';

    final public const category_service_4 = 'category_service_4';

    final public const category_service_5 = 'category_service_5';

    final public const category_service_6 = 'category_service_6';

    public function load(ObjectManager $manager): void
    {
        /** @var User $user */
        $user = $this->getReference('userTest');
        /** @var ParentCategoryService $ParentCategoryService1 */
        $ParentCategoryService1 = $this->getReference('parent_category_service_1');
        /** @var ParentCategoryService $ParentCategoryService2 */
        $ParentCategoryService2 = $this->getReference('parent_category_service_2');
        /** @var ParentCategoryService $ParentCategoryService3 */
        $ParentCategoryService3 = $this->getReference('parent_category_service_3');

        $categoryService = new CategoryService();
        $categoryService->setParentCategoryService($ParentCategoryService1);
        $categoryService->setName('Facelift');
        $categoryService->setCreatedBy($user);
        $categoryService->setActive(true);
        $categoryService->setDeleted(false);

        $categoryService2 = new CategoryService();
        $categoryService2->setParentCategoryService($ParentCategoryService1);
        $categoryService2->setName('Brow/forehead lift');
        $categoryService2->setCreatedBy($user);
        $categoryService2->setActive(true);
        $categoryService2->setDeleted(false);

        $categoryService3 = new CategoryService();
        $categoryService3->setParentCategoryService($ParentCategoryService1);
        $categoryService3->setName('Eyelid lift');
        $categoryService3->setCreatedBy($user);
        $categoryService3->setActive(true);
        $categoryService3->setDeleted(false);

        $categoryService4 = new CategoryService();
        $categoryService4->setParentCategoryService($ParentCategoryService2);
        $categoryService4->setName('Breast augmentation');
        $categoryService4->setCreatedBy($user);
        $categoryService4->setActive(true);
        $categoryService4->setDeleted(false);

        $categoryService5 = new CategoryService();
        $categoryService5->setParentCategoryService($ParentCategoryService2);
        $categoryService5->setName('Breast reconstruction');
        $categoryService5->setCreatedBy($user);
        $categoryService5->setActive(true);
        $categoryService5->setDeleted(false);

        $categoryService6 = new CategoryService();
        $categoryService6->setParentCategoryService($ParentCategoryService3);
        $categoryService6->setName('Crowns/veneers');
        $categoryService6->setCreatedBy($user);
        $categoryService6->setActive(true);
        $categoryService6->setDeleted(false);

        $manager->persist($categoryService);
        $manager->persist($categoryService2);
        $manager->persist($categoryService3);
        $manager->persist($categoryService4);
        $manager->persist($categoryService5);
        $manager->persist($categoryService6);

        $manager->flush();
        $this->addReference(self::category_service_1, $categoryService);
        $this->addReference(self::category_service_2, $categoryService2);
        $this->addReference(self::category_service_3, $categoryService3);
        $this->addReference(self::category_service_4, $categoryService4);
        $this->addReference(self::category_service_5, $categoryService5);
        $this->addReference(self::category_service_6, $categoryService6);
    }

    public function getOrder(): int
    {
        return 8;
    }
}
