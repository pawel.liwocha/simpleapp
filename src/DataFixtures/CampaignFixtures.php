<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Campaign;
use App\Entity\User;
use App\Enum\CampaignStatus;
use App\Enum\CampaignType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CampaignFixtures extends Fixture implements OrderedFixtureInterface
{
    final public const campaign_1 = 'campaign_1';

    final public const campaign_2 = 'campaign_2';

    final public const campaign_3 = 'campaign_3';

    final public const campaign_4 = 'campaign_4';

    public function load(ObjectManager $manager): void
    {
        /** @var User $user */
        $user = $this->getReference('userTest');

        $campaign = new Campaign();
        $campaign->setCampaignName('Testowa kampania 1');
        $campaign->setStartDate(new \DateTimeImmutable('2024-01-05 15:00:00'));
        $campaign->setEndDate(new \DateTimeImmutable('2029-05-15 15:00:00'));
        $campaign->setCampaignType(CampaignType::WHATSAPP);
        $campaign->setDescription(null);
        $campaign->setStatus(CampaignStatus::CREATED);
        $campaign->setCreatedBy($user);
        $campaign->setPromo(false);
        $campaign->setDeleted(false);

        $campaign2 = new Campaign();
        $campaign2->setCampaignName('Testowa kampania 2');
        $campaign2->setStartDate(new \DateTimeImmutable('2024-02-10 15:00:00'));
        $campaign2->setEndDate(new \DateTimeImmutable('2027-01-15 15:00:00'));
        $campaign2->setCampaignType(CampaignType::WHATSAPP);
        $campaign2->setDescription('jakiś opis test');
        $campaign2->setStatus(CampaignStatus::ACTIVE);
        $campaign2->setCreatedBy($user);
        $campaign2->setPromo(true);
        $campaign2->setDeleted(false);

        $campaign3 = new Campaign();
        $campaign3->setCampaignName('Testowa kampania 3');
        $campaign3->setStartDate((new \DateTimeImmutable('now'))->modify('+16 days'));
        $campaign3->setEndDate((new \DateTimeImmutable('now'))->modify('+4 months'));
        $campaign3->setCampaignType(CampaignType::WHATSAPP);
        $campaign3->setDescription('opisujemy testowo kampanie');
        $campaign3->setStatus(CampaignStatus::INACTIVE);
        $campaign3->setCreatedBy($user);
        $campaign3->setPromo(true);
        $campaign3->setDeleted(false);

        $campaign4 = new Campaign();
        $campaign4->setCampaignName('Testowa kampania 4');
        $campaign4->setStartDate((new \DateTimeImmutable('now'))->modify('+10 days'));
        $campaign4->setEndDate((new \DateTimeImmutable('now'))->modify('+6 months'));
        $campaign4->setCampaignType(CampaignType::WHATSAPP);
        $campaign4->setDescription(null);
        $campaign4->setStatus(CampaignStatus::CREATED);
        $campaign4->setCreatedBy($user);
        $campaign4->setPromo(false);
        $campaign4->setDeleted(false);

        $manager->persist($campaign);
        $manager->persist($campaign2);
        $manager->persist($campaign3);
        $manager->persist($campaign4);

        $manager->flush();
        $this->addReference(self::campaign_1, $campaign);
        $this->addReference(self::campaign_2, $campaign2);
        $this->addReference(self::campaign_3, $campaign3);
        $this->addReference(self::campaign_4, $campaign4);
    }

    public function getOrder(): int
    {
        return 3;
    }
}
