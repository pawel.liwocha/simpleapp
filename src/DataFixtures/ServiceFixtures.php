<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\CategoryService;
use App\Entity\Service;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ServiceFixtures extends Fixture implements OrderedFixtureInterface
{
    final public const service_1 = 'service_1';

    final public const service_2 = 'service_2';

    final public const service_3 = 'service_3';

    final public const service_4 = 'service_4';

    final public const service_5 = 'service_5';

    final public const service_6 = 'service_6';

    final public const service_7 = 'service_7';

    final public const service_8 = 'service_8';

    final public const service_9 = 'service_9';

    final public const service_10 = 'service_10';

    final public const service_11 = 'service_11';

    public function load(ObjectManager $manager): void
    {
        /** @var User $user */
        $user = $this->getReference('userTest');
        /** @var CategoryService $CategoryService1 */
        $CategoryService1 = $this->getReference('category_service_1');
        /** @var CategoryService $CategoryService2 */
        $CategoryService2 = $this->getReference('category_service_2');
        /** @var CategoryService $CategoryService3 */
        $CategoryService3 = $this->getReference('category_service_3');
        /** @var CategoryService $CategoryService4 */
        $CategoryService4 = $this->getReference('category_service_4');
        /** @var CategoryService $CategoryService5 */
        $CategoryService5 = $this->getReference('category_service_5');
        /** @var CategoryService $CategoryService6 */
        $CategoryService6 = $this->getReference('category_service_6');

        $service = new Service();
        $service->setCategoryService($CategoryService1);
        $service->setName('Deep plane facelif');
        $service->setCreatedBy($user);
        $service->setActive(true);
        $service->setDeleted(false);

        $service2 = new Service();
        $service2->setCategoryService($CategoryService1);
        $service2->setName('SMAS facelift');
        $service2->setCreatedBy($user);
        $service2->setActive(true);
        $service2->setDeleted(false);

        $service3 = new Service();
        $service3->setCategoryService($CategoryService1);
        $service3->setName('Liquid facelift');
        $service3->setCreatedBy($user);
        $service3->setActive(true);
        $service3->setDeleted(false);

        $service4 = new Service();
        $service4->setCategoryService($CategoryService2);
        $service4->setName('Coronal brow lift');
        $service4->setCreatedBy($user);
        $service4->setActive(true);
        $service4->setDeleted(false);

        $service5 = new Service();
        $service5->setCategoryService($CategoryService2);
        $service5->setName('Temporal brow lift');
        $service5->setCreatedBy($user);
        $service5->setActive(true);
        $service5->setDeleted(false);

        $service6 = new Service();
        $service6->setCategoryService($CategoryService3);
        $service6->setName('Blepharoplasty');
        $service6->setCreatedBy($user);
        $service6->setActive(true);
        $service6->setDeleted(false);

        $service7 = new Service();
        $service7->setCategoryService($CategoryService4);
        $service7->setName('Silicone breast implants');
        $service7->setCreatedBy($user);
        $service7->setActive(true);
        $service7->setDeleted(false);

        $service8 = new Service();
        $service8->setCategoryService($CategoryService4);
        $service8->setName('Smooth breast implants');
        $service8->setCreatedBy($user);
        $service8->setActive(false);
        $service8->setDeleted(false);

        $service9 = new Service();
        $service9->setCategoryService($CategoryService5);
        $service9->setName('Flap reconstruction');
        $service9->setCreatedBy($user);
        $service9->setActive(false);
        $service9->setDeleted(false);

        $service10 = new Service();
        $service10->setCategoryService($CategoryService6);
        $service10->setName('Porcelain veneers');
        $service10->setCreatedBy($user);
        $service10->setActive(true);
        $service10->setDeleted(false);

        $service11 = new Service();
        $service11->setCategoryService($CategoryService6);
        $service11->setName('Composite veneers');
        $service11->setCreatedBy($user);
        $service11->setActive(true);
        $service11->setDeleted(false);

        $manager->persist($service);
        $manager->persist($service2);
        $manager->persist($service3);
        $manager->persist($service4);
        $manager->persist($service5);
        $manager->persist($service6);
        $manager->persist($service7);
        $manager->persist($service8);
        $manager->persist($service9);
        $manager->persist($service10);
        $manager->persist($service11);

        $manager->flush();
        $this->addReference(self::service_1, $service);
        $this->addReference(self::service_2, $service2);
        $this->addReference(self::service_3, $service3);
        $this->addReference(self::service_4, $service4);
        $this->addReference(self::service_5, $service5);
        $this->addReference(self::service_6, $service6);
        $this->addReference(self::service_7, $service7);
        $this->addReference(self::service_8, $service8);
        $this->addReference(self::service_9, $service9);
        $this->addReference(self::service_10, $service10);
        $this->addReference(self::service_11, $service11);
    }

    public function getOrder(): int
    {
        return 9;
    }
}
