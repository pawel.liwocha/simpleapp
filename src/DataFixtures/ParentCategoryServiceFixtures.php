<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\ParentCategoryService;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ParentCategoryServiceFixtures extends Fixture implements OrderedFixtureInterface
{
    final public const parent_category_service_1 = 'parent_category_service_1';

    final public const parent_category_service_2 = 'parent_category_service_2';

    final public const parent_category_service_3 = 'parent_category_service_3';

    public function load(ObjectManager $manager): void
    {
        /** @var User $user */
        $user = $this->getReference('userTest');

        $parentCategoryService = new ParentCategoryService();
        $parentCategoryService->setName('Head, face and eyes');
        $parentCategoryService->setCreatedBy($user);
        $parentCategoryService->setActive(true);
        $parentCategoryService->setDeleted(false);

        $parentCategoryService2 = new ParentCategoryService();
        $parentCategoryService2->setName('Breasts');
        $parentCategoryService2->setCreatedBy($user);
        $parentCategoryService2->setActive(true);
        $parentCategoryService2->setDeleted(false);

        $parentCategoryService3 = new ParentCategoryService();
        $parentCategoryService3->setName('Dental');
        $parentCategoryService3->setCreatedBy($user);
        $parentCategoryService3->setActive(true);
        $parentCategoryService3->setDeleted(false);

        $manager->persist($parentCategoryService);
        $manager->persist($parentCategoryService2);
        $manager->persist($parentCategoryService3);

        $manager->flush();
        $this->addReference(self::parent_category_service_1, $parentCategoryService);
        $this->addReference(self::parent_category_service_2, $parentCategoryService2);
        $this->addReference(self::parent_category_service_3, $parentCategoryService3);
    }

    public function getOrder(): int
    {
        return 7;
    }
}
