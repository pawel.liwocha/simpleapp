<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Campaign;
use App\Entity\CampaignSentToLead;
use App\Entity\Lead;
use App\Entity\User;
use App\Enum\CampaignSentToLeadStatus;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CampaignSentToLeadFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        /** @var User $user */
        $user = $this->getReference('userTest');
        /** @var Lead $lead_1 */
        $lead_1 = $this->getReference('lead_1');
        /** @var Lead $lead_2 */
        $lead_2 = $this->getReference('lead_2');
        /** @var Lead $lead_3 */
        $lead_3 = $this->getReference('lead_3');
        /** @var Lead $lead_4 */
        $lead_4 = $this->getReference('lead_4');
        /** @var Campaign $campaign_1 */
        $campaign_1 = $this->getReference('campaign_1');
        /** @var Campaign $campaign_2 */
        $campaign_2 = $this->getReference('campaign_2');
        /** @var Campaign $campaign_3 */
        $campaign_3 = $this->getReference('campaign_3');
        /** @var Campaign $campaign_4 */
        $campaign_4 = $this->getReference('campaign_4');

        $campaignSentToUser = new CampaignSentToLead();
        $campaignSentToUser->setCampaign($campaign_1);
        $campaignSentToUser->setLead($lead_1);
        $campaignSentToUser->setStatus(CampaignSentToLeadStatus::SEND);
        $campaignSentToUser->setSentDate(new \DateTimeImmutable('2024-01-20 00:01:15'));
        $campaignSentToUser->setSendDate(new \DateTimeImmutable('2024-01-20'));
        $campaignSentToUser->setDescription('Text Jakiś opis');
        $campaignSentToUser->setMessageWhatsAppId(null);
        $campaignSentToUser->setMessageSmsId(null);
        $campaignSentToUser->setMessageEmailId(null);
        $campaignSentToUser->setCreatedBy($user);
        $campaignSentToUser->setDeleted(false);

        $campaignSentToUser2 = new CampaignSentToLead();
        $campaignSentToUser2->setCampaign($campaign_2);
        $campaignSentToUser2->setLead($lead_1);
        $campaignSentToUser2->setStatus(CampaignSentToLeadStatus::PLANNED);
        $campaignSentToUser2->setSentDate(null);
        $campaignSentToUser2->setSendDate(new \DateTimeImmutable('2024-03-04'));
        $campaignSentToUser2->setDescription('Text Jakiś opis');
        $campaignSentToUser2->setMessageWhatsAppId(null);
        $campaignSentToUser2->setMessageSmsId(null);
        $campaignSentToUser2->setMessageEmailId(null);
        $campaignSentToUser2->setCreatedBy($user);
        $campaignSentToUser2->setDeleted(false);

        $campaignSentToUser3 = new CampaignSentToLead();
        $campaignSentToUser3->setCampaign($campaign_2);
        $campaignSentToUser3->setLead($lead_2);
        $campaignSentToUser3->setStatus(CampaignSentToLeadStatus::PLANNED);
        $campaignSentToUser3->setSentDate(null);
        $campaignSentToUser3->setSendDate(new \DateTimeImmutable('2024-03-20'));
        $campaignSentToUser3->setDescription('Text Jakiś opis');
        $campaignSentToUser3->setMessageWhatsAppId(null);
        $campaignSentToUser3->setMessageSmsId(null);
        $campaignSentToUser3->setMessageEmailId(null);
        $campaignSentToUser3->setCreatedBy($user);
        $campaignSentToUser3->setDeleted(false);

        $campaignSentToUser4 = new CampaignSentToLead();
        $campaignSentToUser4->setCampaign($campaign_3);
        $campaignSentToUser4->setLead($lead_2);
        $campaignSentToUser4->setStatus(CampaignSentToLeadStatus::PLANNED);
        $campaignSentToUser4->setSentDate(null);
        $campaignSentToUser4->setSendDate(new \DateTimeImmutable('2024-06-10'));
        $campaignSentToUser4->setDescription(null);
        $campaignSentToUser4->setMessageWhatsAppId(null);
        $campaignSentToUser4->setMessageSmsId(null);
        $campaignSentToUser4->setMessageEmailId(null);
        $campaignSentToUser4->setCreatedBy($user);
        $campaignSentToUser4->setDeleted(false);

        $campaignSentToUser5 = new CampaignSentToLead();
        $campaignSentToUser5->setCampaign($campaign_4);
        $campaignSentToUser5->setLead($lead_3);
        $campaignSentToUser5->setStatus(CampaignSentToLeadStatus::PLANNED);
        $campaignSentToUser5->setSentDate(null);
        $campaignSentToUser5->setSendDate(new \DateTimeImmutable('2024-09-27'));
        $campaignSentToUser5->setDescription(null);
        $campaignSentToUser5->setMessageWhatsAppId(null);
        $campaignSentToUser5->setMessageSmsId(null);
        $campaignSentToUser5->setMessageEmailId(null);
        $campaignSentToUser5->setCreatedBy($user);
        $campaignSentToUser5->setDeleted(false);

        $campaignSentToUser6 = new CampaignSentToLead();
        $campaignSentToUser6->setCampaign($campaign_1);
        $campaignSentToUser6->setLead($lead_3);
        $campaignSentToUser6->setStatus(CampaignSentToLeadStatus::SEND);
        $campaignSentToUser6->setSentDate(new \DateTimeImmutable('2024-01-27 10:00:35'));
        $campaignSentToUser6->setSendDate(new \DateTimeImmutable('2024-01-27 10:00:00'));
        $campaignSentToUser6->setDescription(null);
        $campaignSentToUser6->setMessageWhatsAppId(null);
        $campaignSentToUser6->setMessageSmsId(null);
        $campaignSentToUser6->setMessageEmailId(null);
        $campaignSentToUser6->setCreatedBy($user);
        $campaignSentToUser6->setDeleted(false);

        $campaignSentToUser7 = new CampaignSentToLead();
        $campaignSentToUser7->setCampaign($campaign_1);
        $campaignSentToUser7->setLead($lead_4);
        $campaignSentToUser7->setStatus(CampaignSentToLeadStatus::SEND);
        $campaignSentToUser7->setSentDate(new \DateTimeImmutable('2024-01-27 10:00:35'));
        $campaignSentToUser7->setSendDate(new \DateTimeImmutable('2024-01-27 10:00:00'));
        $campaignSentToUser7->setDescription(null);
        $campaignSentToUser7->setMessageWhatsAppId(null);
        $campaignSentToUser7->setMessageSmsId(null);
        $campaignSentToUser7->setMessageEmailId(null);
        $campaignSentToUser7->setCreatedBy($user);
        $campaignSentToUser7->setDeleted(false);

        $campaignSentToUser8 = new CampaignSentToLead();
        $campaignSentToUser8->setCampaign($campaign_3);
        $campaignSentToUser8->setLead($lead_4);
        $campaignSentToUser8->setStatus(CampaignSentToLeadStatus::PLANNED);
        $campaignSentToUser8->setSentDate(null);
        $campaignSentToUser8->setSendDate(new \DateTimeImmutable('2024-05-27'));
        $campaignSentToUser8->setDescription(null);
        $campaignSentToUser8->setMessageWhatsAppId(null);
        $campaignSentToUser8->setMessageSmsId(null);
        $campaignSentToUser8->setMessageEmailId(null);
        $campaignSentToUser8->setCreatedBy($user);
        $campaignSentToUser8->setDeleted(false);

        $manager->persist($campaignSentToUser);
        $manager->persist($campaignSentToUser2);
        $manager->persist($campaignSentToUser3);
        $manager->persist($campaignSentToUser4);
        $manager->persist($campaignSentToUser5);
        $manager->persist($campaignSentToUser6);
        $manager->persist($campaignSentToUser7);
        $manager->persist($campaignSentToUser8);

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 6;
    }
}
