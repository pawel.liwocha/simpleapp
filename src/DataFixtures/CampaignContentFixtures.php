<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Campaign;
use App\Entity\CampaignContent;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CampaignContentFixtures extends Fixture implements OrderedFixtureInterface
{
    final public const campaign_content_1 = 'campaign_content_1';

    final public const campaign_content_2 = 'campaign_content_2';

    final public const campaign_content_3 = 'campaign_content_3';

    final public const campaign_content_4 = 'campaign_content_4';

    public function load(ObjectManager $manager): void
    {
        /** @var User $user */
        $user = $this->getReference('userTest');
        /** @var Campaign $campaign_1 */
        $campaign_1 = $this->getReference('campaign_1');
        /** @var Campaign $campaign_2 */
        $campaign_2 = $this->getReference('campaign_2');
        /** @var Campaign $campaign_3 */
        $campaign_3 = $this->getReference('campaign_3');
        /** @var Campaign $campaign_4 */
        $campaign_4 = $this->getReference('campaign_4');

        $campaignContent = new CampaignContent();
        $campaignContent->setCampaign($campaign_1);
        $campaignContent->setFrom('From Jeden');
        $campaignContent->setText('Text Jakiś test wiadomości');
        $campaignContent->setCreatedBy($user);
        $campaignContent->setDeleted(false);

        $campaignContent2 = new CampaignContent();
        $campaignContent2->setCampaign($campaign_2);
        $campaignContent2->setFrom('From Dwa');
        $campaignContent2->setText('Text test test');
        $campaignContent2->setCreatedBy($user);
        $campaignContent2->setDeleted(false);

        $campaignContent3 = new CampaignContent();
        $campaignContent3->setCampaign($campaign_3);
        $campaignContent3->setFrom('From Trzy');
        $campaignContent3->setText('Text Bum Bum Test');
        $campaignContent3->setCreatedBy($user);
        $campaignContent3->setDeleted(false);

        $campaignContent4 = new CampaignContent();
        $campaignContent4->setCampaign($campaign_4);
        $campaignContent4->setFrom('From Cztery');
        $campaignContent4->setText('Text Sprawdż test');
        $campaignContent4->setCreatedBy($user);
        $campaignContent4->setDeleted(false);

        $manager->persist($campaignContent);
        $manager->persist($campaignContent2);
        $manager->persist($campaignContent3);
        $manager->persist($campaignContent4);

        $manager->flush();
        $this->addReference(self::campaign_content_1, $campaignContent);
        $this->addReference(self::campaign_content_2, $campaignContent2);
        $this->addReference(self::campaign_content_3, $campaignContent3);
        $this->addReference(self::campaign_content_4, $campaignContent4);
    }

    public function getOrder(): int
    {
        return 4;
    }
}
