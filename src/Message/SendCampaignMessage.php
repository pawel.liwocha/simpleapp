<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Message;

use App\Entity\CampaignSentToLead;

class SendCampaignMessage
{
    public function __construct(
        private readonly CampaignSentToLead $campaignSentToLead,
        private readonly ?\DateTimeImmutable $date = null
    ) {
    }

    public function getCampaignSentToLead(): CampaignSentToLead
    {
        return $this->campaignSentToLead;
    }

    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }
}
