<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException;

class LoginAttemptException extends AuthenticationException
{
    public function getMessageKey(): string
    {
        return $this->getMessage();
    }
}
