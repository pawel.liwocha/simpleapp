<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\LeadRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\UuidV7;

#[ORM\Entity(repositoryClass: LeadRepository::class)]
#[ORM\Table(name: 'lead')]
#[ORM\Index(columns: ['id'], name: 'index_id')]
#[ORM\Index(columns: ['id', 'deleted'], name: 'index_id_deleted')]
#[ORM\Index(columns: ['uuid'], name: 'index_uuid')]
#[ORM\Index(columns: ['uuid', 'deleted'], name: 'index_uuid_deleted')]
#[ORM\Index(columns: ['email', 'mobile_number', 'deleted'], name: 'index_email_mobile_number_deleted')]
class Lead
{
    public function __construct(
        #[ORM\Column(type: Types::DATETIME_IMMUTABLE, options: ['default' => 'CURRENT_TIMESTAMP'])]
        private \DateTimeImmutable $createdAt = new \DateTimeImmutable('now'),
        UuidV7 $uuid = new UuidV7()
    ) {
        $this->uuid = $uuid->toRfc4122();
    }

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::BIGINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\Column(type: Types::STRING, unique: true)]
    private string $uuid;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?\DateTimeImmutable $sourcedDate = null;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    private ?string $source = null;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    private ?string $firstName = null;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    private ?string $lastName = null;

    #[ORM\Column(type: Types::STRING, length: 50, nullable: true)]
    private ?string $mobileNumber = null;

    #[ORM\Column(type: Types::STRING, length: 180, nullable: true)]
    private ?string $email = null;

    #[ORM\Column(type: Types::STRING, length: 800, nullable: true)]
    private ?string $inquiredTreatment = null;

    #[ORM\Column(type: Types::STRING, length: 800, nullable: true)]
    private ?string $comment = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $lastContactDate = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $nextContactDate = null;

    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => false])]
    private bool $deleted = false;

    #[ORM\ManyToOne(fetch: 'EAGER')]
    #[ORM\JoinColumn]
    private User $createdBy;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\ManyToOne(fetch: 'EAGER')]
    #[ORM\JoinColumn]
    private ?User $updatedBy = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $deletedAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getSourcedDate(): ?\DateTimeImmutable
    {
        return $this->sourcedDate;
    }

    public function setSourcedDate(?\DateTimeImmutable $sourcedDate): void
    {
        $this->sourcedDate = $sourcedDate;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): void
    {
        $this->source = $source;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getMobileNumber(): ?string
    {
        return $this->mobileNumber;
    }

    public function setMobileNumber(?string $mobileNumber): void
    {
        $this->mobileNumber = $mobileNumber;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getInquiredTreatment(): ?string
    {
        return $this->inquiredTreatment;
    }

    public function setInquiredTreatment(?string $inquiredTreatment): void
    {
        $this->inquiredTreatment = $inquiredTreatment;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    public function getLastContactDate(): ?\DateTimeImmutable
    {
        return $this->lastContactDate;
    }

    public function setLastContactDate(?\DateTimeImmutable $lastContactDate): void
    {
        $this->lastContactDate = $lastContactDate;
    }

    public function getNextContactDate(): ?\DateTimeImmutable
    {
        return $this->nextContactDate;
    }

    public function setNextContactDate(?\DateTimeImmutable $nextContactDate): void
    {
        $this->nextContactDate = $nextContactDate;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?User $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }

    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): void
    {
        $this->deleted = $deleted;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    public function toArray(): array
    {
        return [
            'uuid' => $this->uuid,
            'sourcedDate' => $this->sourcedDate?->format(DATE_ATOM),
            'source' => $this->source,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'mobileNumber' => $this->mobileNumber,
            'email' => $this->email,
            'inquiredTreatment' => $this->inquiredTreatment,
            'comment' => $this->comment,
            'lastContactDate' => $this->lastContactDate?->format(DATE_ATOM),
            'nextContactDate' => $this->nextContactDate?->format(DATE_ATOM),
            'deleted' => $this->deleted,
            'createdBy' => $this->createdBy->getUuid(),
            'createdAt' => $this->createdAt->format(DATE_ATOM),
            'updatedBy' => $this->updatedBy?->getUuid(),
            'updatedAt' => $this->updatedAt?->format(DATE_ATOM),
        ];
    }
}
