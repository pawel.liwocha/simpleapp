<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\CategoryServiceRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\UuidV7;

#[ORM\Entity(repositoryClass: CategoryServiceRepository::class)]
#[ORM\Table(name: 'category_service')]
#[ORM\Index(columns: ['id'], name: 'index_id')]
#[ORM\Index(columns: ['uuid'], name: 'index_uuid')]
#[ORM\Index(columns: ['uuid', 'deleted'], name: 'index_uuid_deleted')]
#[ORM\Index(columns: ['parent_category_service_id', 'deleted'], name: 'index_pcs_deleted')]
#[ORM\Index(columns: ['name'], name: 'index_uuid')]
#[ORM\Index(columns: ['name', 'deleted'], name: 'index_name_deleted')]
#[ORM\Index(columns: ['name', 'actived', 'deleted'], name: 'index_name_active_deleted')]
#[ORM\Index(columns: ['parent_category_service_id', 'actived', 'deleted'], name: 'index_pcs_active_deleted')]
#[ORM\Index(columns: ['created_by_id', 'deleted'], name: 'index_created_by_deleted')]
class CategoryService
{
    public function __construct(
        #[ORM\Column(type: Types::DATETIME_IMMUTABLE, options: ['default' => 'CURRENT_TIMESTAMP'])]
        private \DateTimeImmutable $createdAt = new \DateTimeImmutable('now'),
        UuidV7 $uuid = new UuidV7()
    ) {
        $this->uuid = $uuid->toRfc4122();
    }

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::STRING, unique: true)]
    private string $uuid;

    #[ORM\ManyToOne(targetEntity: ParentCategoryService::class, fetch: 'EAGER')]
    #[ORM\JoinColumn]
    private ParentCategoryService $parentCategoryService;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private string $name;

    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => true])]
    private bool $actived;

    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => false])]
    private bool $deleted = false;

    #[ORM\ManyToOne(fetch: 'EAGER')]
    #[ORM\JoinColumn]
    private User $createdBy;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\ManyToOne(fetch: 'EAGER')]
    #[ORM\JoinColumn]
    private ?User $updatedBy = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $deletedAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getParentCategoryService(): ParentCategoryService
    {
        return $this->parentCategoryService;
    }

    public function setParentCategoryService(ParentCategoryService $parentCategoryService): void
    {
        $this->parentCategoryService = $parentCategoryService;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getActive(): bool
    {
        return $this->actived;
    }

    public function setActive(bool $active): void
    {
        $this->actived = $active;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?User $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }

    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): void
    {
        $this->deleted = $deleted;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    public function toArray(): array
    {
        return [
            'uuid' => $this->uuid,
            'parentCategoryService' => $this->parentCategoryService->getUuid(),
            'name' => $this->name,
            'active' => $this->actived,
            'deleted' => $this->deleted,
            'createdBy' => $this->createdBy->getUuid(),
            'createdAt' => $this->createdAt->format(DATE_ATOM),
            'updatedBy' => $this->updatedBy?->getUuid(),
            'updatedAt' => $this->updatedAt?->format(DATE_ATOM),
        ];
    }
}
