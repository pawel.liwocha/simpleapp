<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\CampaignStatus;
use App\Enum\CampaignType;
use App\Repository\CampaignRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\UuidV7;

#[ORM\Entity(repositoryClass: CampaignRepository::class)]
#[ORM\Table(name: 'campaign')]
#[ORM\Index(columns: ['id'], name: 'index_id')]
#[ORM\Index(columns: ['id', 'deleted'], name: 'index_id_deleted')]
#[ORM\Index(columns: ['uuid'], name: 'index_uuid')]
#[ORM\Index(columns: ['uuid', 'deleted'], name: 'index_uuid_deleted')]
#[ORM\Index(columns: ['uuid', 'status', 'deleted'], name: 'index_uuid_status_deleted')]
#[ORM\Index(columns: ['start_date', 'end_date', 'deleted'], name: 'index_start_end_deleted')]
#[ORM\Index(columns: ['start_date', 'end_date', 'status', 'deleted'], name: 'index_start_end_status_deleted')]
#[ORM\Index(columns: ['start_date', 'end_date', 'type', 'deleted'], name: 'index_start_end_type_deleted')]
#[ORM\Index(columns: ['type', 'deleted'], name: 'index_type_deleted')]
#[ORM\Index(columns: ['status', 'deleted'], name: 'index_status_deleted')]
class Campaign
{
    public function __construct(
        #[ORM\Column(type: Types::DATETIME_IMMUTABLE, options: ['default' => 'CURRENT_TIMESTAMP'])]
        private \DateTimeImmutable $createdAt = new \DateTimeImmutable('now'),
        UuidV7 $uuid = new UuidV7()
    ) {
        $this->uuid = $uuid->toRfc4122();
    }

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::BIGINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\Column(type: Types::STRING, unique: true)]
    private string $uuid;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private string $campaignName;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private \DateTimeImmutable $startDate;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private \DateTimeImmutable $endDate;

    #[ORM\Column(type: Types::STRING, length: 500, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: Types::STRING, length: 50, enumType: CampaignType::class, options: ['default' => CampaignType::WHATSAPP])]
    private CampaignType $type;

    #[ORM\Column(type: Types::STRING, length: 50, enumType: CampaignStatus::class, options: ['default' => CampaignStatus::CREATED])]
    private CampaignStatus $status;

    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => false])]
    private bool $promo = false;

    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => false])]
    private bool $deleted = false;

    #[ORM\ManyToOne(fetch: 'EAGER')]
    #[ORM\JoinColumn]
    private User $createdBy;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\ManyToOne(fetch: 'EAGER')]
    #[ORM\JoinColumn]
    private ?User $updatedBy = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $deletedAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getCampaignName(): string
    {
        return $this->campaignName;
    }

    public function setCampaignName(string $campaignName): void
    {
        $this->campaignName = $campaignName;
    }

    public function getStartDate(): \DateTimeImmutable
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeImmutable $startDate): void
    {
        $this->startDate = $startDate;
    }

    public function getEndDate(): \DateTimeImmutable
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeImmutable $endDate): void
    {
        $this->endDate = $endDate;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getCampaignType(): CampaignType
    {
        return $this->type;
    }

    public function setCampaignType(CampaignType $type): void
    {
        $this->type = $type;
    }

    public function getStatus(): CampaignStatus
    {
        return $this->status;
    }

    public function setStatus(CampaignStatus $status): void
    {
        $this->status = $status;
    }

    public function isPromo(): ?bool
    {
        return $this->promo;
    }

    public function setPromo(bool $promo): void
    {
        $this->promo = $promo;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?User $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }

    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): void
    {
        $this->deleted = $deleted;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    public function toArray(): array
    {
        return [
            'uuid' => $this->uuid,
            'campaignName' => $this->campaignName,
            'startDate' => $this->startDate->format(DATE_ATOM),
            'endDate' => $this->endDate->format(DATE_ATOM),
            'description' => $this->description,
            'type' => $this->type,
            'status' => $this->status,
            'promo' => $this->promo,
            'deleted' => $this->deleted,
            'createdBy' => $this->createdBy->getUuid(),
            'createdAt' => $this->createdAt->format(DATE_ATOM),
            'updatedBy' => $this->updatedBy?->getUuid(),
            'updatedAt' => $this->updatedAt?->format(DATE_ATOM),
        ];
    }
}
