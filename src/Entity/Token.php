<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\TokenRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TokenRepository::class)]
#[ORM\Table(name: 'token')]
#[ORM\Index(columns: ['id'], name: 'index_id')]
#[ORM\Index(columns: ['user_id'], name: 'index_user')]
#[ORM\Index(columns: ['token'], name: 'index_token')]
class Token
{
    public function __construct(
        #[ORM\Column(type: Types::DATETIME_IMMUTABLE, options: ['default' => 'CURRENT_TIMESTAMP'])]
        private \DateTimeImmutable $createdAt = new \DateTimeImmutable('now')
    ) {
    }

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::BIGINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EAGER')]
    #[ORM\JoinColumn]
    private User $user;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    private string $token;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, options: ['default' => 'CURRENT_TIMESTAMP'])]
    private \DateTimeImmutable $expired;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getExpired(): \DateTimeImmutable
    {
        return $this->expired;
    }

    public function setExpired(\DateTimeImmutable $expired): void
    {
        $this->expired = $expired;
    }
}
