<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\CampaignSentToLeadStatus;
use App\Repository\CampaignSentToLeadRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\UuidV7;

#[ORM\Entity(repositoryClass: CampaignSentToLeadRepository::class)]
#[ORM\Table(name: 'campaign_sent_to_lead')]
#[ORM\Index(columns: ['id'], name: 'index_id')]
#[ORM\Index(columns: ['campaign_id'], name: 'index_campaign')]
#[ORM\Index(columns: ['lead_id'], name: 'index_lead')]
#[ORM\Index(columns: ['send_date', 'status', 'deleted'], name: 'index_senddate_status_deleted')]
#[ORM\Index(columns: ['campaign_id', 'deleted'], name: 'index_campaign_deleted')]
#[ORM\Index(columns: ['lead_id', 'deleted'], name: 'index_lead_deleted')]
#[ORM\Index(columns: ['status', 'deleted'], name: 'index_status_deleted')]
class CampaignSentToLead
{
    public function __construct(
        #[ORM\Column(type: Types::DATETIME_IMMUTABLE, options: ['default' => 'CURRENT_TIMESTAMP'])]
        private \DateTimeImmutable $createdAt = new \DateTimeImmutable('now'),
        UuidV7 $uuid = new UuidV7()
    ) {
        $this->uuid = $uuid->toRfc4122();
    }

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::BIGINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\Column(type: Types::STRING, unique: true)]
    private string $uuid;

    #[ORM\ManyToOne(targetEntity: Campaign::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false)]
    private Campaign $campaign;

    #[ORM\ManyToOne(targetEntity: Lead::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false)]
    private Lead $lead;

    #[ORM\Column(type: Types::STRING, length: 1000, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: Types::STRING, length: 50, enumType: CampaignSentToLeadStatus::class, options: ['default' => CampaignSentToLeadStatus::PLANNED])]
    private CampaignSentToLeadStatus $status;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $sentDate = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private \DateTimeImmutable $sendDate;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    private ?string $messageWhatsAppId = null;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    private ?string $messageSmsId = null;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    private ?string $messageEmailId = null;

    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => false])]
    private bool $deleted = false;

    #[ORM\ManyToOne(fetch: 'EAGER')]
    #[ORM\JoinColumn]
    private User $createdBy;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\ManyToOne(fetch: 'EAGER')]
    #[ORM\JoinColumn]
    private ?User $updatedBy = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $deletedAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCampaign(): Campaign
    {
        return $this->campaign;
    }

    public function setCampaign(Campaign $campaign): void
    {
        $this->campaign = $campaign;
    }

    public function getLead(): Lead
    {
        return $this->lead;
    }

    public function setLead(Lead $lead): void
    {
        $this->lead = $lead;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getStatus(): CampaignSentToLeadStatus
    {
        return $this->status;
    }

    public function setStatus(CampaignSentToLeadStatus $status): void
    {
        $this->status = $status;
    }

    public function getSentDate(): ?\DateTimeImmutable
    {
        return $this->sentDate;
    }

    public function setSentDate(?\DateTimeImmutable $sentDate): void
    {
        $this->sentDate = $sentDate;
    }

    public function getSendDate(): \DateTimeImmutable
    {
        return $this->sendDate;
    }

    public function setSendDate(\DateTimeImmutable $sendDate): void
    {
        $this->sendDate = $sendDate;
    }

    public function getMessageWhatsAppId(): ?string
    {
        return $this->messageWhatsAppId;
    }

    public function setMessageWhatsAppId(?string $messageWhatsAppId): void
    {
        $this->messageWhatsAppId = $messageWhatsAppId;
    }

    public function getMessageSmsId(): ?string
    {
        return $this->messageSmsId;
    }

    public function setMessageSmsId(?string $messageSmsId): void
    {
        $this->messageSmsId = $messageSmsId;
    }

    public function getMessageEmailId(): ?string
    {
        return $this->messageEmailId;
    }

    public function setMessageEmailId(?string $messageEmailId): void
    {
        $this->messageEmailId = $messageEmailId;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?User $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }

    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): void
    {
        $this->deleted = $deleted;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    public function toArray(): array
    {
        return [
            'uuid' => $this->uuid,
            'campaign' => $this->campaign->getUuid(),
            'lead' => $this->lead->getUuid(),
            'description' => $this->description,
            'status' => $this->status,
            'sendDate' => $this->sendDate->format(DATE_ATOM),
            'sentDate' => $this->sentDate?->format(DATE_ATOM),
            'messageWhatsAppId' => $this->messageWhatsAppId,
            'messageSmsId' => $this->messageSmsId,
            'messageEmailId' => $this->messageEmailId,
            'deleted' => $this->deleted,
            'createdBy' => $this->createdBy->getUuid(),
            'createdAt' => $this->createdAt->format(DATE_ATOM),
            'updatedBy' => $this->updatedBy?->getUuid(),
            'updatedAt' => $this->updatedAt?->format(DATE_ATOM),
        ];
    }
}
