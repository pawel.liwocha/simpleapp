<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\LoginStatus;
use App\Repository\LoginLogRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LoginLogRepository::class)]
#[ORM\Table(name: 'login_log')]
#[ORM\Index(columns: ['id'], name: 'index_id')]
#[ORM\Index(columns: ['user_id'], name: 'index_user')]
class LoginLog
{
    public function __construct(
        #[ORM\Column(type: Types::DATETIME_IMMUTABLE, options: ['default' => 'CURRENT_TIMESTAMP'])]
        private \DateTimeImmutable $createdAt = new \DateTimeImmutable('now')
    ) {
    }

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::BIGINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EAGER')]
    #[ORM\JoinColumn]
    private User $user;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    private string $token;

    #[ORM\Column(type: Types::STRING, length: 1000, nullable: true)]
    private ?string $userAgent = null;

    #[ORM\Column(type: Types::STRING, length: 100, nullable: true)]
    private ?string $ip = null;

    #[ORM\Column(type: Types::STRING, length: 50, enumType: LoginStatus::class, options: ['default' => LoginStatus::SUCCESS])]
    private ?LoginStatus $status = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    public function setUserAgent(?string $userAgent): void
    {
        $this->userAgent = $userAgent;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): void
    {
        $this->ip = $ip;
    }

    public function getStatus(): ?LoginStatus
    {
        return $this->status;
    }

    public function setStatus(?LoginStatus $status): void
    {
        $this->status = $status;
    }
}
