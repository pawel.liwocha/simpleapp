<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Controller\Api;

use App\Entity\Lead;
use App\Entity\User;
use App\Interface\TokenAuthenticatedController;
use App\Repository\LeadRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route(path: '/lead', name: 'lead_')]
class LeadController extends AbstractController implements TokenAuthenticatedController
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly LeadRepository $leadRepository,
        private readonly LoggerInterface $logger,
        private readonly ValidatorInterface $validator
    ) {
    }

    #[Route(path: '/add', name: 'add', methods: ['POST'])]
    public function add(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $content = json_decode($request->getContent());
        if ($content) {
            try {
                $lead = new Lead();
                $lead->setSourcedDate(isset($content->sourcedDate) ? new \DateTimeImmutable((string) $content->sourcedDate) : new \DateTimeImmutable('now'));
                $lead->setSource(isset($content->source) ? htmlspecialchars(trim((string) $content->source)) : null);
                $lead->setFirstName(isset($content->firstName) ? htmlspecialchars(trim((string) $content->firstName)) : null);
                $lead->setLastName(isset($content->lastName) ? htmlspecialchars(trim((string) $content->lastName)) : null);
                $lead->setEmail(isset($content->email) ? htmlspecialchars(trim((string) $content->email)) : null);
                $lead->setMobileNumber(isset($content->mobileNumber) ? htmlspecialchars(trim((string) $content->mobileNumber)) : null);
                $lead->setInquiredTreatment(isset($content->inquiredTreatment) ? htmlspecialchars(trim((string) $content->inquiredTreatment)) : null);
                $lead->setComment(isset($content->comment) ? htmlspecialchars(trim((string) $content->comment)) : null);
                $lead->setLastContactDate(null);
                $lead->setNextContactDate(null);
                $lead->setCreatedBy($user);
                $lead->setDeleted(false);

                try {
                    $this->em->persist($lead);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Add lead error: '.$exception->getMessage().PHP_EOL.' userAdd: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'Lead successfully added', 'lead' => $lead->toArray()], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Add lead error: '.$exception->getMessage().PHP_EOL.' userAdd: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/list', name: 'list', methods: ['GET'])]
    public function list(): JsonResponse
    {
        $leads = $this->leadRepository->findBy(['deleted' => 0]);
        $return = [];

        foreach ($leads as $lead) {
            $return[] = $lead->toArray();
        }

        return $this->json(['Success' => true, 'leads' => $return], Response::HTTP_OK);
    }

    #[Route(path: '/list-to-send', name: 'listToSend', methods: ['GET'])]
    public function listToSend(Request $request): JsonResponse
    {
        $content = json_decode($request->getContent());
        if ($content) {
            $return = [];
            if ($content->campaignType === 'whatsapp' || $content->campaignType === 'sms' || $content->campaignType === 'coldcall') {
                $leads = $this->leadRepository->findLeadsWithPhoneNumber();
                foreach ($leads as $lead) {
                    $return[] = $lead->toArray();
                }
            } elseif ($content->campaignType === 'email') {
                $leads = $this->leadRepository->findLeadsWithEmail();
                foreach ($leads as $lead) {
                    $return[] = $lead->toArray();
                }
            } else {
                return $this->json(['Success' => false, 'info' => 'Campaign type not recognized'], Response::HTTP_BAD_REQUEST);
            }

            return $this->json(['Success' => true, 'leads' => $return], Response::HTTP_OK);
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/import', name: 'import', methods: ['POST'])]
    public function import(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $content = $request->request->all();

        /** @var UploadedFile $file */
        $file = $request->files->get('csv_file');

        $violations = $this->validator->validate(
            $file,
            [
                new NotBlank([
                    'message' => 'Please select a file to upload',
                ]),
                new File([
                    'maxSize' => '2M',
                    'mimeTypes' => [
                        'text/csv',
                        'text/plain',
                    ],
                ]),
            ]
        );

        if ($violations->count() > 0) {
            return $this->json(['Success' => false, 'info' => 'File is to big or is not csv'], Response::HTTP_BAD_REQUEST);
        }

        $errors = false;
        $contentDuplicates = isset($content['duplicates']) ? htmlspecialchars(trim((string) $content['duplicates'])) : 'clone';
        $contentError = isset($content['errors']) && filter_var($content['errors'], FILTER_VALIDATE_BOOLEAN);

        if (($handle = fopen($file->getPathname(), 'r')) !== false) {
            fgetcsv($handle, 1000, ',');
            while (($data = fgetcsv($handle, 1000, ';')) !== false) {
                if ($contentError) {
                    $count = \count($data);
                    if ($count !== 8) {
                        $errors = true;
                    }
                }

                $leads = [];

                if ($contentDuplicates === 'overwrite') {
                    $leads = $this->leadRepository->findDuplicateLead((string) $data[4], (string) $data[5]);
                }

                if ($leads !== []) {
                    foreach ($leads as $lead) {
                        $lead->setSourcedDate(isset($data[0]) ? new \DateTimeImmutable($data[0]) : $lead->getSourcedDate());
                        $lead->setSource($data[1] ?? $lead->getSource());
                        $lead->setFirstName($data[2] ?? $lead->getFirstName());
                        $lead->setLastName($data[3] ?? $lead->getLastName());
                        $lead->setEmail($data[4] ?? $lead->getEmail());
                        $lead->setMobileNumber($data[5] ?? $lead->getMobileNumber());
                        $lead->setInquiredTreatment($data[6] ?? $lead->getInquiredTreatment());
                        $lead->setComment($data[7] ?? $lead->getComment());
                        $lead->setUpdatedAt(new \DateTimeImmutable('now'));
                        $lead->setUpdatedBy($user);
                    }
                } else {
                    $lead = new Lead();
                    $lead->setSourcedDate(isset($data[0]) ? new \DateTimeImmutable($data[0]) : new \DateTimeImmutable('now'));
                    $lead->setSource($data[1] ?? null);
                    $lead->setFirstName($data[2] ?? null);
                    $lead->setLastName($data[3] ?? null);
                    $lead->setEmail($data[4] ?? null);
                    $lead->setMobileNumber($data[5] ?? null);
                    $lead->setInquiredTreatment($data[6] ?? null);
                    $lead->setComment($data[7] ?? null);
                    $lead->setLastContactDate(null);
                    $lead->setNextContactDate(null);
                    $lead->setCreatedBy($user);
                    $lead->setDeleted(false);
                }

                $this->em->persist($lead);
            }

            fclose($handle);

            if ($errors) {
                $this->em->clear();

                return $this->json(['Success' => false, 'info' => 'Some rows has wrong number of columns'], Response::HTTP_CONFLICT);
            }

            try {
                $this->em->flush();
            } catch (\Exception $exception) {
                $this->logger->error('Import lead error: '.$exception->getMessage().PHP_EOL.' userImport: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Import error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => true, 'info' => 'Import successfully'], Response::HTTP_OK);
    }

    #[Route(path: '/{uuid}', name: 'show', methods: ['GET'])]
    public function show(string $uuid): JsonResponse
    {
        $lead = $this->leadRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);

        if ($lead instanceof Lead) {
            return $this->json(['lead' => $lead->toArray()], Response::HTTP_OK);
        }

        return $this->json(['Success' => false, 'info' => 'Lead not exist'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/edit', name: 'edit', methods: ['PUT'])]
    public function edit(Request $request, string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $content = json_decode($request->getContent());
        if ($content) {
            try {
                $lead = $this->leadRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
                if ($lead instanceof Lead) {
                    if (isset($content->sourcedDate)) {
                        $lead->setSourcedDate($content->sourcedDate ? new \DateTimeImmutable((string) $content->sourcedDate) : $lead->getSourcedDate());
                    }

                    if (isset($content->source)) {
                        $lead->setSource($content->source ? htmlspecialchars(trim((string) $content->source)) : $lead->getSource());
                    }

                    if (isset($content->firstName)) {
                        $lead->setFirstName($content->firstName ? htmlspecialchars(trim((string) $content->firstName)) : $lead->getFirstName());
                    }

                    if (isset($content->lastName)) {
                        $lead->setLastName($content->lastName ? htmlspecialchars(trim((string) $content->lastName)) : (string) $lead->getLastName());
                    }

                    if (isset($content->email)) {
                        $lead->setEmail($content->email ? htmlspecialchars(trim((string) $content->email)) : $lead->getEmail());
                    }

                    if (isset($content->mobileNumber)) {
                        $lead->setMobileNumber($content->mobileNumber ? htmlspecialchars(trim((string) $content->mobileNumber)) : $lead->getMobileNumber());
                    }

                    if (isset($content->inquiredTreatment)) {
                        $lead->setInquiredTreatment($content->inquiredTreatment ? htmlspecialchars(trim((string) $content->inquiredTreatment)) : $lead->getInquiredTreatment());
                    }

                    if (isset($content->comment)) {
                        $lead->setComment($content->comment ? htmlspecialchars(trim((string) $content->comment)) : $lead->getComment());
                    }

                    $lead->setUpdatedBy($user);
                    $lead->setUpdatedAt(new \DateTimeImmutable('now'));

                    try {
                        $this->em->persist($user);
                        $this->em->flush();
                    } catch (\Exception $exception) {
                        $this->logger->error('Edit lead error: '.$exception->getMessage().PHP_EOL.' userEdit: '.$user->getId().PHP_EOL);

                        return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                    }

                    return $this->json(['Success' => true, 'info' => 'Lead successfully updated', 'lead' => $lead->toArray()], Response::HTTP_OK);
                }

                return $this->json(['Success' => false, 'info' => 'Lead not exist'], Response::HTTP_BAD_REQUEST);
            } catch (\Exception $exception) {
                $this->logger->error('Edit lead error: '.$exception->getMessage().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/delete', name: 'delete', methods: ['DELETE'])]
    public function delete(string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $lead = $this->leadRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
        if ($lead instanceof Lead) {
            try {
                $lead->setDeleted(true);
                $lead->setDeletedAt(new \DateTimeImmutable('now'));
                $lead->setUpdatedBy($user);

                try {
                    $this->em->persist($lead);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Delete lead error: '.$exception->getMessage().PHP_EOL.' userDelete: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'Lead deleted'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Delete lead error: '.$exception->getMessage().PHP_EOL.' userDelete: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Lead not exist'], Response::HTTP_BAD_REQUEST);
    }
}
