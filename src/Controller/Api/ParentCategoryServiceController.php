<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Controller\Api;

use App\Entity\ParentCategoryService;
use App\Entity\User;
use App\Interface\TokenAuthenticatedController;
use App\Repository\ParentCategoryServiceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/parent-category-service', name: 'parent_category_service_')]
class ParentCategoryServiceController extends AbstractController implements TokenAuthenticatedController
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ParentCategoryServiceRepository $parentCategoryServiceRepository,
        private readonly LoggerInterface $logger
    ) {
    }

    #[Route(path: '/add', name: 'add', methods: ['POST'])]
    public function add(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $content = json_decode($request->getContent());
        if ($content) {
            try {
                $parentCategoryService = new ParentCategoryService();
                $parentCategoryService->setName(htmlspecialchars(trim((string) $content->name)));
                $parentCategoryService->setActive(filter_var($content->active, FILTER_VALIDATE_BOOLEAN));
                $parentCategoryService->setCreatedBy($user);
                $parentCategoryService->setDeleted(false);

                try {
                    $this->em->persist($parentCategoryService);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Add ParentCategoryService error: '.$exception->getMessage().PHP_EOL.' userAdd: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'ParentCategoryService successfully added', 'ParentCategoryService' => $parentCategoryService->toArray()], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Add ParentCategoryService error: '.$exception->getMessage().PHP_EOL.' userAdd: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/list-all', name: 'list_all', methods: ['GET'])]
    public function list(): JsonResponse
    {
        $ParentCategoryServices = $this->parentCategoryServiceRepository->findBy(['deleted' => 0]);
        $return = [];

        foreach ($ParentCategoryServices as $parentCategoryService) {
            $return[] = $parentCategoryService->toArray();
        }

        return $this->json(['Success' => true, 'ParentCategoryService' => $return], Response::HTTP_OK);
    }

    #[Route(path: '/list-active', name: 'list_active', methods: ['GET'])]
    public function listActive(): JsonResponse
    {
        $ParentCategoryServices = $this->parentCategoryServiceRepository->findBy(['actived' => 0, 'deleted' => 0]);
        $return = [];

        foreach ($ParentCategoryServices as $parentCategoryService) {
            $return[] = $parentCategoryService->toArray();
        }

        return $this->json(['Success' => true, 'ParentCategoryService' => $return], Response::HTTP_OK);
    }

    #[Route(path: '/{uuid}', name: 'show', methods: ['GET'])]
    public function show(string $uuid): JsonResponse
    {
        $parentCategoryService = $this->parentCategoryServiceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);

        if ($parentCategoryService instanceof ParentCategoryService) {
            return $this->json(['ParentCategoryService' => $parentCategoryService->toArray()], Response::HTTP_OK);
        }

        return $this->json(['Success' => false, 'info' => 'ParentCategoryService not exist'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/edit', name: 'edit', methods: ['PUT'])]
    public function edit(Request $request, string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $content = json_decode($request->getContent());
        if ($content) {
            try {
                $parentCategoryService = $this->parentCategoryServiceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
                if ($parentCategoryService instanceof ParentCategoryService) {
                    if (isset($content->name)) {
                        $parentCategoryService->setName($content->name ? htmlspecialchars(trim((string) $content->name)) : $parentCategoryService->getName());
                    }

                    if (isset($content->active)) {
                        $parentCategoryService->setActive($content->active ? filter_var($content->active, FILTER_VALIDATE_BOOLEAN) : $parentCategoryService->getActive());
                    }

                    $parentCategoryService->setUpdatedBy($user);
                    $parentCategoryService->setUpdatedAt(new \DateTimeImmutable('now'));

                    try {
                        $this->em->persist($parentCategoryService);
                        $this->em->flush();
                    } catch (\Exception $exception) {
                        $this->logger->error('Edit ParentCategoryService error: '.$exception->getMessage().PHP_EOL.' userEdit: '.$user->getId().PHP_EOL);

                        return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                    }

                    return $this->json(['Success' => true, 'info' => 'ParentCategoryService successfully updated', 'ParentCategoryService' => $parentCategoryService->toArray()], Response::HTTP_OK);
                }

                return $this->json(['Success' => false, 'info' => 'ParentCategoryService not exist'], Response::HTTP_BAD_REQUEST);
            } catch (\Exception $exception) {
                $this->logger->error('Edit ParentCategoryService error: '.$exception->getMessage().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/active', name: 'active', methods: ['PUT'])]
    public function active(string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $parentCategoryService = $this->parentCategoryServiceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
        if ($parentCategoryService instanceof ParentCategoryService) {
            try {
                $parentCategoryService->setActive(true);
                $parentCategoryService->setDeletedAt(new \DateTimeImmutable('now'));
                $parentCategoryService->setUpdatedBy($user);

                try {
                    $this->em->persist($parentCategoryService);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Active ParentCategoryService error: '.$exception->getMessage().PHP_EOL.' userActive: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'ParentCategoryService Actived'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Active ParentCategoryService error: '.$exception->getMessage().PHP_EOL.' userActive: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'ParentCategoryService not exist'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/deactivate', name: 'deactivate', methods: ['PUT'])]
    public function deactivate(string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $parentCategoryService = $this->parentCategoryServiceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
        if ($parentCategoryService instanceof ParentCategoryService) {
            try {
                $parentCategoryService->setActive(false);
                $parentCategoryService->setDeletedAt(new \DateTimeImmutable('now'));
                $parentCategoryService->setUpdatedBy($user);

                try {
                    $this->em->persist($parentCategoryService);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Deactivate ParentCategoryService error: '.$exception->getMessage().PHP_EOL.' userDeactivatee: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'ParentCategoryService Deactivated'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Deactivate ParentCategoryService error: '.$exception->getMessage().PHP_EOL.' userDeactivate: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'ParentCategoryService not exist'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/delete', name: 'delete', methods: ['DELETE'])]
    public function delete(string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $parentCategoryService = $this->parentCategoryServiceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
        if ($parentCategoryService instanceof ParentCategoryService) {
            try {
                $parentCategoryService->setDeleted(true);
                $parentCategoryService->setDeletedAt(new \DateTimeImmutable('now'));
                $parentCategoryService->setUpdatedBy($user);

                try {
                    $this->em->persist($parentCategoryService);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Delete ParentCategoryService error: '.$exception->getMessage().PHP_EOL.' userDelete: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'ParentCategoryService deleted'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Delete ParentCategoryService error: '.$exception->getMessage().PHP_EOL.' userDelete: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'ParentCategoryService not exist'], Response::HTTP_BAD_REQUEST);
    }
}
