<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Controller\Api;

use App\Entity\User;
use App\Enum\UserStatus;
use App\Repository\UserRepository;
use App\Service\EmailService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

#[Route(path: '/reset-password', name: 'reset_password_')]
class ResetPasswordController extends AbstractController
{
    use ResetPasswordControllerTrait;

    public function __construct(
        private readonly ResetPasswordHelperInterface $resetPasswordHelper,
        private readonly EntityManagerInterface $em,
        private readonly UserRepository $userRepository,
        private readonly EmailService $emailService,
        private readonly TranslatorInterface $translator,
        private readonly UserPasswordHasherInterface $passwordHasher
    ) {
    }

    /**
     * Display & process form to request a password reset.
     */
    #[Route('/', name: 'request', methods: ['POST'])]
    public function request(Request $request): JsonResponse
    {
        $content = json_decode($request->getContent());
        $username = $content->username;

        $user = $this->userRepository->findOneBy(['userName' => $username, 'status' => UserStatus::ACTIVE, 'deleted' => false]);

        if ($user instanceof User) {
            $resetToken = $this->resetPasswordHelper->generateResetToken($user);
            $data = [
                'resetToken' => $resetToken,
            ];

            $this->emailService->sendPasswordReset($user, $data);

            $this->setTokenObjectInSession($resetToken);
        }

        return $this->json(['Success' => true, 'info' => 'If username exist in database, we will send email to his address'], Response::HTTP_OK);
    }

    /**
     * Validates and process the reset URL that the user clicked in their email.
     */
    #[Route('/reset/{token}', name: 'reset', methods: ['POST'])]
    public function reset(Request $request, string $token): JsonResponse
    {
        try {
            $user = $this->resetPasswordHelper->validateTokenAndFetchUser($token);
        } catch (ResetPasswordExceptionInterface $resetPasswordException) {
            $error = sprintf(
                '%s - %s',
                $this->translator->trans(ResetPasswordExceptionInterface::MESSAGE_PROBLEM_VALIDATE, [], 'ResetPasswordBundle'),
                $this->translator->trans($resetPasswordException->getReason(), [], 'ResetPasswordBundle')
            );

            return $this->json(['Success' => false, 'info' => $error], Response::HTTP_OK);
        }

        $content = json_decode($request->getContent());
        $password = $content->password;
        if (\is_string($password) && $user instanceof PasswordAuthenticatedUserInterface) {
            // A password reset token should be used only once, remove it.
            $this->resetPasswordHelper->removeResetRequest($token);

            $encodedPassword = $this->passwordHasher->hashPassword(
                $user,
                $password
            );

            $user->setPassword($encodedPassword);

            $this->em->persist($user);
            $this->em->flush();

            // The session is cleaned up after the password has been changed.
            $this->cleanSessionAfterReset();

            return $this->json(['Success' => true, 'info' => 'Password successfully changed'], Response::HTTP_OK);
        }

        return $this->json(['Success' => false, 'info' => 'Password not send or user not exist'], Response::HTTP_OK);
    }
}
