<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Controller\Api;

use App\Entity\LoginLog;
use App\Entity\User;
use App\Enum\LoginStatus;
use App\Service\SecurityService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class SecurityController extends AbstractController
{
    public function __construct(
        private readonly SecurityService $securityService,
        private readonly EntityManagerInterface $em
    ) {
    }

    #[Route('/login', name: 'login')]
    public function index(Request $request, #[CurrentUser] ?User $user): JsonResponse
    {
        if (!$user instanceof User) {
            return $this->json([
                'message' => 'Missing credentials',
            ], Response::HTTP_UNAUTHORIZED);
        }

        $token = $this->securityService->generateToken($user);

        $loginLog = new LoginLog();
        $loginLog->setUser($user);
        $loginLog->setToken($token->getToken());
        $loginLog->setUserAgent($request->headers->get('User-Agent'));
        $loginLog->setIp($_SERVER['HTTP_CF_CONNECTING_IP'] ?? ($_SERVER['REMOTE_ADDR'] ?? 'undefined'));
        $loginLog->setStatus(LoginStatus::SUCCESS);

        $this->em->persist($loginLog);
        $this->em->flush();

        return $this->json([
            'user' => $user->toArray(),
            'token' => $token->getToken(),
        ]);
    }

    #[Route('/logout', name: 'logout')]
    public function logout(): never
    {
        throw new \Exception("Don't forget to activate logout in security.yaml");
    }
}
