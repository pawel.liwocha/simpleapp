<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Controller\Api;

use App\Entity\User;
use App\Enum\UserStatus;
use App\Interface\TokenAuthenticatedController;
use App\Repository\UserRepository;
use App\Service\EmailService;
use App\Service\SecurityService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[IsGranted('ROLE_SUPER_ADMIN')]
#[Route(path: '/user', name: 'user_')]
class UserController extends AbstractController implements TokenAuthenticatedController
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly UserRepository $userRepository,
        private readonly LoggerInterface $logger,
        private readonly ValidatorInterface $validator,
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly SecurityService $securityService,
        private readonly EmailService $emailService,
        private RoleHierarchyInterface $roleHierarchy
    ) {
    }

    #[Route(path: '/add', name: 'add', methods: ['POST'])]
    public function add(Request $request): JsonResponse
    {
        $content = json_decode($request->getContent());
        if ($content) {
            if (!isset($content->firstName)
                || !isset($content->lastName)
                || !isset($content->email)
                || !isset($content->role)
            ) {
                return $this->json(['Success' => false, 'info' => 'Content not have firstName, lastName, role or email'], Response::HTTP_BAD_REQUEST);
            }

            try {
                $password = $this->securityService->generatePassword();

                $user = new User();
                $user->setUserName(strtolower($content->lastName.substr((string) $content->firstName, 0, 1)));
                $user->setFirstName(htmlspecialchars(trim((string) $content->firstName)));
                $user->setLastName(htmlspecialchars(trim((string) $content->lastName)));
                $user->setEmail(htmlspecialchars(trim((string) $content->email)));
                $user->setStatus(UserStatus::ACTIVE);
                $user->setRoles([$content->role]);
                $user->setDeleted(false);
                $user->setDeactivateAt(null);
                $user->setPassword($this->passwordHasher->hashPassword($user, $password));

                try {
                    $this->em->persist($user);
                    $this->em->flush();

                    $this->emailService->sendNewUser($user, ['password' => $password, 'user' => $user->toArray()]);
                } catch (\Exception $exception) {
                    $this->logger->error('Add user error: '.$exception->getMessage().PHP_EOL.' userAdd: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'User successfully added', 'user' => $user->toArray()], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Add user error: '.$exception->getMessage().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/list', name: 'list', methods: ['GET'])]
    public function list(): JsonResponse
    {
        $users = $this->userRepository->findBy(['deleted' => 0]);
        $return = [];

        foreach ($users as $user) {
            $return[] = $user->toArray();
        }

        return $this->json(['Success' => true, 'users' => $return], Response::HTTP_OK);
    }

    #[Route(path: '/get-roles', name: 'getRoles', methods: ['GET'])]
    public function getRoles(): JsonResponse
    {
        $roles = [];
        array_walk_recursive($this->roleHierarchy, static function ($val) use (&$roles): void {
            $roles[] = $val;
        });

        $roles = array_values(array_unique($roles));
        foreach ($roles as $role => $value) {
            if ($value === 'ROLE_ALLOWED_TO_SWITCH') {
                unset($roles[$role]);
            }
        }

        return $this->json(['Success' => true, 'roles' => array_values(array_unique($roles))], Response::HTTP_OK);
    }

    #[Route(path: '/{uuid}', name: 'show', methods: ['GET'])]
    public function show(string $uuid): JsonResponse
    {
        $user = $this->userRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);

        if ($user instanceof User) {
            return $this->json(['user' => $user->toArray()], Response::HTTP_OK);
        }

        return $this->json(['Success' => false, 'info' => 'User not exist'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/edit', name: 'edit', methods: ['PUT'])]
    public function edit(Request $request, string $uuid): JsonResponse
    {
        $content = json_decode($request->getContent());
        if ($content) {
            try {
                $user = $this->userRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
                if ($user instanceof User) {
                    if (isset($content->firstName)) {
                        $user->setFirstName($content->firstName ? htmlspecialchars(trim((string) $content->firstName)) : $user->getFirstName());
                    }

                    if (isset($content->lastName)) {
                        $user->setLastName($content->lastName ? htmlspecialchars(trim((string) $content->lastName)) : (string) $user->getLastName());
                    }

                    if (isset($content->email)) {
                        $user->setEmail($content->email ? htmlspecialchars(trim((string) $content->email)) : $user->getEmail());
                    }

                    try {
                        $this->em->persist($user);
                        $this->em->flush();
                    } catch (\Exception $exception) {
                        $this->logger->error('Edit user error: '.$exception->getMessage().PHP_EOL.' userEdit: '.$user->getId().PHP_EOL);

                        return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                    }

                    return $this->json(['Success' => true, 'info' => 'User successfully updated', 'user' => $user->toArray()], Response::HTTP_OK);
                }

                return $this->json(['Success' => false, 'info' => 'User not exist'], Response::HTTP_BAD_REQUEST);
            } catch (\Exception $exception) {
                $this->logger->error('Edit user error: '.$exception->getMessage().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/change-password', name: 'change_password', methods: ['PUT'])]
    public function changePassword(Request $request, string $uuid): JsonResponse
    {
        $content = json_decode($request->getContent());
        if ($content) {
            $user = $this->userRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
            if ($user instanceof User) {
                $plainPassword = $content->plainPassword;
                $confirmPassword = $content->confirmPassword;

                $errors = $this->validator->validate($plainPassword, [
                    new Assert\NotBlank(),
                    new Assert\Length(['min' => 8]),
                    new Assert\Regex([
                        'pattern' => '/^(?=.*[A-Z])(?=.*\d)(?=.*[^A-Za-z0-9])/',
                    ]),
                ]);

                if (\count($errors) > 0) {
                    $this->logger->error('Change password validator error: '.PHP_EOL.' userChangePassword: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Password does not meet complexity requirements.'], Response::HTTP_BAD_REQUEST);
                }

                if ($plainPassword === $confirmPassword) {
                    $encodedPassword = $this->passwordHasher->hashPassword(
                        $user,
                        (string) $plainPassword
                    );

                    try {
                        $user->setPassword($encodedPassword);
                        $this->em->persist($user);
                        $this->em->flush();
                    } catch (\Exception $exception) {
                        $this->logger->error('Change password error: '.$exception->getMessage().PHP_EOL.' userChangePassword: '.$user->getId().PHP_EOL);

                        return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                    }

                    return $this->json(['Success' => true, 'info' => 'Change password success'], Response::HTTP_OK);
                }

                return $this->json(['Success' => false, 'info' => 'Passwords not matching'], Response::HTTP_BAD_REQUEST);
            }

            return $this->json(['Success' => false, 'info' => 'User not exist'], Response::HTTP_BAD_REQUEST);
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/activate', name: 'activate', methods: ['PUT'])]
    public function activate(string $uuid): JsonResponse
    {
        $user = $this->userRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0, 'status' => UserStatus::INACTIVE]);
        if ($user instanceof User) {
            try {
                $user->setStatus(UserStatus::ACTIVE);
                $user->setDeactivateAt(null);

                try {
                    $this->em->persist($user);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Activate user error: '.$exception->getMessage().PHP_EOL.' userActivate: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'User activated'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Activate user error: '.$exception->getMessage().PHP_EOL.' userActivate: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'User not exist or is Active'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/deactivate', name: 'deactivate', methods: ['PUT'])]
    public function deactivate(string $uuid): JsonResponse
    {
        $user = $this->userRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0, 'status' => UserStatus::ACTIVE]);
        if ($user instanceof User) {
            try {
                $user->setStatus(UserStatus::INACTIVE);
                $user->setDeactivateAt(new \DateTimeImmutable('now'));

                try {
                    $this->em->persist($user);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Deactivate user error: '.$exception->getMessage().PHP_EOL.' userDeactivate: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'User deactivated'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Deactivate user error: '.$exception->getMessage().PHP_EOL.' userDeactivate: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'User not exist or is Deactivate'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/change-role', name: 'change_role', methods: ['POST'])]
    public function changeRole(Request $request, string $uuid): JsonResponse
    {
        $content = json_decode($request->getContent());
        if ($content) {
            $user = $this->userRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
            if ($user instanceof User) {
                try {
                    $user->setRoles([$content->role]);

                    try {
                        $this->em->persist($user);
                        $this->em->flush();
                    } catch (\Exception $exception) {
                        $this->logger->error('Change Role error: '.$exception->getMessage().PHP_EOL.' userChangeRole: '.$user->getId().PHP_EOL);

                        return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                    }

                    return $this->json(['Success' => true, 'info' => 'User role was changed'], Response::HTTP_OK);
                } catch (\Exception $exception) {
                    $this->logger->error('Change Role user error: '.$exception->getMessage().PHP_EOL.' userChangeRole: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }
            }

            return $this->json(['Success' => false, 'info' => 'User not exist or is Active'], Response::HTTP_BAD_REQUEST);
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/delete', name: 'delete', methods: ['DELETE'])]
    public function delete(string $uuid): JsonResponse
    {
        $user = $this->userRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
        if ($user instanceof User) {
            try {
                $user->setDeleted(true);

                try {
                    $this->em->persist($user);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Delete user error: '.$exception->getMessage().PHP_EOL.' userDelete: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'User deleted'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Delete user error: '.$exception->getMessage().PHP_EOL.' userDelete: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'User not exist'], Response::HTTP_BAD_REQUEST);
    }
}
