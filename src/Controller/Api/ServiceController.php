<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Controller\Api;

use App\Entity\CategoryService;
use App\Entity\Service;
use App\Entity\User;
use App\Interface\TokenAuthenticatedController;
use App\Repository\CategoryServiceRepository;
use App\Repository\ServiceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/service', name: 'service_')]
class ServiceController extends AbstractController implements TokenAuthenticatedController
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ServiceRepository $serviceRepository,
        private readonly CategoryServiceRepository $categoryServiceRepository,
        private readonly LoggerInterface $logger
    ) {
    }

    #[Route(path: '/add', name: 'add', methods: ['POST'])]
    public function add(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $content = json_decode($request->getContent());
        if ($content) {
            $categoryService = $this->categoryServiceRepository->findOneBy(['uuid' => htmlspecialchars(trim((string) $content->categoryServiceUuid)), 'deleted' => 0]);
            if ($categoryService instanceof CategoryService) {
                try {
                    $service = new Service();
                    $service->setName(htmlspecialchars(trim((string) $content->name)));
                    $service->setCategoryService($categoryService);
                    $service->setActive(filter_var($content->active, FILTER_VALIDATE_BOOLEAN));
                    $service->setCreatedBy($user);
                    $service->setDeleted(false);

                    try {
                        $this->em->persist($service);
                        $this->em->flush();
                    } catch (\Exception $exception) {
                        $this->logger->error('Add Service error: '.$exception->getMessage().PHP_EOL.' userAdd: '.$user->getId().PHP_EOL);

                        return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                    }

                    return $this->json(['Success' => true, 'info' => 'Service successfully added', 'Service' => $service->toArray()], Response::HTTP_OK);
                } catch (\Exception $exception) {
                    $this->logger->error('Add Service error: '.$exception->getMessage().PHP_EOL.' userAdd: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }
            }

            return $this->json(['Success' => false, 'info' => 'CategoryService not exist'], Response::HTTP_BAD_REQUEST);
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/list-all', name: 'list_all', methods: ['GET'])]
    public function list(): JsonResponse
    {
        $Services = $this->serviceRepository->findBy(['deleted' => 0]);
        $return = [];

        foreach ($Services as $service) {
            $return[] = $service->toArray();
        }

        return $this->json(['Success' => true, 'Services' => $return], Response::HTTP_OK);
    }

    #[Route(path: '/list-active', name: 'list_active', methods: ['GET'])]
    public function listActive(): JsonResponse
    {
        $Services = $this->serviceRepository->findBy(['actived' => 0, 'deleted' => 0]);
        $return = [];

        foreach ($Services as $service) {
            $return[] = $service->toArray();
        }

        return $this->json(['Success' => true, 'Services' => $return], Response::HTTP_OK);
    }

    #[Route(path: '/{uuid}', name: 'show', methods: ['GET'])]
    public function show(string $uuid): JsonResponse
    {
        $service = $this->serviceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);

        if ($service instanceof Service) {
            return $this->json(['Service' => $service->toArray()], Response::HTTP_OK);
        }

        return $this->json(['Success' => false, 'info' => 'Service not exist'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/edit', name: 'edit', methods: ['PUT'])]
    public function edit(Request $request, string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $content = json_decode($request->getContent());
        if ($content) {
            try {
                $service = $this->serviceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
                if ($service instanceof Service) {
                    if (isset($content->name)) {
                        $service->setName($content->name ? htmlspecialchars(trim((string) $content->name)) : $service->getName());
                    }

                    if (isset($content->categoryServiceUuid)) {
                        $categoryService = $this->categoryServiceRepository->findOneBy(['uuid' => htmlspecialchars(trim((string) $content->categoryServiceUuid)), 'deleted' => 0]);
                        if (!$categoryService instanceof CategoryService) {
                            return $this->json(['Success' => false, 'info' => 'CategoryService not exist'], Response::HTTP_BAD_REQUEST);
                        }

                        $service->setCategoryService($categoryService);
                    }

                    if (isset($content->active)) {
                        $service->setActive($content->active ? filter_var($content->active, FILTER_VALIDATE_BOOLEAN) : $service->getActive());
                    }

                    $service->setUpdatedBy($user);
                    $service->setUpdatedAt(new \DateTimeImmutable('now'));

                    try {
                        $this->em->persist($service);
                        $this->em->flush();
                    } catch (\Exception $exception) {
                        $this->logger->error('Edit Service error: '.$exception->getMessage().PHP_EOL.' userEdit: '.$user->getId().PHP_EOL);

                        return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                    }

                    return $this->json(['Success' => true, 'info' => 'Service successfully updated', 'Service' => $service->toArray()], Response::HTTP_OK);
                }

                return $this->json(['Success' => false, 'info' => 'Service not exist'], Response::HTTP_BAD_REQUEST);
            } catch (\Exception $exception) {
                $this->logger->error('Edit Service error: '.$exception->getMessage().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/active', name: 'active', methods: ['PUT'])]
    public function active(string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $service = $this->serviceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
        if ($service instanceof Service) {
            try {
                $service->setActive(true);
                $service->setDeletedAt(new \DateTimeImmutable('now'));
                $service->setUpdatedBy($user);

                try {
                    $this->em->persist($service);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Active Service error: '.$exception->getMessage().PHP_EOL.' userActive: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'Service Actived'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Active Service error: '.$exception->getMessage().PHP_EOL.' userActive: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Service not exist'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/deactivate', name: 'deactivate', methods: ['PUT'])]
    public function deactivate(string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $service = $this->serviceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
        if ($service instanceof Service) {
            try {
                $service->setActive(false);
                $service->setDeletedAt(new \DateTimeImmutable('now'));
                $service->setUpdatedBy($user);

                try {
                    $this->em->persist($service);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Deactivate Service error: '.$exception->getMessage().PHP_EOL.' userDeactivatee: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'Service Deactivated'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Deactivate Service error: '.$exception->getMessage().PHP_EOL.' userDeactivate: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Service not exist'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/delete', name: 'delete', methods: ['DELETE'])]
    public function delete(string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $service = $this->serviceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
        if ($service instanceof Service) {
            try {
                $service->setDeleted(true);
                $service->setDeletedAt(new \DateTimeImmutable('now'));
                $service->setUpdatedBy($user);

                try {
                    $this->em->persist($service);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Delete Service error: '.$exception->getMessage().PHP_EOL.' userDelete: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'Service deleted'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Delete Service error: '.$exception->getMessage().PHP_EOL.' userDelete: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Service not exist'], Response::HTTP_BAD_REQUEST);
    }
}
