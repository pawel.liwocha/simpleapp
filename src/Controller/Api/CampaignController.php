<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Controller\Api;

use App\Entity\Campaign;
use App\Entity\CampaignContent;
use App\Entity\User;
use App\Enum\CampaignStatus;
use App\Enum\CampaignType;
use App\Interface\TokenAuthenticatedController;
use App\Repository\CampaignContentRepository;
use App\Repository\CampaignRepository;
use App\Service\CampaignService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/campaign', name: 'campaign_')]
class CampaignController extends AbstractController implements TokenAuthenticatedController
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly CampaignRepository $campaignRepository,
        private readonly CampaignContentRepository $campaignContentRepository,
        private readonly LoggerInterface $logger,
        private readonly CampaignService $campaignService
    ) {
    }

    #[Route(path: '/add', name: 'add', methods: ['POST'])]
    public function add(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $content = json_decode($request->getContent());
        if ($content) {
            if (!isset($content->campaignName)
                || !isset($content->startDate)
                || !isset($content->endDate)
                || !isset($content->type)
            ) {
                return $this->json(['Success' => false, 'info' => 'Content not have campaignName, startDate, endDate or type'], Response::HTTP_BAD_REQUEST);
            }

            try {
                $campaign = new Campaign();
                $campaign->setCampaignName(htmlspecialchars(trim((string) $content->campaignName)));
                $campaign->setStartDate(new \DateTimeImmutable((string) $content->startDate));
                $campaign->setEndDate(new \DateTimeImmutable((string) $content->endDate));
                $campaign->setCampaignType(CampaignType::from($content->type));
                $campaign->setDescription(isset($content->description) ? htmlspecialchars(trim((string) $content->description)) : null);
                $campaign->setStatus(CampaignStatus::CREATED);
                $campaign->setCreatedBy($user);
                $campaign->setPromo(isset($content->promo) && (bool) $content->promo);
                $campaign->setDeleted(false);

                $campaignContent = new CampaignContent();
                $campaignContent->setCampaign($campaign);
                $campaignContent->setFrom(isset($content->contentFrom) ? htmlspecialchars(trim((string) $content->contentFrom)) : null);
                $campaignContent->setText(isset($content->contentText) ? htmlspecialchars(trim((string) $content->contentText)) : null);
                $campaignContent->setCreatedBy($user);
                $campaignContent->setDeleted(false);

                try {
                    $this->em->persist($campaign);
                    $this->em->persist($campaignContent);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Add Campaign error: '.$exception->getMessage().PHP_EOL.' userCampaign: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                $data = $campaign->toArray();
                $data['from'] = $campaignContent->getFrom();
                $data['text'] = $campaignContent->getText();

                return $this->json(['Success' => true, 'info' => 'Campaign successfully added', 'campaign' => $data], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Add Campaign error: '.$exception->getMessage().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/list', name: 'list', methods: ['GET'])]
    public function list(): JsonResponse
    {
        $campaigns = $this->campaignRepository->findBy(['deleted' => 0]);
        $return = [];

        $i = 0;
        foreach ($campaigns as $campaign) {
            $campaignContent = $this->campaignContentRepository->findOneBy(['campaign' => $campaign, 'deleted' => 0]);
            $return[$i] = $campaign->toArray();
            if ($campaignContent instanceof CampaignContent) {
                $return[$i]['from'] = $campaignContent->getFrom();
                $return[$i]['text'] = $campaignContent->getText();
            }

            ++$i;
        }

        return $this->json(['Success' => true, 'campaigns' => $return], Response::HTTP_OK);
    }

    #[Route(path: '/list-active', name: 'listActive', methods: ['GET'])]
    public function listActive(): JsonResponse
    {
        $campaigns = $this->campaignRepository->findBy(['status' => CampaignStatus::ACTIVE, 'deleted' => 0]);
        $return = [];

        $i = 0;
        foreach ($campaigns as $campaign) {
            $campaignContent = $this->campaignContentRepository->findOneBy(['campaign' => $campaign, 'deleted' => 0]);
            $return[$i] = $campaign->toArray();
            if ($campaignContent instanceof CampaignContent) {
                $return[$i]['from'] = $campaignContent->getFrom();
                $return[$i]['text'] = $campaignContent->getText();
            }

            ++$i;
        }

        return $this->json(['Success' => true, 'campaigns' => $return], Response::HTTP_OK);
    }

    #[Route(path: '/list-inactive', name: 'listInactive', methods: ['GET'])]
    public function listInactive(): JsonResponse
    {
        $campaigns = $this->campaignRepository->findBy(['status' => CampaignStatus::INACTIVE, 'deleted' => 0]);
        $return = [];

        $i = 0;
        foreach ($campaigns as $campaign) {
            $campaignContent = $this->campaignContentRepository->findOneBy(['campaign' => $campaign, 'deleted' => 0]);
            $return[$i] = $campaign->toArray();
            if ($campaignContent instanceof CampaignContent) {
                $return[$i]['from'] = $campaignContent->getFrom();
                $return[$i]['text'] = $campaignContent->getText();
            }

            ++$i;
        }

        return $this->json(['Success' => true, 'campaigns' => $return], Response::HTTP_OK);
    }

    #[Route(path: '/list-created', name: 'listCreated', methods: ['GET'])]
    public function listCreated(): JsonResponse
    {
        $campaigns = $this->campaignRepository->findBy(['status' => CampaignStatus::CREATED, 'deleted' => 0]);
        $return = [];

        $i = 0;
        foreach ($campaigns as $campaign) {
            $campaignContent = $this->campaignContentRepository->findOneBy(['campaign' => $campaign, 'deleted' => 0]);
            $return[$i] = $campaign->toArray();
            if ($campaignContent instanceof CampaignContent) {
                $return[$i]['from'] = $campaignContent->getFrom();
                $return[$i]['text'] = $campaignContent->getText();
            }

            ++$i;
        }

        return $this->json(['Success' => true, 'campaigns' => $return], Response::HTTP_OK);
    }

    #[Route(path: '/list-to-send', name: 'listToSend', methods: ['GET'])]
    public function listToSend(): JsonResponse
    {
        $campaigns = $this->campaignRepository->findAllCampaignsWithStatusActiveOrCreated();
        $return = [];

        $i = 0;
        foreach ($campaigns as $campaign) {
            $campaignContent = $this->campaignContentRepository->findOneBy(['campaign' => $campaign, 'deleted' => 0]);
            $return[$i] = $campaign->toArray();
            if ($campaignContent instanceof CampaignContent) {
                $return[$i]['from'] = $campaignContent->getFrom();
                $return[$i]['text'] = $campaignContent->getText();
            }

            ++$i;
        }

        return $this->json(['Success' => true, 'campaigns' => $return], Response::HTTP_OK);
    }

    #[Route(path: '/send', name: 'send', methods: ['POST'])]
    public function send(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $content = json_decode($request->getContent());
        if ($content) {
            if (isset($content->campaignUuid, $content->leads, $content->date)) {
                $campaign = $this->campaignRepository->findOneBy(['uuid' => $content->campaignUuid, 'deleted' => 0]);
                if ($campaign instanceof Campaign) {
                    $dateSend = new \DateTimeImmutable((string) $content->date);
                    if (($dateSend >= $campaign->getStartDate()) && ($dateSend <= $campaign->getEndDate())) {
                        if ($campaign->getCampaignType() === CampaignType::WHATSAPP) {
                            try {
                                $send = $this->campaignService->prepareSendViaWhatsapp($campaign, $user, $content);
                            } catch (\Exception) {
                                return $this->json(['Success' => false, 'info' => 'Error with prepare campaign to send via whatsapp, contact with Administrators'], Response::HTTP_OK);
                            }

                            return $this->json(['Success' => true, 'info' => $send], Response::HTTP_OK);
                        }

                        if ($campaign->getCampaignType() === CampaignType::SMS) {
                            try {
                                $send = $this->campaignService->prepareSendViaSms($campaign, $user, $content);
                            } catch (\Exception) {
                                return $this->json(['Success' => false, 'info' => 'Error with prepare campaign to send via sms, contact with Administrators'], Response::HTTP_OK);
                            }

                            return $this->json(['Success' => true, 'info' => $send], Response::HTTP_OK);
                        }

                        if ($campaign->getCampaignType() === CampaignType::EMAIL) {
                            try {
                                $send = $this->campaignService->prepareSendViaEmail($campaign, $user, $content);
                            } catch (\Exception) {
                                return $this->json(['Success' => false, 'info' => 'Error with prepare campaign to send via email, contact with Administrators'], Response::HTTP_OK);
                            }

                            return $this->json(['Success' => true, 'info' => $send], Response::HTTP_OK);
                        }

                        return $this->json(['Success' => false, 'info' => 'Campaign type not recognized'], Response::HTTP_BAD_REQUEST);
                    }

                    return $this->json(['Success' => false, 'info' => 'Sent date does not fit between the Campaign start and end dates'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => false, 'info' => 'Campaign not exist'], Response::HTTP_BAD_REQUEST);
            }

            return $this->json(['Success' => false, 'info' => 'Content does not have required values'], Response::HTTP_BAD_REQUEST);
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}', name: 'show', methods: ['GET'])]
    public function show(string $uuid): JsonResponse
    {
        $campaign = $this->campaignRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);

        if ($campaign instanceof Campaign) {
            $campaignContent = $this->campaignContentRepository->findOneBy(['campaign' => $campaign, 'deleted' => 0]);
            $return = $campaign->toArray();
            if ($campaignContent instanceof CampaignContent) {
                $return['from'] = $campaignContent->getFrom();
                $return['text'] = $campaignContent->getText();
            }

            return $this->json(['campaign' => $return], Response::HTTP_OK);
        }

        return $this->json(['Success' => false, 'info' => 'Campaign not exist'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/edit', name: 'edit', methods: ['PUT'])]
    public function edit(Request $request, string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $campaign = $this->campaignRepository->findOneBy(['uuid' => $uuid, 'status' => CampaignStatus::CREATED, 'deleted' => 0]);
        $content = json_decode($request->getContent());

        if ($campaign instanceof Campaign) {
            if ($content) {
                try {
                    if (isset($content->campaignName)) {
                        $campaign->setCampaignName(htmlspecialchars(trim((string) $content->campaignName)));
                    }

                    if (isset($content->startDate)) {
                        $campaign->setStartDate(new \DateTimeImmutable((string) $content->startDate));
                    }

                    if (isset($content->endDate)) {
                        $campaign->setEndDate(new \DateTimeImmutable((string) $content->endDate));
                    }

                    if (isset($content->type)) {
                        $campaign->setCampaignType(CampaignType::from($content->type));
                    }

                    if (isset($content->description)) {
                        $campaign->setDescription(htmlspecialchars(trim((string) $content->description)));
                    }

                    if (isset($content->promo)) {
                        $campaign->setPromo((bool) $content->promo);
                    }

                    $campaign->setUpdatedBy($user);
                    $campaign->setUpdatedAt(new \DateTimeImmutable('now'));

                    $campaignContent = $this->campaignContentRepository->findOneBy(['campaign' => $campaign, 'deleted' => 0]);

                    if ($campaignContent instanceof CampaignContent) {
                        if (isset($content->campaignContentText)) {
                            $campaignContent->setText(htmlspecialchars(trim((string) $content->campaignContentText)));
                        }

                        if (isset($content->campaignContentFrom)) {
                            $campaignContent->setFrom(htmlspecialchars(trim((string) $content->campaignContentFrom)));
                        }

                        $campaignContent->setUpdatedBy($user);
                        $campaignContent->setUpdatedAt(new \DateTimeImmutable('now'));

                        $this->em->persist($campaignContent);
                    }

                    try {
                        $this->em->persist($campaign);
                        $this->em->flush();
                    } catch (\Exception $exception) {
                        $this->logger->error('Edit campaign error: '.$exception->getMessage().PHP_EOL.' userEdit: '.$user->getId().PHP_EOL);

                        return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                    }

                    $data = $campaign->toArray();
                    if ($campaignContent instanceof CampaignContent) {
                        $data['from'] = $campaignContent->getFrom();
                        $data['text'] = $campaignContent->getText();
                    }

                    return $this->json(['Success' => true, 'info' => 'Campaign edited', 'campaign' => $data], Response::HTTP_OK);
                } catch (\Exception $exception) {
                    $this->logger->error('Edit campaign error: '.$exception->getMessage().PHP_EOL.' userEdit: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }
            }

            return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
        }

        return $this->json(['Success' => false, 'info' => 'Campaign not exist or is Active/Inactive'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/activate', name: 'activate', methods: ['PUT'])]
    public function activate(string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $campaign = $this->campaignRepository->findCampaignInactiveOrCreated($uuid);
        if ($campaign instanceof Campaign) {
            try {
                $campaign->setStatus(CampaignStatus::ACTIVE);
                $campaign->setUpdatedBy($user);
                $campaign->setUpdatedAt(new \DateTimeImmutable('now'));

                try {
                    $this->em->persist($campaign);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Activate campaign error: '.$exception->getMessage().PHP_EOL.' userActivate: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'Campaign activated'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Activate campaign error: '.$exception->getMessage().PHP_EOL.' userActivate: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Campaign not exist or is Active'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/deactivate', name: 'deactivate', methods: ['PUT'])]
    public function deactivate(string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $campaign = $this->campaignRepository->findCampaignActiveOrCreated($uuid);
        if ($campaign instanceof Campaign) {
            try {
                $campaign->setStatus(CampaignStatus::INACTIVE);
                $campaign->setUpdatedBy($user);
                $campaign->setUpdatedAt(new \DateTimeImmutable('now'));

                try {
                    $this->em->persist($campaign);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Deactivate campaign error: '.$exception->getMessage().PHP_EOL.' userDeactivate: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'Campaign deactivated'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Deactivate campaign error: '.$exception->getMessage().PHP_EOL.' userDeactivate: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Campaign not exist or is Active'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/change-type', name: 'change_type', methods: ['POST'])]
    public function changeType(Request $request, string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $content = json_decode($request->getContent());
        $campaign = $this->campaignRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
        if ($campaign instanceof Campaign && $content->type) {
            try {
                $campaign->setCampaignType(CampaignType::from($content->type));
                $campaign->setUpdatedBy($user);
                $campaign->setUpdatedAt(new \DateTimeImmutable('now'));

                try {
                    $this->em->persist($campaign);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Change type campaign error: '.$exception->getMessage().PHP_EOL.' userChangeType: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'Campaign type changed'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Change type campaign error: '.$exception->getMessage().PHP_EOL.' userChangeType: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Campaign not exist or content type not send'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/change-promo', name: 'change_promo', methods: ['PUT'])]
    public function changePromo(string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $campaign = $this->campaignRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
        if ($campaign instanceof Campaign) {
            try {
                if ($campaign->isPromo()) {
                    $campaign->setPromo(false);
                } else {
                    $campaign->setPromo(true);
                }

                $campaign->setUpdatedBy($user);
                $campaign->setUpdatedAt(new \DateTimeImmutable('now'));

                try {
                    $this->em->persist($campaign);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Change promo campaign error: '.$exception->getMessage().PHP_EOL.' userChangePromo: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'Campaign promo changed'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Change promo campaign error: '.$exception->getMessage().PHP_EOL.' userChangePromo: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Campaign not exist'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/delete', name: 'delete', methods: ['DELETE'])]
    public function delete(string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $campaign = $this->campaignRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
        if ($campaign instanceof Campaign) {
            try {
                $campaign->setDeleted(true);
                $campaign->setDeletedAt(new \DateTimeImmutable('now'));
                $campaign->setUpdatedBy($user);

                try {
                    $this->em->persist($campaign);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Delete campaign error: '.$exception->getMessage().PHP_EOL.' userDelete: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'Campaign deleted'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Delete campaign error: '.$exception->getMessage().PHP_EOL.' userDelete: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Campaign not exist'], Response::HTTP_BAD_REQUEST);
    }
}
