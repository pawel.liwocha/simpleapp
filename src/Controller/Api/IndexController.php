<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Controller\Api;

use App\Interface\TokenAuthenticatedController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class IndexController extends AbstractController implements TokenAuthenticatedController
{
    #[Route(path: '/', name: 'index')]
    public function index(): Response
    {
        return new Response(
            'Index of SimpleApp API',
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );
    }
}
