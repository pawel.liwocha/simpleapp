<?php

declare(strict_types=1);

/**
 * SimpleApp API.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2024  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Controller\Api;

use App\Entity\CategoryService;
use App\Entity\ParentCategoryService;
use App\Entity\User;
use App\Interface\TokenAuthenticatedController;
use App\Repository\CategoryServiceRepository;
use App\Repository\ParentCategoryServiceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/category-service', name: 'category_service_')]
class CategoryServiceController extends AbstractController implements TokenAuthenticatedController
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly CategoryServiceRepository $categoryServiceRepository,
        private readonly ParentCategoryServiceRepository $parentCategoryServiceRepository,
        private readonly LoggerInterface $logger
    ) {
    }

    #[Route(path: '/add', name: 'add', methods: ['POST'])]
    public function add(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $content = json_decode($request->getContent());
        if ($content) {
            $parentCategoryService = $this->parentCategoryServiceRepository->findOneBy(['uuid' => htmlspecialchars(trim((string) $content->parentCategoryServiceUuid)), 'deleted' => 0]);
            if ($parentCategoryService instanceof ParentCategoryService) {
                try {
                    $categoryService = new CategoryService();
                    $categoryService->setName(htmlspecialchars(trim((string) $content->name)));
                    $categoryService->setParentCategoryService($parentCategoryService);
                    $categoryService->setActive(filter_var($content->active, FILTER_VALIDATE_BOOLEAN));
                    $categoryService->setCreatedBy($user);
                    $categoryService->setDeleted(false);

                    try {
                        $this->em->persist($categoryService);
                        $this->em->flush();
                    } catch (\Exception $exception) {
                        $this->logger->error('Add CategoryService error: '.$exception->getMessage().PHP_EOL.' userAdd: '.$user->getId().PHP_EOL);

                        return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                    }

                    return $this->json(['Success' => true, 'info' => 'CategoryService successfully added', 'CategoryService' => $categoryService->toArray()], Response::HTTP_OK);
                } catch (\Exception $exception) {
                    $this->logger->error('Add CategoryService error: '.$exception->getMessage().PHP_EOL.' userAdd: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }
            }

            return $this->json(['Success' => false, 'info' => 'ParentCategoryService not exist'], Response::HTTP_BAD_REQUEST);
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/list-all', name: 'list_all', methods: ['GET'])]
    public function list(): JsonResponse
    {
        $CategoryServices = $this->categoryServiceRepository->findBy(['deleted' => 0]);
        $return = [];

        foreach ($CategoryServices as $categoryService) {
            $return[] = $categoryService->toArray();
        }

        return $this->json(['Success' => true, 'CategoryServices' => $return], Response::HTTP_OK);
    }

    #[Route(path: '/list-active', name: 'list_active', methods: ['GET'])]
    public function listActive(): JsonResponse
    {
        $CategoryServices = $this->categoryServiceRepository->findBy(['actived' => 0, 'deleted' => 0]);
        $return = [];

        foreach ($CategoryServices as $categoryService) {
            $return[] = $categoryService->toArray();
        }

        return $this->json(['Success' => true, 'CategoryServices' => $return], Response::HTTP_OK);
    }

    #[Route(path: '/{uuid}', name: 'show', methods: ['GET'])]
    public function show(string $uuid): JsonResponse
    {
        $categoryService = $this->categoryServiceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);

        if ($categoryService instanceof CategoryService) {
            return $this->json(['CategoryService' => $categoryService->toArray()], Response::HTTP_OK);
        }

        return $this->json(['Success' => false, 'info' => 'CategoryService not exist'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/edit', name: 'edit', methods: ['PUT'])]
    public function edit(Request $request, string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $content = json_decode($request->getContent());
        if ($content) {
            try {
                $categoryService = $this->categoryServiceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
                if ($categoryService instanceof CategoryService) {
                    if (isset($content->name)) {
                        $categoryService->setName($content->name ? htmlspecialchars(trim((string) $content->name)) : $categoryService->getName());
                    }

                    if (isset($content->parentCategoryServiceUuid)) {
                        $parentCategoryService = $this->parentCategoryServiceRepository->findOneBy(['uuid' => htmlspecialchars(trim((string) $content->parentCategoryServiceUuid)), 'deleted' => 0]);
                        if (!$parentCategoryService instanceof ParentCategoryService) {
                            return $this->json(['Success' => false, 'info' => 'ParentCategoryService not exist'], Response::HTTP_BAD_REQUEST);
                        }

                        $categoryService->setParentCategoryService($parentCategoryService);
                    }

                    if (isset($content->active)) {
                        $categoryService->setActive($content->active ? filter_var($content->active, FILTER_VALIDATE_BOOLEAN) : $categoryService->getActive());
                    }

                    $categoryService->setUpdatedBy($user);
                    $categoryService->setUpdatedAt(new \DateTimeImmutable('now'));

                    try {
                        $this->em->persist($categoryService);
                        $this->em->flush();
                    } catch (\Exception $exception) {
                        $this->logger->error('Edit CategoryService error: '.$exception->getMessage().PHP_EOL.' userEdit: '.$user->getId().PHP_EOL);

                        return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                    }

                    return $this->json(['Success' => true, 'info' => 'CategoryService successfully updated', 'CategoryService' => $categoryService->toArray()], Response::HTTP_OK);
                }

                return $this->json(['Success' => false, 'info' => 'CategoryService not exist'], Response::HTTP_BAD_REQUEST);
            } catch (\Exception $exception) {
                $this->logger->error('Edit CategoryService error: '.$exception->getMessage().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'Content empty'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/active', name: 'active', methods: ['PUT'])]
    public function active(string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $categoryService = $this->categoryServiceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
        if ($categoryService instanceof CategoryService) {
            try {
                $categoryService->setActive(true);
                $categoryService->setDeletedAt(new \DateTimeImmutable('now'));
                $categoryService->setUpdatedBy($user);

                try {
                    $this->em->persist($categoryService);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Active CategoryService error: '.$exception->getMessage().PHP_EOL.' userActive: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'CategoryService Actived'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Active CategoryService error: '.$exception->getMessage().PHP_EOL.' userActive: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'CategoryService not exist'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/deactivate', name: 'deactivate', methods: ['PUT'])]
    public function deactivate(string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $categoryService = $this->categoryServiceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
        if ($categoryService instanceof CategoryService) {
            try {
                $categoryService->setActive(false);
                $categoryService->setDeletedAt(new \DateTimeImmutable('now'));
                $categoryService->setUpdatedBy($user);

                try {
                    $this->em->persist($categoryService);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Deactivate CategoryService error: '.$exception->getMessage().PHP_EOL.' userDeactivatee: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'CategoryService Deactivated'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Deactivate CategoryService error: '.$exception->getMessage().PHP_EOL.' userDeactivate: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'CategoryService not exist'], Response::HTTP_BAD_REQUEST);
    }

    #[Route(path: '/{uuid}/delete', name: 'delete', methods: ['DELETE'])]
    public function delete(string $uuid): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $categoryService = $this->categoryServiceRepository->findOneBy(['uuid' => $uuid, 'deleted' => 0]);
        if ($categoryService instanceof CategoryService) {
            try {
                $categoryService->setDeleted(true);
                $categoryService->setDeletedAt(new \DateTimeImmutable('now'));
                $categoryService->setUpdatedBy($user);

                try {
                    $this->em->persist($categoryService);
                    $this->em->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Delete CategoryService error: '.$exception->getMessage().PHP_EOL.' userDelete: '.$user->getId().PHP_EOL);

                    return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
                }

                return $this->json(['Success' => true, 'info' => 'CategoryService deleted'], Response::HTTP_OK);
            } catch (\Exception $exception) {
                $this->logger->error('Delete CategoryService error: '.$exception->getMessage().PHP_EOL.' userDelete: '.$user->getId().PHP_EOL);

                return $this->json(['Success' => false, 'info' => 'Save error'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json(['Success' => false, 'info' => 'CategoryService not exist'], Response::HTTP_BAD_REQUEST);
    }
}
