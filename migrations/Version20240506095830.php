<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240506095830 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE campaign (
          id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
          uuid VARCHAR(255) NOT NULL,
          campaign_name VARCHAR(255) NOT NULL,
          start_date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          end_date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          description VARCHAR(500) DEFAULT NULL,
          TYPE VARCHAR(50) DEFAULT \'whatsapp\' NOT NULL,
          STATUS VARCHAR(50) DEFAULT \'created\' NOT NULL,
          promo TINYINT(1) DEFAULT 0 NOT NULL,
          deleted TINYINT(1) DEFAULT 0 NOT NULL,
          created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          created_by_id BIGINT UNSIGNED DEFAULT NULL,
          updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          updated_by_id BIGINT UNSIGNED DEFAULT NULL,
          deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          UNIQUE INDEX UNIQ_1F1512DDD17F50A6 (uuid) USING BTREE,
          INDEX IDX_1F1512DDB03A8386 (created_by_id) USING BTREE,
          INDEX IDX_1F1512DD896DBBDE (updated_by_id) USING BTREE,
          INDEX index_id (id) USING BTREE,
          INDEX index_id_deleted (id, deleted) USING BTREE,
          INDEX index_uuid (uuid) USING BTREE,
          INDEX index_uuid_deleted (uuid, deleted) USING BTREE,
          INDEX index_uuid_status_deleted (uuid, STATUS, deleted) USING BTREE,
          INDEX index_start_end_deleted (start_date, end_date, deleted) USING BTREE,
          INDEX index_start_end_status_deleted (
            start_date, end_date, STATUS, deleted
          ) USING BTREE,
          INDEX index_start_end_type_deleted (
            start_date, end_date, TYPE, deleted
          ) USING BTREE,
          INDEX index_type_deleted (TYPE, deleted) USING BTREE,
          INDEX index_status_deleted (STATUS, deleted) USING BTREE,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE campaign_content (
          id INT AUTO_INCREMENT NOT NULL,
          campaign_id BIGINT UNSIGNED DEFAULT NULL,
          created_by_id BIGINT UNSIGNED DEFAULT NULL,
          updated_by_id BIGINT UNSIGNED DEFAULT NULL,
          created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          uuid VARCHAR(255) NOT NULL,
          froms VARCHAR(255) DEFAULT NULL,
          texts LONGTEXT DEFAULT NULL,
          deleted TINYINT(1) DEFAULT 0 NOT NULL,
          updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          UNIQUE INDEX UNIQ_567FAC85D17F50A6 (uuid) USING BTREE,
          INDEX IDX_567FAC85F639F774 (campaign_id) USING BTREE,
          INDEX IDX_567FAC85B03A8386 (created_by_id) USING BTREE,
          INDEX IDX_567FAC85896DBBDE (updated_by_id) USING BTREE,
          INDEX index_id (id) USING BTREE,
          INDEX index_uuid (uuid) USING BTREE,
          INDEX index_uuid_deleted (uuid, deleted) USING BTREE,
          INDEX index_campaign_deleted (campaign_id, deleted) USING BTREE,
          INDEX index_created_by_deleted (created_by_id, deleted) USING BTREE,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE campaign_sent_to_lead (
          id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
          campaign_id BIGINT UNSIGNED NOT NULL,
          lead_id BIGINT UNSIGNED NOT NULL,
          created_by_id BIGINT UNSIGNED DEFAULT NULL,
          updated_by_id BIGINT UNSIGNED DEFAULT NULL,
          created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          uuid VARCHAR(255) NOT NULL,
          description VARCHAR(1000) DEFAULT NULL,
          STATUS VARCHAR(50) DEFAULT \'planned\' NOT NULL,
          sent_date DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          send_date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          message_whats_app_id VARCHAR(255) DEFAULT NULL,
          message_sms_id VARCHAR(255) DEFAULT NULL,
          message_email_id VARCHAR(255) DEFAULT NULL,
          deleted TINYINT(1) DEFAULT 0 NOT NULL,
          updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          UNIQUE INDEX UNIQ_E8469625D17F50A6 (uuid) USING BTREE,
          INDEX IDX_E8469625B03A8386 (created_by_id) USING BTREE,
          INDEX IDX_E8469625896DBBDE (updated_by_id) USING BTREE,
          INDEX index_id (id) USING BTREE,
          INDEX index_campaign (campaign_id) USING BTREE,
          INDEX index_lead (lead_id) USING BTREE,
          INDEX index_senddate_status_deleted (send_date, STATUS, deleted) USING BTREE,
          INDEX index_campaign_deleted (campaign_id, deleted) USING BTREE,
          INDEX index_lead_deleted (lead_id, deleted) USING BTREE,
          INDEX index_status_deleted (STATUS, deleted) USING BTREE,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_service (
          id INT AUTO_INCREMENT NOT NULL,
          parent_category_service_id INT DEFAULT NULL,
          created_by_id BIGINT UNSIGNED DEFAULT NULL,
          updated_by_id BIGINT UNSIGNED DEFAULT NULL,
          created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          uuid VARCHAR(255) NOT NULL,
          name VARCHAR(255) NOT NULL,
          actived TINYINT(1) DEFAULT 1 NOT NULL,
          deleted TINYINT(1) DEFAULT 0 NOT NULL,
          updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          UNIQUE INDEX UNIQ_2645DAACD17F50A6 (uuid) USING BTREE,
          INDEX IDX_2645DAAC9DF66FE (parent_category_service_id) USING BTREE,
          INDEX IDX_2645DAACB03A8386 (created_by_id) USING BTREE,
          INDEX IDX_2645DAAC896DBBDE (updated_by_id) USING BTREE,
          INDEX index_id (id) USING BTREE,
          INDEX index_uuid (name) USING BTREE,
          INDEX index_uuid_deleted (uuid, deleted) USING BTREE,
          INDEX index_pcs_deleted (
            parent_category_service_id, deleted
          ) USING BTREE,
          INDEX index_name_deleted (name, deleted) USING BTREE,
          INDEX index_name_active_deleted (name, actived, deleted) USING BTREE,
          INDEX index_pcs_active_deleted (
            parent_category_service_id, actived,
            deleted
          ) USING BTREE,
          INDEX index_created_by_deleted (created_by_id, deleted) USING BTREE,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE failure_login_attempt (
          id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
          created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          ip VARCHAR(255) DEFAULT NULL,
          username VARCHAR(255) NOT NULL,
          data JSON DEFAULT NULL COMMENT \'(DC2Type:json)\',
          INDEX index_id (id) USING BTREE,
          INDEX index_username (username) USING BTREE,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lead (
          id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
          created_by_id BIGINT UNSIGNED DEFAULT NULL,
          updated_by_id BIGINT UNSIGNED DEFAULT NULL,
          created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          uuid VARCHAR(255) NOT NULL,
          sourced_date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          source VARCHAR(255) DEFAULT NULL,
          first_name VARCHAR(255) DEFAULT NULL,
          last_name VARCHAR(255) DEFAULT NULL,
          mobile_number VARCHAR(50) DEFAULT NULL,
          email VARCHAR(180) DEFAULT NULL,
          inquired_treatment VARCHAR(800) DEFAULT NULL,
          COMMENT VARCHAR(800) DEFAULT NULL,
          last_contact_date DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          next_contact_date DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          deleted TINYINT(1) DEFAULT 0 NOT NULL,
          updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          UNIQUE INDEX UNIQ_289161CBD17F50A6 (uuid) USING BTREE,
          INDEX IDX_289161CBB03A8386 (created_by_id) USING BTREE,
          INDEX IDX_289161CB896DBBDE (updated_by_id) USING BTREE,
          INDEX index_id (id) USING BTREE,
          INDEX index_id_deleted (id, deleted) USING BTREE,
          INDEX index_uuid (uuid) USING BTREE,
          INDEX index_uuid_deleted (uuid, deleted) USING BTREE,
          INDEX index_email_mobile_number_deleted (email, mobile_number, deleted) USING BTREE,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE login_log (
          id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
          user_id BIGINT UNSIGNED DEFAULT NULL,
          created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          token VARCHAR(255) DEFAULT NULL,
          user_agent VARCHAR(1000) DEFAULT NULL,
          ip VARCHAR(100) DEFAULT NULL,
          STATUS VARCHAR(50) DEFAULT \'success\' NOT NULL,
          INDEX index_id (id) USING BTREE,
          INDEX index_user (user_id) USING BTREE,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parent_category_service (
          id INT AUTO_INCREMENT NOT NULL,
          created_by_id BIGINT UNSIGNED DEFAULT NULL,
          updated_by_id BIGINT UNSIGNED DEFAULT NULL,
          created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          uuid VARCHAR(255) NOT NULL,
          name VARCHAR(255) NOT NULL,
          actived TINYINT(1) DEFAULT 1 NOT NULL,
          deleted TINYINT(1) DEFAULT 0 NOT NULL,
          updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          UNIQUE INDEX UNIQ_92D9A89FD17F50A6 (uuid) USING BTREE,
          INDEX IDX_92D9A89FB03A8386 (created_by_id) USING BTREE,
          INDEX IDX_92D9A89F896DBBDE (updated_by_id) USING BTREE,
          INDEX index_id (id) USING BTREE,
          INDEX index_uuid (name) USING BTREE,
          INDEX index_uuid_deleted (uuid, deleted) USING BTREE,
          INDEX index_name_deleted (name, deleted) USING BTREE,
          INDEX index_name_active_deleted (name, actived, deleted) USING BTREE,
          INDEX index_created_by_deleted (created_by_id, deleted) USING BTREE,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (
          id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
          user_id BIGINT UNSIGNED NOT NULL,
          selector VARCHAR(20) NOT NULL,
          hashed_token VARCHAR(100) NOT NULL,
          requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          INDEX IDX_7CE748AA76ED395 (user_id) USING BTREE,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE service (
          id INT AUTO_INCREMENT NOT NULL,
          category_service_id INT DEFAULT NULL,
          created_by_id BIGINT UNSIGNED DEFAULT NULL,
          updated_by_id BIGINT UNSIGNED DEFAULT NULL,
          created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          uuid VARCHAR(255) NOT NULL,
          name VARCHAR(255) NOT NULL,
          actived TINYINT(1) DEFAULT 1 NOT NULL,
          deleted TINYINT(1) DEFAULT 0 NOT NULL,
          updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          UNIQUE INDEX UNIQ_E19D9AD2D17F50A6 (uuid) USING BTREE,
          INDEX IDX_E19D9AD2CB42F998 (category_service_id) USING BTREE,
          INDEX IDX_E19D9AD2B03A8386 (created_by_id) USING BTREE,
          INDEX IDX_E19D9AD2896DBBDE (updated_by_id) USING BTREE,
          INDEX index_id (id) USING BTREE,
          INDEX index_uuid (name) USING BTREE,
          INDEX index_uuid_deleted (uuid, deleted) USING BTREE,
          INDEX index_cs_deleted (category_service_id, deleted) USING BTREE,
          INDEX index_name_deleted (name, deleted) USING BTREE,
          INDEX index_name_active_deleted (name, actived, deleted) USING BTREE,
          INDEX index_cs_active_deleted (
            category_service_id, actived, deleted
          ) USING BTREE,
          INDEX index_created_by_deleted (created_by_id, deleted) USING BTREE,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE token (
          id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
          user_id BIGINT UNSIGNED DEFAULT NULL,
          created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          token VARCHAR(255) DEFAULT NULL,
          expired DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          INDEX index_id (id) USING BTREE,
          INDEX index_user (user_id) USING BTREE,
          INDEX index_token (token) USING BTREE,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (
          id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
          created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          uuid VARCHAR(255) NOT NULL,
          user_name VARCHAR(255) NOT NULL,
          first_name VARCHAR(255) NOT NULL,
          last_name VARCHAR(255) NOT NULL,
          email VARCHAR(180) NOT NULL,
          PASSWORD VARCHAR(180) NOT NULL,
          STATUS VARCHAR(50) DEFAULT \'active\' NOT NULL,
          roles JSON NOT NULL COMMENT \'(DC2Type:json)\',
          deleted TINYINT(1) DEFAULT 0 NOT NULL,
          deactivate_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          deleted_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          UNIQUE INDEX UNIQ_1483A5E9D17F50A6 (uuid) USING BTREE,
          UNIQUE INDEX UNIQ_1483A5E924A232CF (user_name) USING BTREE,
          INDEX index_id (id) USING BTREE,
          INDEX index_id_deleted (id, deleted) USING BTREE,
          INDEX index_uuid (uuid) USING BTREE,
          INDEX index_uuid_deleted (uuid, deleted) USING BTREE,
          INDEX index_username (user_name) USING BTREE,
          INDEX index_username_deleted (user_name, deleted) USING BTREE,
          INDEX index_username_status_deleted (user_name, STATUS, deleted) USING BTREE,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (
          id BIGINT AUTO_INCREMENT NOT NULL,
          body LONGTEXT NOT NULL,
          headers LONGTEXT NOT NULL,
          queue_name VARCHAR(190) NOT NULL,
          created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
          INDEX IDX_75EA56E0FB7336F0 (queue_name) USING BTREE,
          INDEX IDX_75EA56E0E3BD61CE (available_at) USING BTREE,
          INDEX IDX_75EA56E016BA31DB (delivered_at) USING BTREE,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE
          campaign
        ADD
          CONSTRAINT FK_1F1512DDB03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          campaign
        ADD
          CONSTRAINT FK_1F1512DD896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          campaign_content
        ADD
          CONSTRAINT FK_567FAC85F639F774 FOREIGN KEY (campaign_id) REFERENCES campaign (id)');
        $this->addSql('ALTER TABLE
          campaign_content
        ADD
          CONSTRAINT FK_567FAC85B03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          campaign_content
        ADD
          CONSTRAINT FK_567FAC85896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          campaign_sent_to_lead
        ADD
          CONSTRAINT FK_E8469625F639F774 FOREIGN KEY (campaign_id) REFERENCES campaign (id)');
        $this->addSql('ALTER TABLE
          campaign_sent_to_lead
        ADD
          CONSTRAINT FK_E846962555458D FOREIGN KEY (lead_id) REFERENCES lead (id)');
        $this->addSql('ALTER TABLE
          campaign_sent_to_lead
        ADD
          CONSTRAINT FK_E8469625B03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          campaign_sent_to_lead
        ADD
          CONSTRAINT FK_E8469625896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          category_service
        ADD
          CONSTRAINT FK_2645DAAC9DF66FE FOREIGN KEY (parent_category_service_id) REFERENCES parent_category_service (id)');
        $this->addSql('ALTER TABLE
          category_service
        ADD
          CONSTRAINT FK_2645DAACB03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          category_service
        ADD
          CONSTRAINT FK_2645DAAC896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          lead
        ADD
          CONSTRAINT FK_289161CBB03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          lead
        ADD
          CONSTRAINT FK_289161CB896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          login_log
        ADD
          CONSTRAINT FK_F16D9FFFA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          parent_category_service
        ADD
          CONSTRAINT FK_92D9A89FB03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          parent_category_service
        ADD
          CONSTRAINT FK_92D9A89F896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          reset_password_request
        ADD
          CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          service
        ADD
          CONSTRAINT FK_E19D9AD2CB42F998 FOREIGN KEY (category_service_id) REFERENCES category_service (id)');
        $this->addSql('ALTER TABLE
          service
        ADD
          CONSTRAINT FK_E19D9AD2B03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          service
        ADD
          CONSTRAINT FK_E19D9AD2896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE
          token
        ADD
          CONSTRAINT FK_5F37A13BA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE campaign DROP FOREIGN KEY FK_1F1512DDB03A8386');
        $this->addSql('ALTER TABLE campaign DROP FOREIGN KEY FK_1F1512DD896DBBDE');
        $this->addSql('ALTER TABLE campaign_content DROP FOREIGN KEY FK_567FAC85F639F774');
        $this->addSql('ALTER TABLE campaign_content DROP FOREIGN KEY FK_567FAC85B03A8386');
        $this->addSql('ALTER TABLE campaign_content DROP FOREIGN KEY FK_567FAC85896DBBDE');
        $this->addSql('ALTER TABLE campaign_sent_to_lead DROP FOREIGN KEY FK_E8469625F639F774');
        $this->addSql('ALTER TABLE campaign_sent_to_lead DROP FOREIGN KEY FK_E846962555458D');
        $this->addSql('ALTER TABLE campaign_sent_to_lead DROP FOREIGN KEY FK_E8469625B03A8386');
        $this->addSql('ALTER TABLE campaign_sent_to_lead DROP FOREIGN KEY FK_E8469625896DBBDE');
        $this->addSql('ALTER TABLE category_service DROP FOREIGN KEY FK_2645DAAC9DF66FE');
        $this->addSql('ALTER TABLE category_service DROP FOREIGN KEY FK_2645DAACB03A8386');
        $this->addSql('ALTER TABLE category_service DROP FOREIGN KEY FK_2645DAAC896DBBDE');
        $this->addSql('ALTER TABLE lead DROP FOREIGN KEY FK_289161CBB03A8386');
        $this->addSql('ALTER TABLE lead DROP FOREIGN KEY FK_289161CB896DBBDE');
        $this->addSql('ALTER TABLE login_log DROP FOREIGN KEY FK_F16D9FFFA76ED395');
        $this->addSql('ALTER TABLE parent_category_service DROP FOREIGN KEY FK_92D9A89FB03A8386');
        $this->addSql('ALTER TABLE parent_category_service DROP FOREIGN KEY FK_92D9A89F896DBBDE');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD2CB42F998');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD2B03A8386');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD2896DBBDE');
        $this->addSql('ALTER TABLE token DROP FOREIGN KEY FK_5F37A13BA76ED395');
        $this->addSql('DROP TABLE campaign');
        $this->addSql('DROP TABLE campaign_content');
        $this->addSql('DROP TABLE campaign_sent_to_lead');
        $this->addSql('DROP TABLE category_service');
        $this->addSql('DROP TABLE failure_login_attempt');
        $this->addSql('DROP TABLE lead');
        $this->addSql('DROP TABLE login_log');
        $this->addSql('DROP TABLE parent_category_service');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE service');
        $this->addSql('DROP TABLE token');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
