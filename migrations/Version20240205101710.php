<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240205101710 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX index_campaign_senddate_status_deleted ON campaign_sent_to_lead');
        $this->addSql('CREATE INDEX index_senddate_status_deleted ON campaign_sent_to_lead (send_date, status, deleted)');
    }

    public function down(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('DROP INDEX index_senddate_status_deleted ON campaign_sent_to_lead');
        $this->addSql('CREATE NONCLUSTERED INDEX index_campaign_sentdate_status_deleted ON campaign_sent_to_lead (campaign_id, sent_date, status, deleted)');
    }
}
