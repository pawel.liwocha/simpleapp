<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240209091050 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE campaign_sent_to_lead ADD message_whats_app_id NVARCHAR(255)');
        $this->addSql('ALTER TABLE campaign_sent_to_lead ADD message_sms_id NVARCHAR(255)');
        $this->addSql('ALTER TABLE campaign_sent_to_lead ADD message_email_id NVARCHAR(255)');
        $this->addSql('ALTER TABLE campaign_sent_to_lead ALTER COLUMN send_date DATETIME2(6) NOT NULL');
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'campaign_sent_to_lead', N'COLUMN', send_date");
    }

    public function down(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('ALTER TABLE campaign_sent_to_lead DROP COLUMN message_whats_app_id');
        $this->addSql('ALTER TABLE campaign_sent_to_lead DROP COLUMN message_sms_id');
        $this->addSql('ALTER TABLE campaign_sent_to_lead DROP COLUMN message_email_id');
        $this->addSql('ALTER TABLE campaign_sent_to_lead ALTER COLUMN send_date DATETIME2(6) NOT NULL');
        $this->addSql("EXEC sp_dropextendedproperty N'MS_Description', N'SCHEMA', 'dbo', N'TABLE', 'campaign_sent_to_lead', N'COLUMN', send_date");
    }
}
