<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240201122958 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE campaign_sent_to_lead (
    id BIGINT IDENTITY NOT NULL,
    uuid NVARCHAR(255) NOT NULL,
    campaign_id BIGINT NOT NULL,
    lead_id BIGINT NOT NULL,
    description NVARCHAR(1000),
    status NVARCHAR(50) NOT NULL,
    sent_date DATETIME2(6),
    send_date DATETIME2(6) NOT NULL,
    deleted BIT NOT NULL,
    created_by_id BIGINT,
    created_at DATETIME2(6) NOT NULL,
    updated_by_id BIGINT,
    updated_at DATETIME2(6),
    deleted_at DATETIME2(6),
    PRIMARY KEY (id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_49E2E443D17F50A6 ON campaign_sent_to_lead (uuid) WHERE uuid IS NOT NULL');
        $this->addSql('CREATE INDEX IDX_49E2E443B03A8386 ON campaign_sent_to_lead (created_by_id)');
        $this->addSql('CREATE INDEX IDX_49E2E443896DBBDE ON campaign_sent_to_lead (updated_by_id)');
        $this->addSql('CREATE INDEX index_id ON campaign_sent_to_lead (id)');
        $this->addSql('CREATE INDEX index_campaign ON campaign_sent_to_lead (campaign_id)');
        $this->addSql('CREATE INDEX index_lead ON campaign_sent_to_lead (lead_id)');
        $this->addSql('CREATE INDEX index_campaign_deleted ON campaign_sent_to_lead (campaign_id, deleted)');
        $this->addSql('CREATE INDEX index_lead_deleted ON campaign_sent_to_lead (lead_id, deleted)');
        $this->addSql('CREATE INDEX index_status_deleted ON campaign_sent_to_lead (status, deleted)');
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'campaign_sent_to_lead', N'COLUMN', created_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'campaign_sent_to_lead', N'COLUMN', sent_date");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'campaign_sent_to_lead', N'COLUMN', updated_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'campaign_sent_to_lead', N'COLUMN', deleted_at");
        $this->addSql('ALTER TABLE campaign_sent_to_lead ADD CONSTRAINT DF_49E2E443_8B8E8428 DEFAULT CURRENT_TIMESTAMP FOR created_at');
        $this->addSql("ALTER TABLE campaign_sent_to_lead ADD CONSTRAINT DF_49E2E443_7B00651C DEFAULT 'planned' FOR status");
        $this->addSql('ALTER TABLE campaign_sent_to_lead ADD CONSTRAINT DF_49E2E443_EB3B4E33 DEFAULT 0 FOR deleted');
        $this->addSql('ALTER TABLE campaign_sent_to_lead ADD CONSTRAINT FK_49E2E443F639F774 FOREIGN KEY (campaign_id) REFERENCES campaign (id)');
        $this->addSql('ALTER TABLE campaign_sent_to_lead ADD CONSTRAINT FK_49E2E44355458D FOREIGN KEY (lead_id) REFERENCES lead (id)');
        $this->addSql('ALTER TABLE campaign_sent_to_lead ADD CONSTRAINT FK_49E2E443B03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE campaign_sent_to_lead ADD CONSTRAINT FK_49E2E443896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
    }

    public function down(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('ALTER TABLE campaign_sent_to_lead DROP CONSTRAINT FK_49E2E443F639F774');
        $this->addSql('ALTER TABLE campaign_sent_to_lead DROP CONSTRAINT FK_49E2E44355458D');
        $this->addSql('ALTER TABLE campaign_sent_to_lead DROP CONSTRAINT FK_49E2E443B03A8386');
        $this->addSql('ALTER TABLE campaign_sent_to_lead DROP CONSTRAINT FK_49E2E443896DBBDE');
        $this->addSql('DROP TABLE campaign_sent_to_lead');
    }
}
