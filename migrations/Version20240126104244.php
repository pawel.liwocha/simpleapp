<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240126104244 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE lead (
            id BIGINT IDENTITY NOT NULL,
            uuid NVARCHAR(255) NOT NULL,
            sourced_date DATETIME2(6) NOT NULL,
            source NVARCHAR(255),
            first_name NVARCHAR(255),
            last_name NVARCHAR(255),
            mobile_number NVARCHAR(50),
            email NVARCHAR(180),
            inquired_treatment NVARCHAR(800),
            comment NVARCHAR(800),
            deleted BIT NOT NULL,
            created_by_id BIGINT,
            created_at DATETIME2(6) NOT NULL,
            updated_by_id BIGINT,
            updated_at DATETIME2(6),
            deleted_at DATETIME2(6),
            PRIMARY KEY (id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F995283D17F50A6 ON lead (uuid) WHERE uuid IS NOT NULL');
        $this->addSql('CREATE INDEX IDX_F995283B03A8386 ON lead (created_by_id)');
        $this->addSql('CREATE INDEX IDX_F995283896DBBDE ON lead (updated_by_id)');
        $this->addSql('CREATE INDEX index_id ON lead (id)');
        $this->addSql('CREATE INDEX index_id_deleted ON lead (id, deleted)');
        $this->addSql('CREATE INDEX index_uuid ON lead (uuid)');
        $this->addSql('CREATE INDEX index_uuid_deleted ON lead (uuid, deleted)');
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'lead', N'COLUMN', created_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'lead', N'COLUMN', sourced_date");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'lead', N'COLUMN', updated_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'lead', N'COLUMN', deleted_at");
        $this->addSql('ALTER TABLE lead ADD CONSTRAINT DF_F995283_8B8E8428 DEFAULT CURRENT_TIMESTAMP FOR created_at');
        $this->addSql('ALTER TABLE lead ADD CONSTRAINT DF_F995283_EB3B4E33 DEFAULT 0 FOR deleted');
        $this->addSql('ALTER TABLE lead ADD CONSTRAINT FK_F995283B03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE lead ADD CONSTRAINT FK_F995283896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
        $this->addSql("EXEC sp_rename N'users.uniq_6ca36478e7927c74', N'UNIQ_6CA3647824A232CF', N'INDEX'");
    }

    public function down(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('ALTER TABLE lead DROP CONSTRAINT FK_F995283B03A8386');
        $this->addSql('ALTER TABLE lead DROP CONSTRAINT FK_F995283896DBBDE');
        $this->addSql('DROP TABLE lead');
        $this->addSql("EXEC sp_rename N'users.uniq_6ca3647824a232cf', N'UNIQ_6CA36478E7927C74', N'INDEX'");
    }
}
