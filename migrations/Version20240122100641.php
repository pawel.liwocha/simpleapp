<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240122100641 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE reset_password_request (
            id BIGINT IDENTITY NOT NULL,
            user_id BIGINT NOT NULL,
            selector NVARCHAR(20) NOT NULL,
            hashed_token NVARCHAR(100) NOT NULL,
            requested_at DATETIME2(6) NOT NULL,
            expires_at DATETIME2(6) NOT NULL,
            PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_A0ADF42EA76ED395 ON reset_password_request (user_id)');
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'reset_password_request', N'COLUMN', requested_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'reset_password_request', N'COLUMN', expires_at");
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_A0ADF42EA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('CREATE INDEX index_username_status_deleted ON users (user_name, status, deleted)');
    }

    public function down(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('ALTER TABLE reset_password_request DROP CONSTRAINT FK_A0ADF42EA76ED395');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP INDEX index_username_status_deleted ON users');
    }
}
