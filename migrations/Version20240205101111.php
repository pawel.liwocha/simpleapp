<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240205101111 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE INDEX index_campaign_senddate_status_deleted ON campaign_sent_to_lead (campaign_id, send_date, status, deleted)');
        $this->addSql("EXEC sp_rename N'campaign_sent_to_lead.uniq_49e2e443d17f50a6', N'UNIQ_ECE053C1D17F50A6', N'INDEX'");
        $this->addSql("EXEC sp_rename N'campaign_sent_to_lead.idx_49e2e443b03a8386', N'IDX_ECE053C1B03A8386', N'INDEX'");
        $this->addSql("EXEC sp_rename N'campaign_sent_to_lead.idx_49e2e443896dbbde', N'IDX_ECE053C1896DBBDE', N'INDEX'");
    }

    public function down(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('DROP INDEX index_campaign_senddate_status_deleted ON campaign_sent_to_lead');
        $this->addSql("EXEC sp_rename N'campaign_sent_to_lead.uniq_ece053c1d17f50a6', N'UNIQ_49E2E443D17F50A6', N'INDEX'");
        $this->addSql("EXEC sp_rename N'campaign_sent_to_lead.idx_ece053c1b03a8386', N'IDX_49E2E443B03A8386', N'INDEX'");
        $this->addSql("EXEC sp_rename N'campaign_sent_to_lead.idx_ece053c1896dbbde', N'IDX_49E2E443896DBBDE', N'INDEX'");
    }
}
