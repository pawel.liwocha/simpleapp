<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240226121801 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE category_service (
    id INT IDENTITY NOT NULL,
    uuid NVARCHAR(255) NOT NULL,
    parent_category_service_id INT,
    name NVARCHAR(255) NOT NULL,
    actived BIT NOT NULL,
    deleted BIT NOT NULL,
    created_by_id BIGINT,
    created_at DATETIME2(6) NOT NULL,
    updated_by_id BIGINT,
    updated_at DATETIME2(6),
    deleted_at DATETIME2(6),
     PRIMARY KEY (id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_54E59921D17F50A6 ON category_service (uuid) WHERE uuid IS NOT NULL');
        $this->addSql('CREATE INDEX IDX_54E599219DF66FE ON category_service (parent_category_service_id)');
        $this->addSql('CREATE INDEX IDX_54E59921B03A8386 ON category_service (created_by_id)');
        $this->addSql('CREATE INDEX IDX_54E59921896DBBDE ON category_service (updated_by_id)');
        $this->addSql('CREATE INDEX index_id ON category_service (id)');
        $this->addSql('CREATE INDEX index_uuid ON category_service (name)');
        $this->addSql('CREATE INDEX index_uuid_deleted ON category_service (uuid, deleted)');
        $this->addSql('CREATE INDEX index_pcs_deleted ON category_service (parent_category_service_id, deleted)');
        $this->addSql('CREATE INDEX index_name_deleted ON category_service (name, deleted)');
        $this->addSql('CREATE INDEX index_name_active_deleted ON category_service (name, actived, deleted)');
        $this->addSql('CREATE INDEX index_pcs_active_deleted ON category_service (parent_category_service_id, actived, deleted)');
        $this->addSql('CREATE INDEX index_created_by_deleted ON category_service (created_by_id, deleted)');
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'category_service', N'COLUMN', created_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'category_service', N'COLUMN', updated_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'category_service', N'COLUMN', deleted_at");
        $this->addSql('ALTER TABLE category_service ADD CONSTRAINT DF_54E59921_8B8E8428 DEFAULT CURRENT_TIMESTAMP FOR created_at');
        $this->addSql('ALTER TABLE category_service ADD CONSTRAINT DF_54E59921_7698351C DEFAULT 1 FOR actived');
        $this->addSql('ALTER TABLE category_service ADD CONSTRAINT DF_54E59921_EB3B4E33 DEFAULT 0 FOR deleted');
        $this->addSql('CREATE TABLE parent_category_service (
    id INT IDENTITY NOT NULL,
    uuid NVARCHAR(255) NOT NULL,
    name NVARCHAR(255) NOT NULL,
    actived BIT NOT NULL,
    deleted BIT NOT NULL,
    created_by_id BIGINT,
    created_at DATETIME2(6) NOT NULL,
    updated_by_id BIGINT,
    updated_at DATETIME2(6),
    deleted_at DATETIME2(6),
    PRIMARY KEY (id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_43C5ACEED17F50A6 ON parent_category_service (uuid) WHERE uuid IS NOT NULL');
        $this->addSql('CREATE INDEX IDX_43C5ACEEB03A8386 ON parent_category_service (created_by_id)');
        $this->addSql('CREATE INDEX IDX_43C5ACEE896DBBDE ON parent_category_service (updated_by_id)');
        $this->addSql('CREATE INDEX index_id ON parent_category_service (id)');
        $this->addSql('CREATE INDEX index_uuid ON parent_category_service (name)');
        $this->addSql('CREATE INDEX index_uuid_deleted ON parent_category_service (uuid, deleted)');
        $this->addSql('CREATE INDEX index_name_deleted ON parent_category_service (name, deleted)');
        $this->addSql('CREATE INDEX index_name_active_deleted ON parent_category_service (name, actived, deleted)');
        $this->addSql('CREATE INDEX index_created_by_deleted ON parent_category_service (created_by_id, deleted)');
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'parent_category_service', N'COLUMN', created_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'parent_category_service', N'COLUMN', updated_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'parent_category_service', N'COLUMN', deleted_at");
        $this->addSql('ALTER TABLE parent_category_service ADD CONSTRAINT DF_43C5ACEE_8B8E8428 DEFAULT CURRENT_TIMESTAMP FOR created_at');
        $this->addSql('ALTER TABLE parent_category_service ADD CONSTRAINT DF_43C5ACEE_7698351C DEFAULT 1 FOR actived');
        $this->addSql('ALTER TABLE parent_category_service ADD CONSTRAINT DF_43C5ACEE_EB3B4E33 DEFAULT 0 FOR deleted');
        $this->addSql('CREATE TABLE service (
    id INT IDENTITY NOT NULL,
    uuid NVARCHAR(255) NOT NULL,
    category_service_id INT,
    name NVARCHAR(255) NOT NULL,
    actived BIT NOT NULL,
    deleted BIT NOT NULL,
    created_by_id BIGINT,
    created_at DATETIME2(6) NOT NULL,
    updated_by_id BIGINT,
    updated_at DATETIME2(6),
    deleted_at DATETIME2(6),
    PRIMARY KEY (id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_65A4AB8FD17F50A6 ON service (uuid) WHERE uuid IS NOT NULL');
        $this->addSql('CREATE INDEX IDX_65A4AB8FCB42F998 ON service (category_service_id)');
        $this->addSql('CREATE INDEX IDX_65A4AB8FB03A8386 ON service (created_by_id)');
        $this->addSql('CREATE INDEX IDX_65A4AB8F896DBBDE ON service (updated_by_id)');
        $this->addSql('CREATE INDEX index_id ON service (id)');
        $this->addSql('CREATE INDEX index_uuid ON service (name)');
        $this->addSql('CREATE INDEX index_uuid_deleted ON service (uuid, deleted)');
        $this->addSql('CREATE INDEX index_cs_deleted ON service (category_service_id, deleted)');
        $this->addSql('CREATE INDEX index_name_deleted ON service (name, deleted)');
        $this->addSql('CREATE INDEX index_name_active_deleted ON service (name, actived, deleted)');
        $this->addSql('CREATE INDEX index_cs_active_deleted ON service (category_service_id, actived, deleted)');
        $this->addSql('CREATE INDEX index_created_by_deleted ON service (created_by_id, deleted)');
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'service', N'COLUMN', created_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'service', N'COLUMN', updated_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'service', N'COLUMN', deleted_at");
        $this->addSql('ALTER TABLE service ADD CONSTRAINT DF_65A4AB8F_8B8E8428 DEFAULT CURRENT_TIMESTAMP FOR created_at');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT DF_65A4AB8F_7698351C DEFAULT 1 FOR actived');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT DF_65A4AB8F_EB3B4E33 DEFAULT 0 FOR deleted');
        $this->addSql('ALTER TABLE category_service ADD CONSTRAINT FK_54E599219DF66FE FOREIGN KEY (parent_category_service_id) REFERENCES parent_category_service (id)');
        $this->addSql('ALTER TABLE category_service ADD CONSTRAINT FK_54E59921B03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE category_service ADD CONSTRAINT FK_54E59921896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE parent_category_service ADD CONSTRAINT FK_43C5ACEEB03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE parent_category_service ADD CONSTRAINT FK_43C5ACEE896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_65A4AB8FCB42F998 FOREIGN KEY (category_service_id) REFERENCES category_service (id)');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_65A4AB8FB03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_65A4AB8F896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
    }

    public function down(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('ALTER TABLE category_service DROP CONSTRAINT FK_54E599219DF66FE');
        $this->addSql('ALTER TABLE category_service DROP CONSTRAINT FK_54E59921B03A8386');
        $this->addSql('ALTER TABLE category_service DROP CONSTRAINT FK_54E59921896DBBDE');
        $this->addSql('ALTER TABLE parent_category_service DROP CONSTRAINT FK_43C5ACEEB03A8386');
        $this->addSql('ALTER TABLE parent_category_service DROP CONSTRAINT FK_43C5ACEE896DBBDE');
        $this->addSql('ALTER TABLE service DROP CONSTRAINT FK_65A4AB8FCB42F998');
        $this->addSql('ALTER TABLE service DROP CONSTRAINT FK_65A4AB8FB03A8386');
        $this->addSql('ALTER TABLE service DROP CONSTRAINT FK_65A4AB8F896DBBDE');
        $this->addSql('DROP TABLE category_service');
        $this->addSql('DROP TABLE parent_category_service');
        $this->addSql('DROP TABLE service');
    }
}
