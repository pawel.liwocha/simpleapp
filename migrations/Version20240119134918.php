<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240119134918 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE failure_login_attempt (
            id BIGINT IDENTITY NOT NULL,
            ip NVARCHAR(255),
            username NVARCHAR(255) NOT NULL,
            data VARCHAR(MAX),
            created_at DATETIME2(6) NOT NULL,
            PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX index_id ON failure_login_attempt (id)');
        $this->addSql('CREATE INDEX index_username ON failure_login_attempt (username)');
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'failure_login_attempt', N'COLUMN', created_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:json)', N'SCHEMA', 'dbo', N'TABLE', 'failure_login_attempt', N'COLUMN', data");
        $this->addSql('ALTER TABLE failure_login_attempt ADD CONSTRAINT DF_1B677A7F_8B8E8428 DEFAULT CURRENT_TIMESTAMP FOR created_at');
        $this->addSql('CREATE TABLE login_log (
            id BIGINT IDENTITY NOT NULL,
            user_id BIGINT,
            token NVARCHAR(255),
            user_agent NVARCHAR(1000),
            ip NVARCHAR(100),
            status NVARCHAR(50) NOT NULL,
            created_at DATETIME2(6) NOT NULL,
            PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX index_id ON login_log (id)');
        $this->addSql('CREATE INDEX index_user ON login_log (user_id)');
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'login_log', N'COLUMN', created_at");
        $this->addSql('ALTER TABLE login_log ADD CONSTRAINT DF_AFA638E5_8B8E8428 DEFAULT CURRENT_TIMESTAMP FOR created_at');
        $this->addSql("ALTER TABLE login_log ADD CONSTRAINT DF_AFA638E5_7B00651C DEFAULT 'success' FOR status");
        $this->addSql('CREATE TABLE token (
            id BIGINT IDENTITY NOT NULL,
            user_id BIGINT,
            token NVARCHAR(255),
            expired DATETIME2(6) NOT NULL,
            created_at DATETIME2(6) NOT NULL,
            PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX index_id ON token (id)');
        $this->addSql('CREATE INDEX index_user ON token (user_id)');
        $this->addSql('CREATE INDEX index_token ON token (token)');
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'token', N'COLUMN', created_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'token', N'COLUMN', expired");
        $this->addSql('ALTER TABLE token ADD CONSTRAINT DF_271760AA_8B8E8428 DEFAULT CURRENT_TIMESTAMP FOR created_at');
        $this->addSql('ALTER TABLE token ADD CONSTRAINT DF_271760AA_194FED4B DEFAULT CURRENT_TIMESTAMP FOR expired');
        $this->addSql('CREATE TABLE users (
            id BIGINT IDENTITY NOT NULL,
            uuid NVARCHAR(255) NOT NULL,
            user_name NVARCHAR(255) NOT NULL,
            first_name NVARCHAR(255) NOT NULL,
            last_name NVARCHAR(255) NOT NULL,
            email NVARCHAR(180) NOT NULL,
            password NVARCHAR(180) NOT NULL,
            status NVARCHAR(50) NOT NULL,
            roles VARCHAR(MAX) NOT NULL,
            deleted BIT NOT NULL,
            created_at DATETIME2(6) NOT NULL,
            deactivate_at DATETIME2(6),
            deleted_at DATETIME2(6),
            PRIMARY KEY (id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6CA36478D17F50A6 ON users (uuid) WHERE uuid IS NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6CA36478E7927C74 ON users (user_name) WHERE user_name IS NOT NULL');
        $this->addSql('CREATE INDEX index_id ON users (id)');
        $this->addSql('CREATE INDEX index_id_deleted ON users (id, deleted)');
        $this->addSql('CREATE INDEX index_uuid ON users (uuid)');
        $this->addSql('CREATE INDEX index_uuid_deleted ON users (uuid, deleted)');
        $this->addSql('CREATE INDEX index_username ON users (user_name)');
        $this->addSql('CREATE INDEX index_username_deleted ON users (user_name, deleted)');
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'users', N'COLUMN', created_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:json)', N'SCHEMA', 'dbo', N'TABLE', 'users', N'COLUMN', roles");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'users', N'COLUMN', deactivate_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'users', N'COLUMN', deleted_at");
        $this->addSql('ALTER TABLE users ADD CONSTRAINT DF_6CA36478_8B8E8428 DEFAULT CURRENT_TIMESTAMP FOR created_at');
        $this->addSql("ALTER TABLE users ADD CONSTRAINT DF_6CA36478_7B00651C DEFAULT 'active' FOR status");
        $this->addSql('ALTER TABLE users ADD CONSTRAINT DF_6CA36478_EB3B4E33 DEFAULT 0 FOR deleted');
        $this->addSql('CREATE TABLE messenger_messages (
            id BIGINT IDENTITY NOT NULL,
            body VARCHAR(MAX) NOT NULL,
            headers VARCHAR(MAX) NOT NULL,
            queue_name NVARCHAR(190) NOT NULL,
            created_at DATETIME2(6) NOT NULL,
            available_at DATETIME2(6) NOT NULL,
            delivered_at DATETIME2(6),
            PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'messenger_messages', N'COLUMN', created_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'messenger_messages', N'COLUMN', available_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'messenger_messages', N'COLUMN', delivered_at");
        $this->addSql('ALTER TABLE login_log ADD CONSTRAINT FK_AFA638E5A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE token ADD CONSTRAINT FK_271760AAA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
    }

    public function down(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('ALTER TABLE login_log DROP CONSTRAINT FK_AFA638E5A76ED395');
        $this->addSql('ALTER TABLE token DROP CONSTRAINT FK_271760AAA76ED395');
        $this->addSql('DROP TABLE failure_login_attempt');
        $this->addSql('DROP TABLE login_log');
        $this->addSql('DROP TABLE token');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
