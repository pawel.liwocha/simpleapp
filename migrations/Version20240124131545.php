<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240124131545 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE campaign (
            id BIGINT IDENTITY NOT NULL,
            uuid NVARCHAR(255) NOT NULL,
            campaign_name NVARCHAR(255) NOT NULL,
            start_date DATETIME2(6) NOT NULL,
            end_date DATETIME2(6) NOT NULL,
            description NVARCHAR(500),
            type NVARCHAR(50) NOT NULL,
            status NVARCHAR(50) NOT NULL,
            promo BIT NOT NULL,
            deleted BIT NOT NULL,
            created_by_id BIGINT,
            created_at DATETIME2(6) NOT NULL,
            updated_by_id BIGINT,
            updated_at DATETIME2(6),
            deleted_at DATETIME2(6),
            PRIMARY KEY (id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A4B06A5D17F50A6 ON campaign (uuid) WHERE uuid IS NOT NULL');
        $this->addSql('CREATE INDEX IDX_A4B06A5B03A8386 ON campaign (created_by_id)');
        $this->addSql('CREATE INDEX IDX_A4B06A5896DBBDE ON campaign (updated_by_id)');
        $this->addSql('CREATE INDEX index_id ON campaign (id)');
        $this->addSql('CREATE INDEX index_id_deleted ON campaign (id, deleted)');
        $this->addSql('CREATE INDEX index_uuid ON campaign (uuid)');
        $this->addSql('CREATE INDEX index_uuid_deleted ON campaign (uuid, deleted)');
        $this->addSql('CREATE INDEX index_start_end_deleted ON campaign (start_date, end_date, deleted)');
        $this->addSql('CREATE INDEX index_type_deleted ON campaign (type, deleted)');
        $this->addSql('CREATE INDEX index_status_deleted ON campaign (status, deleted)');
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'campaign', N'COLUMN', created_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'campaign', N'COLUMN', start_date");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'campaign', N'COLUMN', end_date");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'campaign', N'COLUMN', updated_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'campaign', N'COLUMN', deleted_at");
        $this->addSql('ALTER TABLE campaign ADD CONSTRAINT DF_A4B06A5_8B8E8428 DEFAULT CURRENT_TIMESTAMP FOR created_at');
        $this->addSql("ALTER TABLE campaign ADD CONSTRAINT DF_A4B06A5_8CDE5729 DEFAULT 'whatsapp' FOR type");
        $this->addSql("ALTER TABLE campaign ADD CONSTRAINT DF_A4B06A5_7B00651C DEFAULT 'created' FOR status");
        $this->addSql('ALTER TABLE campaign ADD CONSTRAINT DF_A4B06A5_B0139AFB DEFAULT 0 FOR promo');
        $this->addSql('ALTER TABLE campaign ADD CONSTRAINT DF_A4B06A5_EB3B4E33 DEFAULT 0 FOR deleted');
        $this->addSql('CREATE TABLE campaign_content (
            id INT IDENTITY NOT NULL,
            uuid NVARCHAR(255) NOT NULL,
            campaign_id BIGINT,
            froms NVARCHAR(255),
            texts VARCHAR(MAX),
            deleted BIT NOT NULL,
            created_by_id BIGINT,
            created_at DATETIME2(6) NOT NULL,
            updated_by_id BIGINT,
            updated_at DATETIME2(6),
            deleted_at DATETIME2(6),
            PRIMARY KEY (id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_24DFEF08D17F50A6 ON campaign_content (uuid) WHERE uuid IS NOT NULL');
        $this->addSql('CREATE INDEX IDX_24DFEF08F639F774 ON campaign_content (campaign_id)');
        $this->addSql('CREATE INDEX IDX_24DFEF08B03A8386 ON campaign_content (created_by_id)');
        $this->addSql('CREATE INDEX IDX_24DFEF08896DBBDE ON campaign_content (updated_by_id)');
        $this->addSql('CREATE INDEX index_id ON campaign_content (id)');
        $this->addSql('CREATE INDEX index_uuid ON campaign_content (uuid)');
        $this->addSql('CREATE INDEX index_uuid_deleted ON campaign_content (uuid, deleted)');
        $this->addSql('CREATE INDEX index_campaign_deleted ON campaign_content (campaign_id, deleted)');
        $this->addSql('CREATE INDEX index_created_by_deleted ON campaign_content (created_by_id, deleted)');
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'campaign_content', N'COLUMN', created_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'campaign_content', N'COLUMN', updated_at");
        $this->addSql("EXEC sp_addextendedproperty N'MS_Description', N'(DC2Type:datetime_immutable)', N'SCHEMA', 'dbo', N'TABLE', 'campaign_content', N'COLUMN', deleted_at");
        $this->addSql('ALTER TABLE campaign_content ADD CONSTRAINT DF_24DFEF08_8B8E8428 DEFAULT CURRENT_TIMESTAMP FOR created_at');
        $this->addSql('ALTER TABLE campaign_content ADD CONSTRAINT DF_24DFEF08_EB3B4E33 DEFAULT 0 FOR deleted');
        $this->addSql('ALTER TABLE campaign ADD CONSTRAINT FK_A4B06A5B03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE campaign ADD CONSTRAINT FK_A4B06A5896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE campaign_content ADD CONSTRAINT FK_24DFEF08F639F774 FOREIGN KEY (campaign_id) REFERENCES campaign (id)');
        $this->addSql('ALTER TABLE campaign_content ADD CONSTRAINT FK_24DFEF08B03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE campaign_content ADD CONSTRAINT FK_24DFEF08896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
    }

    public function down(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping MSSQL migration.');
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('ALTER TABLE campaign DROP CONSTRAINT FK_A4B06A5B03A8386');
        $this->addSql('ALTER TABLE campaign DROP CONSTRAINT FK_A4B06A5896DBBDE');
        $this->addSql('ALTER TABLE campaign_content DROP CONSTRAINT FK_24DFEF08F639F774');
        $this->addSql('ALTER TABLE campaign_content DROP CONSTRAINT FK_24DFEF08B03A8386');
        $this->addSql('ALTER TABLE campaign_content DROP CONSTRAINT FK_24DFEF08896DBBDE');
        $this->addSql('DROP TABLE campaign');
        $this->addSql('DROP TABLE campaign_content');
    }
}
