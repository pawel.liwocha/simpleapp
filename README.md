# README #

This README would normally document whatever steps are necessary to get your application up and running.

To send Campaigns need use cron for Command `simpleapp:check-campaign-can-be-send` and SymfonyMessenger in run - e.g. via supervisor

## Requirements
- PHP >= 8.2
- MSSQL - Azure / MariaDB (refactor)
  - przy XAMPP na Windows potrzebne w `php.ini`:
  ```
  extension=php_sqlsrv_82_ts_x64.dll
  extension=php_pdo_sqlsrv_82_ts_x64.dll
  ```
- Composer 2

## Installation

- Copy repository
- Change the `.env` file to configure mysql and mailer and rename to `.env.local`
- `composer install`
- `php bin/console doctrine:migrations:migrate`
- `php bin/console doctrine:fixtures:load`

## CS-Fixer

usage:
```php vendor/bin/php-cs-fixer fix src --dry-run --config .php-cs-fixer.dist.php --diff```
```php vendor/bin/php-cs-fixer fix tests --dry-run --config .php-cs-fixer.dist.php --diff```
```php vendor/bin/php-cs-fixer fix --verbose --diff --config .php-cs-fixer.dist.php --dry-run```

## PHPUnit Tests

usage:
```php vendor/bin/phpunit tests --stop-on-failure```

## PHPStan

usage :
```php vendor/bin/phpstan analyse --verbose --memory-limit=-1 -c phpstan.neon```

## MESSENGER COMPONENT

usage:
```php bin/console messenger:consume async```

## Rector

usage:
```vendor/bin/rector process -c rector.php --dry-run```
