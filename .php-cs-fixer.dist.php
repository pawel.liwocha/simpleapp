<?php

declare(strict_types=1);

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude('config')
    ->exclude('public')
    ->exclude('var')
    ->exclude('node_modules')
    ->notPath('src/Kernel.php')
    ->notPath('tests/bootstrap.php')
    ->notPath('src/Infrastructure/WhatsApp/Response/ApiResponse.php')
;

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules([
        '@PhpCsFixer' => true,
        '@PhpCsFixer:risky' => true,
        '@PHP80Migration:risky' => true,
        '@PHP81Migration' => true,
        '@PHP82Migration' => true,
        '@DoctrineAnnotation' => true,
        '@Symfony' => true,
        'declare_strict_types' => true,
        'yoda_style' => ['equal' => false, 'identical' => false, 'less_and_greater' => false],
    ])
    ->setFinder($finder)
    ->setUsingCache(false)
;
